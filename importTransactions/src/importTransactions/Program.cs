﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace importTransactions
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                System.Console.WriteLine("Usage: ImportTransactions inputCSVfile [CheckNames | CheckLots]");
                Environment.Exit(0);
            }
            string strInFile = args[0];
            string[] fields = null;
            bool bCheckNames = false;  // checking mode
            bool bCheckLots = false;  // check referenced seedlots exist
            bool bPerson = true;  // record  contains a person entry
            if (args.Length > 1)
            {
                if ("CheckNames" == args[1] || "checknames" == args[1] || "Checknames" == args[1])
                    bCheckNames = true;
                if ("CheckLots" == args[1] || "checklots" == args[1] || "Checklots" == args[1] )
                    bCheckLots = true;

            }
            StreamWriter sw = null;  // for writing during check runs
            StreamWriter sw2 = null;  // error file - always open and close it.
            if (bCheckNames)
            {
                FileStream fs = new FileStream("NameIssues.txt", FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs);
            }
            else if (bCheckLots)
            {
                FileStream fs = new FileStream("LotIssues.txt", FileMode.Create, FileAccess.Write);
                sw = new StreamWriter(fs);
            }

            FileStream fs2 = new FileStream("Errors.txt", FileMode.Create, FileAccess.Write);
            sw2 = new StreamWriter(fs2);


            try
            {
                // connect to database
                string connString;
                connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                if (null == connString)
                    connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                SqlConnection connection = new SqlConnection(connString);
                // Person dictionary
                // We will build a dictionary based on person name (firstname plus a space 
                // and a last name) that contains a tuple of their person ID and their organization Id
                string strQuery = "select FirstName,LastName,PersonID,OrganizationID from Person;";
                SqlCommand command = new SqlCommand(strQuery, connection);
                connection.Open();
                Dictionary<String, Tuple<int, int>> dictPerson = new Dictionary<String, Tuple<int, int>>();
                // Using statement below is so that the reader goes out of scope when we are done
                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            string strWholeName = reader[0].ToString() + " " + reader[1].ToString();
                            int personID = Convert.ToInt32(reader[2].ToString());
                            int orgID = Convert.ToInt32(reader[3].ToString());
                            Tuple<int, int> tup = new Tuple<int, int>(personID, orgID);
                            dictPerson.Add(strWholeName, tup);
                            Console.WriteLine(String.Format("{0}, {1} {2} {3}",
                                reader[0], reader[1], reader[2], reader[3]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error building Person dictioniary");
                        Console.WriteLine(ex.Message);
                    }
                }
                // Seedlot dictionery
                // build a dictionary based on SeedLotNumber that has the SeedLotID
                // note SeedLotNumber will have either the AD or the FM number depending
                // on which the lot was created under
                strQuery = "select FMNumber,seedLotID from SeedLot;";
                command.CommandText = strQuery;
                Dictionary<int, int> dictFMSeedLotNum = new Dictionary<int, int>();
                Dictionary<int, int> dictADSeedLotNum = new Dictionary<int, int>();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int ioldLotNo = Convert.ToInt32(reader[0].ToString());
                            if (-1 == ioldLotNo)
                                continue;
                            int newLotID = Convert.ToInt32(reader[1].ToString());
                            dictFMSeedLotNum.Add(ioldLotNo, newLotID);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error building seedlot dictioniary");
                        Console.WriteLine(ex.Message);
                    }
                }
                strQuery = "select ADNumber,seedLotID from SeedLot;";
                command.CommandText = strQuery;
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int ioldLotNo = Convert.ToInt32(reader[0].ToString());
                            int newLotID = Convert.ToInt32(reader[1].ToString());
                            dictADSeedLotNum.Add(ioldLotNo, newLotID);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error building seedlot dictioniary");
                        Console.WriteLine(ex.Message);
                    }
                }
                //
                // Now read the iput file - the csv
                // open file
                int iLine = 0;
                StreamReader sr = new StreamReader(new FileStream(strInFile, FileMode.Open, FileAccess.Read));
                // skip the first line as it is column headers
                sr.ReadLine();
                iLine++;
                // now walk the file buiding insert strings and sending them to the db.
                string strLine = " ";
                int rowID = 0;
                int rowsAdded = 0;
                string strInsert;
                int oldLotNo;
                //
                // for each line
                while (null != strLine)
                {
                    rowID++;
                    strLine = sr.ReadLine();
                    iLine++;
                    if (null == strLine) break;
                    // remove the quotation marks from the fields
                    strLine = strLine.Replace('"', ' ');

                    //strLine = strLine.Replace('"', ' ');
                    // split into an array of the fields
                    fields = strLine.Split(',');
                    // remove leading and trailing spaces 
                    for (int i = 0; i < fields.Length; ++i)
                    {
                        fields[i] = fields[i].Trim(' ');
                    }
                    // Input file has LotNumber,TransDate,grams,TransactionType,Partner Name,optionally trans#
                    // Table has SeedLotTransactionID, CreatedBy, CreationDate, OrganizationID, ModifiedBy ModifiedDate, 
                    // Notes, PersonID, Quantity,SeedLotID, TransactionType,OldTransactionID
                    //
                    // mapping is:
                    // Table Col,               File Col
                    //================          =================
                    // SeedLotTransactionID     ignore
                    // CreatedBy                "Imported"
                    // CreationDate             date/time of import
                    // ModifiedBy               "Imported"
                    // ModifiedDate             date/time of import
                    // TransactionDate          "TransDate (1)
                    // Notes                    ignore
                    // PersonID                 dictPerson[partner name (4)], first part of tuple
                    // Quantity                 grams (2)
                    // SeedLotID                dictSeedLotNum[LotNumber (0)]
                    // TransactionType          get enum from string TransactionType (3)
                    // OldTransactionID         optional from trans# (5)
                    // OrganizationID           dictPerson[partner name (4)], second part of tuple
                    // build the insert statement

                    // Skip transactions of types "Original Quantity from Old Database" and "Transaction from Old Database"
                    if (("Original Quantity from Old Database" == fields[3]) ||
                        ("Transaction from Old Database" == fields[3]))
                        continue;
                    // Lot checking
                    bool bFailedFMLookup = false;
                    bool bFailedADLookup = false;
                    if(bCheckLots)
                    {
                        try
                        {
                            bFailedFMLookup = false;
                            bFailedADLookup = false;
                            oldLotNo = Convert.ToInt32(fields[0]);
                            int newLotNo = dictFMSeedLotNum[oldLotNo];   //  new seedlotID
                        }
                        catch 
                        {
                            bFailedFMLookup = true;
                            try
                            {
                                oldLotNo = Convert.ToInt32(fields[0]);
                                int newLotNo = dictADSeedLotNum[oldLotNo];   //  new seedlotID
                            }
                            catch 
                            {
                                bFailedADLookup = true;
                                sw.WriteLine("Seedlot not found in AD dictionary: {0} row {1}", fields[0], rowID + 1);
                                continue;
                            }
                            if(bFailedFMLookup && bFailedADLookup)
                                sw.WriteLine("Seedlot not found in FM dictionary: {0} row {1}", fields[0],rowID+1);
                            System.Console.WriteLine("{0}", rowID);
                            continue;
                        }
                        continue;
                    }
                    if(bCheckLots)
                    {
                        sw.Flush();
                        Environment.Exit(0);
                    }
                    //
                    //
                    if (fields.Length < 5 || "" == fields[4])  // no person
                    {
                        bPerson = false;
                        strInsert = "INSERT INTO SeedLotTransaction (CreatedBy,CreationDate," +
                       "ModifiedBy,ModifiedDate,TransactionDate,Quantity,SeedLotID,TransactionType";
                    }
                    else
                    {
                        bPerson = true;
                        strInsert = "INSERT INTO SeedLotTransaction (CreatedBy,CreationDate," +
                       "ModifiedBy,ModifiedDate,TransactionDate,PersonID,Quantity,SeedLotID,TransactionType";

                    }
                    if (fields.Length >= 6)  // old transaction ID included
                        strInsert += ",OldTransactionID";
                    if (bPerson)
                        strInsert += ",OrganizationID) VALUES(";
                    else
                        strInsert += ") VALUES(";


                    strInsert += "'" + "Imported" + "', ";              // CreatedBy
                    DateTime dt = DateTime.Now;                         // creationdate
                    strInsert += "'" + dt.ToString() + "', ";
                    strInsert += "'" + "Imported" + "', ";              // ModifiedBy
                    strInsert += "'" + dt.ToString() + "', ";           // ModifiedDate
                    strInsert += "'" + fields[1].ToString() + "', ";    // TransactionDate
                                                                        // lookup person and orgID
                    Tuple<int, int> tup=null;
                    int personID = -1;
                    int orgID = -1;
                    if (bPerson)
                    {
                        try
                        {
                            string strKey = fields[4].ToString();
                            tup = dictPerson[strKey];
                        }
                        catch (Exception ex)
                        {
                            System.Console.WriteLine("{0}  Person not found in dictionary: {1}",   rowID, fields[4]);
                            System.Console.WriteLine("Exception was: {0}", ex.Message);
                            if (bCheckNames)
                            {
                                sw.WriteLine("Person not found in dictionary: {0}", fields[4]);
                                continue;
                            }
                            else
                                Environment.Exit(-2);
                        }
                        if (bCheckNames)
                        {
                            System.Console.WriteLine("{0}", rowID);
                            continue;
                        }
                        personID = tup.Item1;
                        orgID = tup.Item2;
                    }
                    int transType = TransTypeStrToInt(fields[3]);
                    if (bPerson)
                        strInsert += personID.ToString() + ", ";        // personID
                    // determine sign of quantity
                    float fQuant = Convert.ToSingle(fields[2]);
                    if (((int)SeedLotTransactionType.Disbursement == transType) ||
                         ((int)SeedLotTransactionType.Discarded == transType))
                        fQuant = -fQuant;

                    strInsert += fQuant.ToString() + ", ";                  //quantity
                    oldLotNo = Convert.ToInt32(fields[0]);
                    try
                    {
                        strInsert += dictFMSeedLotNum[oldLotNo].ToString() + ",";   //  new seedlotID
                    }
                    catch
                    {
                        try
                        {
                            strInsert += dictADSeedLotNum[oldLotNo].ToString() + ",";   //  new seedlotID
                        }
                        catch
                        {
                            System.Console.WriteLine("Failed to find lot # {0} on row {1}",oldLotNo,rowID);
                            Environment.Exit(-1);
                        }
                    }
                    strInsert += transType.ToString();            //transaction type
                    if (fields.Length >= 6)                       // old transaction ID
                    {
                        int oldTranID = Convert.ToInt32(fields[5]);
                        strInsert += ", " +oldTranID.ToString() ;
                    }
                    if (bPerson)                                // if no person, no org
                        strInsert += ", " + orgID.ToString();
                    strInsert += ");";
                    // sanity check on disbursement
                    if((int)SeedLotTransactionType.Disbursement == transType && !bPerson)
                    {
                        sw2.WriteLine("No person given for Disbursement Transaction. LotNumber {0}, Date {1}",
                            fields[0].ToString(), fields[1].ToString());
                    }
                    // show the command we will then execute
                    Console.WriteLine(strInsert);
                    command.CommandText = strInsert;
                    int rows = command.ExecuteNonQuery();
                    rowsAdded += rows;
                    Console.WriteLine("{0}", rowsAdded);
                }
                Console.WriteLine("Added {0} rows.", rowsAdded);
                if (bCheckNames  || bCheckLots)
                    sw.Flush();
                sw2.Flush(); // flush the error file

            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

        }
        private static int TransTypeStrToInt(string strTrans)
        {
            int iTrans = -1;
            switch (strTrans)
            {
                case "Receive from Partner":
                    iTrans = (int)SeedLotTransactionType.ReceiveFromPartner;
                    break;
                case "Wild Collection":
                    iTrans = (int)SeedLotTransactionType.WildCollection;
                    break;
                case "Harvest":
                    iTrans = (int)SeedLotTransactionType.Harvest;
                    break;
                case "Disbursement":
                    iTrans = (int)SeedLotTransactionType.Disbursement;
                    break;
                case "Discarded":
                    iTrans = (int)SeedLotTransactionType.Discarded;
                    break;
                case "Returned Seed":
                    iTrans = (int)SeedLotTransactionType.Returned;
                    break;
                case "Inventory Correction":
                case "Inventory Adjustment":
                    iTrans = (int)SeedLotTransactionType.InventoryCorrection;
                    break;
            }
            return iTrans;
        }

        public enum SeedLotTransactionType { Harvest, WildCollection, ReceiveFromPartner, Disbursement, Discarded, Returned, InventoryCorrection }
    }

}
