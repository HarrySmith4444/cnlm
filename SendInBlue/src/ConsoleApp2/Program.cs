﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using mailinblue;
using Newtonsoft.Json.Linq;


namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            API sendinBlue = new mailinblue.API("xtb5nNIUpH6v8rgW");

            Dictionary<string, Object> data = new Dictionary<string, Object>();
            Dictionary<string, string> to = new Dictionary<string, string>();
            to.Add("spittingturtle@gmail.com", "Mr Turtle");
            List<string> from_name = new List<string>();
            from_name.Add("harrysmith4444@gmail.com");
            from_name.Add("Mr. Harry");
            //from_name.Add("from email!");
            //List<string> attachment = new List<string>();
            //attachment.Add("https://domain.com/path-to-file/filename1.pdf");
            //attachment.Add("https://domain.com/path-to-file/filename2.jpg");

            data.Add("to", to);
            data.Add("from", from_name);
            data.Add("subject", "Mail In Blue test.");
            data.Add("html", "This is the <h1>HTML of the test</h1>");
            //data.Add("attachment", attachment);

            JObject  sendEmail = sendinBlue.send_email(data);
            Console.WriteLine(sendEmail);
        }
    }
}


