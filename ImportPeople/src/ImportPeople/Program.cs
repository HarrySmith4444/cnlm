﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
//
// Read the input CSV file for LevelIVRegions, look up the corresponding Level3Region ID, construct
// an insert statement and put the Level 4 regions in the database.

namespace ImportPeople
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                System.Console.WriteLine("Usage: ImportPeople inputCSVfile");
                Environment.Exit(0);
            }
            string strInFile = args[0];
            string[] fields = null;
            try
            {
                // connect to database
                string connString;
                connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                if (null == connString)
                    connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                SqlConnection connection = new SqlConnection(connString);
                //// We will build a dictionary of the Organization names and codes here by reading the 
                // Organization table from the database.
                string strQuery = "select OrganizationName,OrganizationID from Organization;";
                SqlCommand command = new SqlCommand(strQuery, connection);
                connection.Open();
                Dictionary<String, int> dictOrg = new Dictionary<String, int>();
                // Using statement below is so that the reader goes out of scope when we are done
                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int index = Convert.ToInt32(reader[1].ToString());
                            dictOrg.Add(reader[0].ToString(), index);
                            Console.WriteLine(String.Format("{0}, {1}",
                                reader[0], reader[1]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                //}
                // Now read the iput file - the csv
                // open file
                StreamReader sr = new StreamReader(new FileStream(strInFile, FileMode.Open, FileAccess.Read));
                // skip the first line as it is column headers
                sr.ReadLine();
                // now walk the file buiding insert strings and sending them to the db.
                string strLine = " ";
                int rowID = 0;
                int rowsAdded = 0;
                //
                // for each line
                while (null != strLine)
                {
                    rowID++;
                    strLine = sr.ReadLine();
                    if (null == strLine) break;
                    // remove the quotation marks from the fields
                    strLine = strLine.Replace('"', ' ');
                    // split into an array of the fields
                    fields = strLine.Split(',');
                    // remove leading and trailing spaces
                    for (int i = 0; i < fields.Length; ++i)
                    {
                        fields[i] = fields[i].Trim(' ');
                    }

                    // Input file has "PartnerName","Organization","Phone","Ext","Email","Title","Notes"
                    // Table has PersonID,CellPhone,FirstName,LastName,OrganizationID,Phone,Email,IsContactPerson,
                    //           City,Country,Extension,FirstLine,SecondLine,Title,ZipCode
                    //
                    // mapping is:
                    // Table Col,               File Col
                    //================          =================
                    // PersonID                 ignore
                    // CellPhone                blank
                    // FirstName                split from PartnerName (0)    
                    // LastName                 split from PartnerName (0)
                    // OrganizationID           Lookup from OrgDictionary (1)
                    // Phone                    Phone (2)
                    // Email                    Email (4)
                    // IsContactPerson          0
                    // City                     null
                    // Country                  null
                    // Extension                Ext(3)
                    // FirstLine                null
                    // SecondLine               null
                    // State                    null
                    // Title                    Title(5)+Notes(6)
                    // ZipCode                  null


                    // build the insert statement
                    string strInsert = "INSERT INTO Person (FirstName,LastName," +
                            "OrganizationID,Phone,Email,IsContactPerson,Extension,Title) VALUES(";
                    // split the name into two parts
                    string[] strNames = fields[0].Split(' ');
                    string strFirst, strSecond;
                    if (strNames.Length < 2)
                    {
                        strSecond = strNames[0];
                        strFirst = "";
                    }else
                    {
                        strSecond = strNames[1];
                        strFirst = strNames[0];
                    }
                    strInsert += "\'" + strFirst + "\'";             //FirstName
                    strInsert += ", " + "\'" + strSecond + "\'";             //SecondName
                    int reg3ID = dictOrg[fields[1]];                    //OrganizationID
                    strInsert += ", " + reg3ID.ToString(); // OrganizationID
                    strInsert += ", " + "\'" + fields[2] + "\'";        // 'Phone'
                    strInsert += ", " + "\'" + fields[4] + "\'";        // 'Email'
                    strInsert += ", " + "0";              // IsContactPerson
                    strInsert += ", " + "\'" + fields[3] + "\'";        // 'Extension'
                    if (fields[6].Length > 0)
                    {
                        strInsert += ", " + "\'" + fields[5] + " : " + fields[6] + "\'";        // 'Title : Notes'
                    }
                    else
                    {
                        strInsert += ", " + "\'" + fields[5] + "\'";        // 'Title'
                    }
                    strInsert += ");";                                 // );
                                                                       // show the command we will then execute
                    Console.WriteLine(strInsert);
                    command.CommandText = strInsert;
                    int rows = command.ExecuteNonQuery();
                    rowsAdded += rows;
                }

                Console.WriteLine("Added {0} rows.", rowsAdded);


            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

        }

    }
}
