﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
//
// Read the input CSV file for LevelIVRegions, look up the corresponding Level3Region ID, construct
// an insert statement and put the Level 4 regions in the database.

namespace ImportFarmBeds
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                System.Console.WriteLine("Usage: ImportFarmBeds inputCSVfile");
                Environment.Exit(0);
            }
            string strInFile = args[0];
            string[] fields = null;
            BedType eBedType;
            try
            {
                // connect to database
                string connString;
                connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                if (null == connString)
                    connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                SqlConnection connection = new SqlConnection(connString);
                // We will build a dictionary of the farm names and FarmSiteIDs here by reading the 
                // FarmSite table from the database.
                string strQuery = "select FarmName,FarmSiteID from FarmSite;";
                SqlCommand command = new SqlCommand(strQuery, connection);
                connection.Open();
                Dictionary<String, int> dictFarms = new Dictionary<String, int>();
                // Using statement below is so that the reader goes out of scope when we are done
                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int index = Convert.ToInt32(reader[1].ToString());
                            dictFarms.Add(reader[0].ToString(), index);
                            Console.WriteLine(String.Format("{0}, {1}",
                                reader[0], reader[1]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                // Now read the iput file - the csv
                // open file
                StreamReader sr = new StreamReader(new FileStream(strInFile, FileMode.Open, FileAccess.Read));
                // No header line in this file for some reason
                // skip the first line as it is column headers
                //sr.ReadLine();
                // now walk the file buiding insert strings and sending them to the db.
                string strLine = " ";
                int rowID = 0;
                int rowsAdded = 0;
                //
                // for each line
                while (null != strLine)
                {
                    rowID++;
                    strLine = sr.ReadLine();
                    if (null == strLine) break;
                    // remove the quotation marks from the fields
                    strLine = strLine.Replace('"', ' ');
                    // split into an array of the fields
                    fields = strLine.Split(',');
                    // remove leading and trailing spaces
                    // also escape any single quotes in the text with another single quote
                    for (int i = 0; i < fields.Length; ++i)
                    {
                        fields[i] = fields[i].Trim(' ');
                        fields[i] = fields[i].Replace("'", "''");
                    }
                    // Input file has "uniqueID","farmbedtag", "farm name","bed type","length, width"
                    // Table has FarmBedID, BedType, FarmSiteID, IrrigationFlowRate, IrrigationType, Length, Width, FarmBedTag
                    //
                    // mapping is:
                    // Table Col,               File Col
                    //================          =================
                    // FarmBedID          ignore
                    // BedType            if bedtype(3) is "Farm" then FarmBed if "Raised Bed" then NurseryBed else error
                    // FarmSiteID                     Lookup in dictionary based on farm name (2)
                    // IrrigationFlowRate               Use 0 for now
                    // IrrigationType       if bedtype(3) is "Farm" then "Overhead" else "Drip"
                    // Length               length (4)
                    // Width                width (5)
                    // FarmBedTag           farmbedtag (1)
                    //
                    // build the insert statement
                    string strInsert = "INSERT INTO FarmBeds (BedType,FarmSiteID,IrrigationFlowRate," +
                        "IrrigationType,Length,Width,FarmBedTag) VALUES(";
                    // Bed Type
                    if ("Farm" == fields[3])
                    {
                        strInsert += "\'" + "FarmBed";
                        eBedType = BedType.FarmBed;
                    }
                    else if ("Raised Bed" == fields[3])
                    {
                        strInsert += "\'" + "NurseryBed";
                        eBedType = BedType.NurseryBed;
                    }
                    else
                    {
                        // stick in farm if unknown
                        strInsert += "\'" + "FarmBed";
                        eBedType = BedType.FarmBed;
                        System.Console.WriteLine("Unrecognized bed type {0} in row {1}", fields[3], rowID);
                    }
                    // Before we lookup the site we have to remove any escaped single quotes
                    string strSiteLookup = fields[2];
                    strSiteLookup = strSiteLookup.Replace("''", "'");
                    int siteID = dictFarms[strSiteLookup];  // FarmSiteID
                    strInsert += "\', " +  siteID.ToString();
                    strInsert += ", " + "0";
                    // Irrigation Type
                    switch (eBedType)  //
                    {
                        case BedType.FarmBed:
                            strInsert += ", '" + "Overhead" + "'";
                            break;
                        case BedType.NurseryBed:
                            strInsert += ", '" + "Drip" + "'";
                            break;
                    }
                    strInsert += ", " + fields[4];       // 'length'
                    strInsert += ", " + fields[5];        // 'width'
                    strInsert += ", " + "\'" + fields[1] + "\'";        // 'FarmBedTag'
                    strInsert += ");";                                 // );
                    // show the command we will then execute
                    Console.WriteLine(strInsert);
                    command.CommandText = strInsert;
                    int rows = command.ExecuteNonQuery();
                    rowsAdded += rows;
                }
                Console.WriteLine("Added {0} rows.", rowsAdded);


            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

        }

    }
    public enum BedType
    {
        FarmBed,
        NurseryBed
    }
}
