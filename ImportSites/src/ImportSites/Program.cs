﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
//
// Read the input CSV file for LevelIVRegions, look up the corresponding Level3Region ID, construct
// an insert statement and put the Level 4 regions in the database.

namespace ImportSites
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                System.Console.WriteLine("Usage: ImportSites inputCSVfile");
                Environment.Exit(0);
            }
            string strInFile = args[0];
            string[] fields = null;
            try
            {
                // connect to database
                string connString;
                connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                if (null == connString)
                    connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                SqlConnection connection = new SqlConnection(connString);
                // We will build a dictionary of the 4 names and codes here by reading the 
                // Level 4 table from the database.
                string strQuery = "select Name,Level4RegionID from Level4Region;";
                SqlCommand command = new SqlCommand(strQuery, connection);
                connection.Open();
                Dictionary<String, int> dictlevel4 = new Dictionary<String, int>();
                // Using statement below is so that the reader goes out of scope when we are done
                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int index = Convert.ToInt32(reader[1].ToString());
                            dictlevel4.Add(reader[0].ToString(), index);
                            Console.WriteLine(String.Format("{0}, {1}",
                                reader[0], reader[1]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                // Now read the iput file - the csv
                // open file
                StreamReader sr = new StreamReader(new FileStream(strInFile, FileMode.Open, FileAccess.Read));
                // skip the first line as it is column headers
                sr.ReadLine();
                // now walk the file buiding insert strings and sending them to the db.
                // In this case we will switch on the SiteType value and put "Farm" sites in 
                // the FarmSite table and Wild sites and Planted sites in the CollectionSite table.  Note that
                // wild sites and planted sites have ecoregions but farms have none in the input data nor their
                // corresponding table.  For "planted" sites we will insert the word "planted" in the Notes field.
                string strLine = " ";
                int rowID = 0;
                int rowsAdded = 0;
                //
                // for each line
                while (null != strLine)
                {
                    rowID++;
                    strLine = sr.ReadLine();
                    if (null == strLine) break;
                    // remove the quotation marks from the fields
                    strLine = strLine.Replace('"', ' ');
                    // split into an array of the fields
                    fields = strLine.Split(',');
                    // remove leading and trailing spaces
                    // also escape any single quotes in the text with another single quote
                    for (int i = 0; i < fields.Length; ++i)
                    {
                        fields[i] = fields[i].Trim(' ');
                        fields[i] = fields[i].Replace("'", "''");
                    }
                    // Input file has "SiteName","SiteCode","Ecoregion","SiteType"
                    // FarmSite Table has FarmSiteID,FarmCode,FarmName
                    // CollectionSite Table has CollectionSiteID,LevelIVRegionID,Notes,SiteCode,SiteName
                    //
                    // mapping is:
                    // Table Col,               File Col
                    //================          =================
                    // FARMSITE
                    // FarmSiteID               ignore
                    // FarmCode                 SiteCode (1)
                    // FarmName                 SiteName (0)
                    // COLLECTIONSITE
                    // CollectionSiteID         ignore
                    // Level4RegionID           Must Look up Level4RegionName(2) from Level4 Region table
                    // Notes                    ignore
                    // SiteCode                 SiteCode (1)
                    // SiteName                 SiteName(0)
                    //
                    /// Switch on FileColumn SiteType (3)

                    // build the insert statement
                    string strInsert = "INSERT INTO ";
                    switch (fields[3])
                    {
                        case Constants.Wild:
                        case Constants.Planted:
                            strInsert += "CollectionSite (Level4RegionID,Notes,SiteCode,SiteName) ";
                            strInsert += "VALUES(";
                            int reg4ID = 0;
                            try {
                                if (0 == fields[2].Length)
                                    fields[2] = "Unknown";
                                reg4ID = dictlevel4[fields[2]];  //Level4RegionID
                            }
                            catch (Exception ex)
                            {
                                System.Console.WriteLine("{0} {1}", ex.Message,fields[2]);
                                Environment.Exit(-1);
                            }
                            strInsert += reg4ID.ToString(); // Level4RegionID
                            if(Constants.Planted == fields[3])  // Notes
                            {
                                strInsert += ", " + "\'" + "Planted" + "\'";       // 'Name'
                            }
                            else
                            {
                                strInsert += ", " + "\'" + " " + "\'";       // 'Name'
                            }
                            // last two fields are same for both site types
                            strInsert += ", ";
                            break;
                        case Constants.Farm:
                            strInsert += "FarmSite (FarmCode,FarmName) ";
                            strInsert += "VALUES(";
                            break;
                        default:
                            System.Console.WriteLine("Unregognized site type {0}", fields[3]);
                            Environment.Exit(-1);
                            break;
                            
                    }
                    strInsert +="\'" + fields[1] + "\'";                // 'SiteCode'
                    strInsert += ", " + "\'" + fields[0] + "\'";        // 'SiteName'
                    strInsert += ");";                                 // );

                   // show the command we will then execute
                    Console.WriteLine(strInsert);
                    command.CommandText = strInsert;
                    int rows = command.ExecuteNonQuery();
                    rowsAdded += rows;
                }
                Console.WriteLine("Added {0} rows.", rowsAdded);


            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                Environment.Exit(-1);
            }

        }

    }

    // string constants not really necessary but good practice for things that could change and
    // to prevent mistyping in comparisons
public static class Constants
    {
        public const string Planted = "Planted";
        public const string Farm = "Farm";
        public const string Wild = "Wild";
    }
}
