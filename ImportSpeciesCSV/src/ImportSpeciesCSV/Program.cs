﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ImportSpeciesCSV
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                System.Console.WriteLine("Usage: ImportSpeciesCSV inputCSVfile");
                Environment.Exit(0);
            }
            string strInFile = args[0];
            string[] fields = null;
            try
            {
                // connect to database
                string connString;
                connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                if (null == connString)
                    connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                SqlConnection connection = new SqlConnection(connString);
                connection.Open();
                // Now read the iput file - the csv
                // open file
                StreamReader sr = new StreamReader(new FileStream(strInFile, FileMode.Open, FileAccess.Read));
                // skip the first line as it is column headers
                sr.ReadLine();
                // now walk the file buiding insert strings and sending them to the db.
                string strLine = " ";
                int rowID = 0;
                int rowsAdded = 0;
                //
                // for each line
                while (null != strLine)
                {
                    rowID++;
                    strLine = sr.ReadLine();
                    if (null == strLine) break;
                    // remove the quotation marks from the fields
                    strLine = strLine.Replace('"', ' ');
                    // split into an array of the fields
                    fields = strLine.Split(',');
                    // remove leading and trailing spaces
                    for (int i = 0; i < fields.Length; ++i)
                    {
                        fields[i] = fields[i].Trim(' ');
                    }
                    // Input file has "ScientificName"(0),"Genus(1)","Species(2)","sub-taxa(3)","CommonName(4)","FourLetterCodon(5)","SixLetterCodon(6)","Synonyms(7)","LifeCycle(8)","RootStructure(9)","Clade(10)","FunctionalGroup(11)","SeedsPerPound(12)"
                    // Table has SpeciesID,Clade,Codon,Genus,LifeCycle,PctCoverOneAdult,RestorationCharacteristics,RootStructure,SeedDormancyType,SubTaxa,Synonyms,SpeciesName,CommonName
                    //
                    // mapping is:
                    // Table Col,                   File Col
                    //================              =================
                    // SpeciesID                    ignore
                    // Clade                        Clade (10)
                    // Codon                        SixLetterCodon(6)
                    // Genus                        Genus (1)
                    // LifeCycle                    LifeCycle (8)
                    // PctCoverOneAdult             ignore
                    // RestorationCharacteristics   ignore
                    // RootStructure                RootStructure (9)
                    // SeedDormancyType             ignore
                    // SubTaxa                      SubTaxa (3)
                    // Synonyms                     Synonyms (7)
                    // SpeciesName                  Species (2)
                    // CommonName                   CommonName (4)
                    //
                    // build the insert statement
                    string strInsert = "INSERT INTO Species (Clade,Codon," +
                        "Genus,LifeCycle,RootStructure,SubTaxa,Synonyms," +
                        "SpeciesName,CommonName,PctCoverOneAdult,RestorationCharacteristics) VALUES(";
                    //strInsert += rowID.ToString();
                    strInsert += "'" + fields[10] + "',"; // Clade
                    strInsert += "'" + fields[6] + "',"; // Codon
                    strInsert += "'" + fields[1] + "',"; // Genus
                    strInsert += "'" + fields[8] + "',"; // LifeCycle
                    strInsert += "'" + fields[9] + "',"; // RootStructure
                    strInsert += "'" + fields[3] + "',"; // SubTaxa
                    strInsert += "'" + fields[7] + "',"; // Synonyms
                    strInsert += "'" + fields[2] + "',"; // Species
                    strInsert += "'" + fields[4] + "',"; // CommonName
                    strInsert += "0.0,";                 // PctCoverOneAdult
                    strInsert += "0";                    // RestorationCharacteristics
                    strInsert += ");";                                 // );
                    // show the command we will then execute
                    Console.WriteLine(strInsert);
                    SqlCommand command = new SqlCommand(strInsert, connection);
                    int rows = command.ExecuteNonQuery();
                    rowsAdded += rows;
                }
                Console.WriteLine("Added {0} rows.", rowsAdded);


            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

        }

    }
}
