﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
//


// Note:
// This program was written to import an export from the SQL Server local Species table
// so the column order, etc. is different from a CSV file from the Access DB

namespace importSpecies
{
    public class Program
    {
        public static void Main(string[] args)
        {

            if (args.Length < 1)
            {
                System.Console.WriteLine("Usage: ImportSpecies inputCSVfile");
                Environment.Exit(0);
            }
            string strInFile = args[0];
            string[] fields = null;
            try
            {
                // connect to database
                string connString;
                connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                if (null == connString)
                    connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                SqlConnection connection = new SqlConnection(connString);
                SqlCommand command = new SqlCommand("", connection);
                connection.Open();
                // Now read the iput file - the csv
                // open file
                StreamReader sr = new StreamReader(new FileStream(strInFile, FileMode.Open, FileAccess.Read));
                // now walk the file buiding insert strings and sending them to the db.
                string strLine = " ";
                int rowID = 0;
                int rowsAdded = 0;
                //
                // for each line
                while (null != strLine)
                {
                    rowID++;
                    strLine = sr.ReadLine();
                    if (null == strLine) break;
                    // split into an array of the fields
                    fields = strLine.Split(',');
                    for (int i = 0;i< fields.Length;++i)
                    {
                        if ("NULL" == fields[i])
                            fields[i] = "";
                    }
                    string strInsert = "INSERT INTO Species (Clade,Codon,Genus,LifeCycle," +
                        "PctCoverOneAdult,RestorationCharacteristics,RootStructure," +
                         "SeedDormancyType,SubTaxa,Synonyms,SpeciesName,CommonName) VALUES(";
                    // skip field[0], its the id
                    strInsert += "'" + fields[1] + "', ";                // 'clade'
                    strInsert += "'" + fields[2] + "', ";                // 'codon'
                    strInsert += "'" + fields[3] + "', ";                // 'genus'
                    strInsert += "'" + fields[4] + "', ";                // 'lifecycle'
                    strInsert += fields[5] + ", ";                          // 'pctcover'
                    strInsert += fields[6] + ", ";                          // 'restchar'
                    strInsert += "'" + fields[7] + "', ";                // 'rootstruct'
                    strInsert += "'" + fields[8] + "', ";                // 'SeedDorm'
                    strInsert += "'" + fields[9] + "', ";                // 'subtaxa'
                    strInsert += "'" + fields[10] + "', ";                // 'synonyms'
                    strInsert += "'" + fields[11] + "', ";                // 'specName'
                    strInsert += "'" + fields[12] + "'";                // 'CommonName'
                    strInsert += ");";                                 // );

                    // show the command we will then execute
                    Console.WriteLine(strInsert);
                    command.CommandText = strInsert;
                    int rows = command.ExecuteNonQuery();
                    rowsAdded += rows;
                }
                Console.WriteLine("Added {0} rows.", rowsAdded);


            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
                Environment.Exit(-1);
            }

        }

    }

    // string constants not really necessary but good practice for things that could change and
    // to prevent mistyping in comparisons
    public static class Constants
    {
        public const string Planted = "Planted";
        public const string Farm = "Farm";
        public const string Wild = "Wild";
    }
}
