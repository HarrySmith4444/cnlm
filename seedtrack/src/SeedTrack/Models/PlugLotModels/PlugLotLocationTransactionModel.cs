﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugLotLocationTransaction
    {
        [Key]
        public int PlugLotID { get; set; }
        public int PlugLotLocationID { get; set; }
        public PlugLocation PlugLocation {get; set;}

        [Display(Name ="Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:g}")]
        public DateTime PlugLotMovementDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        [MaxLength(450)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
