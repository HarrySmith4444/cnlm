﻿using SeedTrack.Models.SeedLotModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugLot
    {
        public int PlugLotID { get; set; }
        public int PlugLotNumber { get; set; }
        public int SeedLotID { get; set; }
        public SeedLot SeedLot { get; set; }
        [MaxLength(40)]
        [Display(Name = "Plug Lot Tag")]
        public string PlugLotTag { get; set; }
        [Display(Name = "# of Seeds per Plug")]
        public int? SeedsPerPlug { get; set; }
        [Display(Name = "Soil Mix")]
        public int? PlugSoilID { get; set; }
        public PlugSoil PlugSoil { get; set; }
        [Display(Name = "Tray Type")]
        public int? PlugContainerID { get; set; }
        public PlugContainer PlugContainer { get; set; }
        [Display(Name = "Seed Cover Used")]
        public eSoilSurfaceCover SeedCover { get; set; }
        [Display(Name = "Grams Used")]
        public float? SeedQuantityUsed { get; set; }
        [Display(Name = "Seed Pretreatment")]
        public string SeedPretreatment { get; set; }
        [Display(Name = "Imbibe Time (hrs)")]
        public int? ImbibeTime { get; set; }
        [Display(Name = "Cold Strat Temp (F)")]
        public int? ColdStratTemp {get; set;}
        [Display(Name = "Grower")]
        public int OrganizationID { get; set; }
        public Organization Organization { get; set; }
        public int? CurrentQuantity { get; set; }
        public string Notes { get; set; }
        public ePluglotState State { get; set; }
        [Display(Name = "Plug Lot Transactions")]
        public virtual ICollection<PlugLotTransaction> PlugLotTransactions { get; set; }
        [Display(Name = "Created By")]
        [MaxLength(450)]
       public string CreatedBy { get; set; }
        [Display(Name = "Creation Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:g}")]
        public DateTime CreationDate { get; set; }
        [MaxLength(450)]
        [Display(Name = "Last Modified By")]
        public string ModifiedBy { get; set; }
        [Display(Name = "Last Modified Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:g}")]
        public DateTime? ModifiedDate { get; set; }


    }
    public enum eSoilSurfaceCover { NoCover = 0, Soil = 1, Grit = 2, SoilAndGrit = 3, IntoGrit = 4 }
    public enum ePluglotState { ColdStrat = 0, Sowed = 1};
    public class PlugLotSearch
    {
        public int iPlugLotNumber { get; set; }
    }
}

