﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugContainer
    {
        public int PlugContainerID { get; set; }
        [Display(Name = "Container Type")]
        public string PlugContainerName { get; set; }
        [Display(Name = "Volume (in3)")]
        public float PlugVolume { get; set; }
        [Display(Name = "Plugs per Tray")]
        public int PlugsPerTray { get; set; }
    }
}
