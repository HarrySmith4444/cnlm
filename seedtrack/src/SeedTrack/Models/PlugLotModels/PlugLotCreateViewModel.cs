﻿using SeedTrack.Models.SeedLotModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugLotCreateViewModel
    {
        public SeedLot SeedLot { get; set; }
        public PlugLot PlugLot { get; set; }
        public Organization Organization { get; set; }
        [Display(Name = "Tray Type")]
        public int PlugContainerID { get; set; }
        public PlugContainer PlugContainer { get; set; }
        [Display(Name = "Seed Cover Used")]
        public eSoilSurfaceCover SeedCover { get; set; }
        [Display(Name = "Grams Used")]
        public float? SeedQuantityUsed { get; set; }
        [Display(Name = "Seed Pretreatment")]
        public string SeedPretreatment { get; set; }
        [Display(Name = "Grower")]
        public int OrganizationID { get; set; }
        [Display(Name ="Seed Lot")]
        public int SeedLotNumber { get; set; }
        public int SeedLotID { get; set; }
        public string LotTag { get; set; }
        [Display(Name = "Soil Mix")]
        public int PlugSoilID { get; set; }
        [Display(Name = "# of Seeds per Plug")]
        public int SeedsPerPlug { get; set; }
        public int? SelectedSeedLot { get; set; }

    }
}
