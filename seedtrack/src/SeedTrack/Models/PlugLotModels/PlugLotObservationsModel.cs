﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugLotObservations
    {   [Key]
        public int PlugLotID { get; set; }
        public PlugLot PlugLot { get; set; }
        public int? Germinated { get; set; }
        public int? Established { get; set; }
        public int? Overgrown { get; set; }

        [Display(Name ="Date")]
        public DateTime PlugLotObservationDate { get; set; }
        [Display(Name = "Notes")]
        public string PlugLotObservation { get; set; }


    }
}
