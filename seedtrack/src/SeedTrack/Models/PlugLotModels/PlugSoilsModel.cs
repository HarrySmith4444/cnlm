﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugSoil
    {
        public int PlugSoilID { get; set; }
        [Display(Name = "Soil Name")]
        public string PlugSoilName { get; set; }
        [Display(Name = "Soil Description")]
        public string PlugSoilDescription { get; set; }

    }
    public class PlugSoilComponent
    {   public int PlugSoilComponentID { get; set; }
        [Display(Name = "Component Name") ]
        public string PlugSoilComponentName { get; set; }
        [Display(Name = "Component Description")]
        public string PlugSoilComponentDescription { get; set; }

    }
    public class PlugSoilFormula
    {
        [Key]
        public int PlugSoilID { get; set; }
        public PlugSoil PlugSoil { get; set; }
        public int PlugSoilComponentID { get; set; }
        public PlugSoilComponent PlugSoilComponent { get; set; }
        [Display(Name = "Percent of Mix")]
        public float PlugSoilComponentPercentage { get; set; }
    }
         
}
