﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeedTrack.Models.SeedLotModels;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugLotTransaction
    {
        public int PlugLotTransactionID { get; set; }
        public int? PlugLotID { get; set; }
        public PlugLot PlugLot { get; set; }
        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:MM/dd/yy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime TransactionDate { get; set; }
        [Display(Name = "Action")]
        public PlugLotTransactionType TransactionType { get; set; }

        public int? PlugQuantity { get; set; }

        // Other data that will come with the transactions
        // goes here
        
        // seedlot is not transacted, just created.
        //public int? SeedLotID { get; set; }
        //public SeedLot SeedLot { get; set; }

        // From / To for "Returned
        public int? OrganizationID { get; set; }
        public Organization Organization { get; set; }

        public int? PersonID { get; set; }
        public Person Person { get; set; }

        [Column(TypeName = "ntext")]
        public string Notes { get; set; }

        [MaxLength(450)]
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        [MaxLength(450)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }


        public enum PlugLotTransactionType { Sowed = 0, Dumped = 1, Allocated = 2, Received = 3, Returned = 4, ColdStrat = 5 }
    }
}
