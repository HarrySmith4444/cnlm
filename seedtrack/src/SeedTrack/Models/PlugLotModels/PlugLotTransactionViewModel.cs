﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugLotTransactionViewModel
    {
        public PlugLot pl { get; set; }
        public PlugLotTransaction plt { get; set; }
        // Coldstrat pluglot
        public int? csplID { get; set; }

    }

    public class PlugLotTransFlags
    {
        public int iFromColdStrat;
         
    }
}
