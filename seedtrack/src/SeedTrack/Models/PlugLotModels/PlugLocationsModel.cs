﻿using SeedTrack.Models.SeedLotModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugLocation
    {
        public int PlugLocationID { get; set; }
        [Display(Name= "Location" )]
        public string PlugLocationName { get; set; }
        [Display(Name = "Farm Site")]
        public string FarmSiteID { get; set; }
        public FarmSite FarmSite { get; set; }
    }
}
