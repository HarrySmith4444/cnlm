﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.PlugLotModels
{
    public class PlugMaintenanceAction
    {
        [Key]
        public int PlugLotID { get; set; }
        public PlugLot PlugLot { get; set; }
        [Display (Name ="Date")]
        public DateTime PlugMaintenanceActionDate { get; set; }
        public enum PlugMaintenanceActionType { Weed=0, OverheadIrrigation=1, HandWater=2, Fertilize=3, Thin=4}
        [Display(Name = "Notes")]
        public string PlugMaintenanceActionNotes { get; set; }
        
    
    }
}
