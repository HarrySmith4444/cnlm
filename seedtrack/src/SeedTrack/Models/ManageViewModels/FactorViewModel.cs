﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.ManageViewModels
{
    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }
}
