﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace SeedTrack.Models.ManageViewModels
{
    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }

        public IList<AuthenticationDescription> OtherLogins { get; set; }


    }
    public class NamedIdentityUserRole : IdentityUserRole<String>,IComparable<NamedIdentityUserRole> // where TKey : IEquatable<TKey>
    {
        public string RoleName { get; set; }
        public string UserID;
        public string Email;
        public string RoleID;
        public NamedIdentityUserRole(string roleName,string roleID,string userID,string email)
            {
                RoleName = roleName;
                RoleId = roleID;
                UserId = userID;
                Email = email;
            }

        public int CompareTo (NamedIdentityUserRole other) { 
            return this.UserId.CompareTo(other.UserId);
            }

    }

    public class ManageUserRolesViewModel
    {
        public IList<ApplicationUser> AllUsers { get; set; }
        public IList<IdentityRole> AllRoles { get; set; }
        public SortedList< NamedIdentityUserRole, IdentityUser> AllUserRoles { get; set; }
        public string SelectedRoleName { get; set; }
        public string SelectedUserName { get; set; }
    }
}
