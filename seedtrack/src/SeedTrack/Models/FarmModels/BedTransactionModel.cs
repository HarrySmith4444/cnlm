﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeedTrack.Models.SeedLotModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeedTrack.Models.PlugLotModels;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace SeedTrack.Models.FarmModels
{
    public class BedTransaction
    {
        public int BedTransactionID { get; set; }
        public int? FarmBedID { get; set; }
        public FarmBed FarmBed { get; set; }
        [Range(1, 6, ErrorMessage = "Rows per bed must be between 1 and 6")]
        [Display(Name = "Rows per Bed")]
        public int RowsPerBed { get; set; }
        [Display(Name = "Plant Spacing")]
        [Range(1, 24, ErrorMessage = "Plant spacing must be between 1 and 24 in.")]
        public int? PlantSpacing { get; set; }
        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:MM/dd/yy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime TransactionDate { get; set; }
        [MaxLength(450)]
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        [MaxLength(450)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        [Display(Name = "Action")]
        public BedTransactionType TransactionType { get; set; }
        public int? SeedLotID { get; set; }
        public SeedLot SeedLot { get; set; }
        public int? PlugLotID { get; set; }
        public PlugLot PlugLot { get; set; }
        // setting upper limit to bedfeet planting entry to 500.  check with Sierra
        [TotalBedFeet(500)]
        public int BedFeet { get; set; }
        [Range(.01, 100000.0, ErrorMessage = "Grams used must be at least .01")]
        [Display(Name = "Grams Used")]
        public float? SeedQuantity { get; set; }
        [Range(1, 100000, ErrorMessage = "Number of plugs must be > 0")]
        [Display(Name = "# of Plugs Used")]
        public int? PlugQuantity { get; set; }
        [Column(TypeName = "ntext")]
        public string Notes { get; set; }

        // constructor sets up our initial conditions
        public BedTransaction()
        {
            this.BedFeet = Constants.DEFAULT_BED_FEET_PLANTED;
            //this.FarmBedID = 0;
            this.PlantSpacing = Constants.DEFAULT_PLANT_SPACING;
            this.PlugLotID = 0;
            this.SeedLotID = 0;
            this.PlugQuantity = Constants.DEFAULT_PLUG_QUANTITY;
            this.RowsPerBed = Constants.DEFAULT_ROWS_PER_BED;
            this.SeedQuantity = Constants.DEFAULT_SEED_QUANTITY;
            this.TransactionType = BedTransactionType.Planted;
            this.TransactionDate = DateTime.Now;

        }
    }
    public enum BedTransactionType { Planted=0, Sowed=1, Cleared=2, Removed=3 }

    public class TotalBedFeet : ValidationAttribute, IClientModelValidator
    {
        private int _bedFeet;

        public TotalBedFeet(int bedFeet)
        {
            _bedFeet = bedFeet;
        }
        public void AddValidation(ClientModelValidationContext context)
        {
            // Not sure what this should do but it's required
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            BedTransaction bt = (BedTransaction)validationContext.ObjectInstance;
            if (bt.BedFeet > _bedFeet)
            {
                return new ValidationResult("Entered bed feet is too large.");
            }

            return ValidationResult.Success;
        }
    }
}
