﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.FarmModels
{
    public class BedTransactionViewModel
    {
        public BedTransaction bedTransaction { get; set; }
        public int? farmID { get; set; }
        public int iSelectedFarm { get; set; }
        public int iSelectedBed { get; set; }
        public int iSelectedBedID { get; set; }
        public int? bedID { get; set; }
        public string farmName { get; set; }
        public string bedTag { get; set; }
        public int? iSelectedAction {get;set;}
        public int? iTotalBedFeet { get; set; }

        // not used at the moment
        public IList<Plantings> lstPlantings { get; set; }
        public BedTransactionViewModel()
        {
            bedTransaction = new BedTransaction();
            farmID = 0;
            bedID = 0;
            iSelectedFarm = 0;
            iSelectedBed = 0;
            iSelectedBedID = 0;
            iSelectedAction = (int)BedTransactionType.Planted;
            iTotalBedFeet = 0;

        }
        public BedTransactionViewModel Clone()
        {
            BedTransactionViewModel mout = new BedTransactionViewModel();
            mout.farmID = this.farmID;
            mout.bedID = this.bedID;
            mout.iSelectedFarm = this.iSelectedFarm;
            mout.iSelectedBed = this.iSelectedBed;
            mout.iSelectedBedID = this.iSelectedBedID;
            mout.bedTransaction.BedFeet = this.bedTransaction.BedFeet;
            mout.bedTransaction.BedTransactionID = 0;
            mout.bedTransaction.FarmBedID = this.bedTransaction.FarmBedID;
            mout.bedTransaction.Notes = this.bedTransaction.Notes;
            mout.bedTransaction.PlantSpacing = this.bedTransaction.PlantSpacing;
            mout.bedTransaction.PlugLotID = this.bedTransaction.PlugLotID;
            mout.bedTransaction.PlugQuantity = this.bedTransaction.PlugQuantity;
            mout.bedTransaction.RowsPerBed = this.bedTransaction.RowsPerBed;
            mout.bedTransaction.SeedLotID = this.bedTransaction.SeedLotID;
            mout.bedTransaction.SeedQuantity = this.bedTransaction.SeedQuantity;
            mout.bedTransaction.TransactionDate = this.bedTransaction.TransactionDate;
            mout.bedTransaction.TransactionType = this.bedTransaction.TransactionType;
            mout.farmName = this.farmName;
            mout.bedTag = this.bedTag;
            mout.bedTransaction.CreatedBy = this.bedTransaction.CreatedBy;
            mout.bedTransaction.CreationDate = this.bedTransaction.CreationDate;
            mout.bedTransaction.ModifiedBy = this.bedTransaction.ModifiedBy;
            mout.bedTransaction.ModifiedDate = this.bedTransaction.ModifiedDate;
            return (mout);
        }

    }

    public class Plantings
    {
        public int? seedLotID { get; set; }
        public float? grams { get; set; }

    }
    public  class BedIDModel
    {
        public int? bedID { get; set; }
    }

}
