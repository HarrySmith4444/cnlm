﻿using SeedTrack.Models.SeedLotModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.SeedLotTransactionModels
{
    public class SeedLotTransactionViewModel2
    {
        public SeedLotTransaction slt { get; set; }
        public Organization org;
        public SeedLot sl { get; set; }
        public Species species { get; set; }

    }
}
