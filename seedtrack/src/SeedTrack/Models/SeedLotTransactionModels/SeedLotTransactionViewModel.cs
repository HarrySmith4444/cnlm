﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeedTrack.Models.SeedLotModels;

namespace SeedTrack.Models.SeedLotTransactionModels
{
    public class SeedLotTransactionViewModel
    {
        public SeedLotTransaction slt { get; set; }
        public Dictionary<int, string> dictPersonIDOrgName;
        public SeedLot sl { get; set; }
        public float fCurrentTotal { get; set; }
        public SeedLotTransactionViewModel(Dictionary<int, string> dict,SeedLot slIn)
        {
            sl = slIn;
            slt = new SeedLotTransaction();
            slt.PersonID = 0;
            slt.TransactionDate = DateTime.Now;
            dictPersonIDOrgName = dict;
           
        }
    }
}
