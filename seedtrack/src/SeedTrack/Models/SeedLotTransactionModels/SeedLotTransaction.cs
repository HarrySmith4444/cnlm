﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using SeedTrack.Models.SeedLotModels;
using System.ComponentModel.DataAnnotations;

namespace SeedTrack.Models.SeedLotTransactionModels
{
    public class SeedLotTransaction
    {
        public int SeedLotTransactionID { get; set; }
        public int SeedLotID { get; set; }
        [Display(Name = "Type")]
        public SeedLotTransactionType TransactionType { get; set; }
        [Display(Name = "Amount")]
        public float Quantity { get; set; }
        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{0:MM/dd/yy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Date")]
        public DateTime TransactionDate { get; set; }
        public int? OrganizationID {get;set;}
        public Organization Organization { get; set; }
        [Column(TypeName = "ntext")]
        public string Notes { get; set; }
        [MaxLength(450)]
        public string CreatedBy { get; set; }
        public DateTime CreationDate { get; set; }
        [MaxLength(450)]
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int? PersonID {get; set;}
        [Display(Name = "Person")]
        public Person Person { get; set; }
        public int? OldTransactionID { get; set; }
    }
    public enum SeedLotTransactionType { Harvest,WildCollection,ReceiveFromPartner,Disbursement,Discarded,Returned,InventoryCorrection}
}
