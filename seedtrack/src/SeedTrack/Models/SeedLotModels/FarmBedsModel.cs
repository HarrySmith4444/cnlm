﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.SeedLotModels
{
    public class FarmBed
    {
        //public FarmBed()
        //{
        //    this.SeedLots = new HashSet<SeedLot>();
        //}

        public int FarmBedID { get; set; }
        public int FarmSiteID { get; set; }
        // FarmBedTag is only unique per farm and is tagged on actual beds
        [Display(Name = "Farm Bed Tag")]
        public string FarmBedTag { get; set; }
        public FarmSite FarmSite{ get; set; }
        [Display(Name = "Bed Type")]
        [DisplayFormat(DataFormatString ="{0}")]
        public string BedType { get; set; }
        [Display(Name = "Bed Length")]
        [DisplayFormat(DataFormatString = "{0}")]
        public float Length { get; set; }
        [DisplayFormat(DataFormatString = "{0}")]
        [Display(Name = "Bed Width")]
        public float Width { get; set; }
        [Display(Name = "Irrigation Type")]
        public string IrrigationType { get; set; }
        [Display(Name = "Irrigation Flow Rate")]
        [DisplayFormat(DataFormatString ="{0}")]
        public float IrrigationFlowRate { get; set; }
        public List<SeedLotFarmBed> SeedLotFarmBeds { get; set; }
        //public virtual ICollection<SeedLot> SeedLots { get; set; }
    }
    public class SeedLotFarmBed
    {
       // [Key]
        public int SeedLotID { get; set; }
        public SeedLot SeedLot { get; set; }
        public int FarmBedID { get; set; }
        public FarmBed FarmBed { get; set; }
    }
    public class FarmSite
    {
        public int FarmSiteID { get; set; }
        [MaxLength(120)]
        [Display(Name = "Farm Name")]
        public string FarmName { get; set; }
        [MaxLength(10)]
        [Display(Name = "Farm Code")]
        public string FarmCode { get; set; }
    }
    public enum BedTypes
    {
        [Display(Name = "Farm Bed")]
        FarmBed,
        [Display(Name = "Nursery Bed")]
        NurseryBed
    }
    public enum IrrigationTypes
    {
        [Display(Name = "Overhead")]
        Overhead,
        [Display(Name = "Drip")]
        Drip
    }
}
