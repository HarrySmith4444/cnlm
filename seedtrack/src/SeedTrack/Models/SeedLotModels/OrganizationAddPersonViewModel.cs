﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeedTrack.Controllers;
using System.ComponentModel.DataAnnotations;

namespace SeedTrack.Models.SeedLotModels
{
    public partial class OrganizationAddPersonViewModel
    {
        // the Org we are adding people to
        public Organization organization { get; set; }
        // people choices
        public SelectList userSelectList { get; set; }
        // the list they will be returned in
        [Display(Name = "Select people to assign")]
        public List<int> selectedIDs { get; set; }
        [Display (Name ="Select a person to add")]

        public int orgID { get; set; }
        public OrganizationAddPersonViewModel() {
        }
        public OrganizationAddPersonViewModel(Organization org, List<SelectListItem> personList)
        {
            organization = org;
            orgID = org.OrganizationID;
            userSelectList = new SelectList(personList, "Value", "Text",selectedIDs);
        }
    }
}
