﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SeedTrack.Models.SeedLotTransactionModels;


namespace SeedTrack.Models.SeedLotModels
{
    public class SeedLotCreateViewModel
    {
        public SeedLot sl { get; set; }
        public SeedLotTransaction slt { get; set; }
        public int siteID1 {get;set;}
        public int percent1 { get; set; }
        public int siteID2 { get; set; }
        public int percent2 { get; set; }
        public int siteID3 { get; set; }
        public int percent3 { get; set; }
        public int siteID4 { get; set; }
        public int percent4 { get; set; }
        public int siteID5 { get; set; }
        public int percent5 { get; set; }
        public int siteID6 { get; set; }
        public int percent6 { get; set; }

        //public int[] sourceSites { get; set; }
        //public int[] sourcePercents { get; set; }
        //public SLOrigSource []slos { get; set; }
    }
}
