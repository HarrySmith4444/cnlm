﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.SeedLotModels
{
    public class SLProvenance
    {
        // each row in SLProvenance represents an original source of a seedlot. 
        public int SLProvenanceID { get; set; }
        public int seedLotID { get; set; }  // the seedlot whose provenance we are referring to
        public int sourceID { get; set; }   // ??? do we need this : the seedlotID of the piece of the provenance described in this record
        public int speciesID { get; set; }  // what it is.  Used to combine identical sources as well as viewing
        public virtual Species Species { get; set; }
        public int collectionSiteID { get; set; }  // where it came from - could be "unknown"
        public virtual CollectionSite CollectionSite { get; set; }
        public int generation { get; set; }  // farmed seed may be set by user. 
        public float percentContribution { get; set; } // How much of the seedlot is from this component. 0-1.0

    }

    // items are used when searching contents of beds to construct the provenance records above
    public class SLProvenanceItem
    {
        public int seedLotID { get; set; }
        public float percentContribution { get; set; }
        public int speciesID { get; set; }  

    }
    public class SLBedProvenance
    {
        public List<SLProvenanceItem> lstProv { get; set; }
        public float bedFeetPlanted { get; set; }
    }
    public class SLOrigSource
    {
        public int siteID { get; set; }
        public float percent { get; set; }
        public SLOrigSource()
        {
            siteID = 0;
            percent = (float)0.0;
        }

    }
}
