﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.SeedLotModels
{
    public class FarmLotViewModel
    {
      public  FarmBed farmBed { get; set; }
       public  FarmLotViewModel(FarmBed bed)
        {
            farmBed = bed;
        }
    }
}
