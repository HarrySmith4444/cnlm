﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.SeedLotModels
{
    public class Level3Region
    {
        [Display(Name = "Level III Region")]
        public int Level3RegionID {get; set;}
        [MaxLength(120)]
        [Display(Name = "Level III Region Name")]
        public string Name {get; set;}
        [Display(Name="Level III Region Code" )]
        public int Code { get; set; }
    }
    public class Level4Region
    {
        [Display(Name = "Level IV Region")]
        public int Level4RegionID {get; set;}

        [MaxLength(120)]
        [Display(Name = "Level IV Region Name")]
        public string Name {get; set;}
        [MaxLength(10)]
        [Display(Name = "Level IV Region Code")]
        public string RegionCode {get; set;}
        [MaxLength(10)]
        [Display(Name = "Level IV/III Region Code")]
        public string CombinedRegionCode { get; set; }
        public int Level3RegionID {get; set;}
        public Level3Region Level3Region { get; set; }

    }
    public class SeedLotRegion
    {
        [Key]
        public int SeedLotID {get; set;}
        public int Level4RegionID {get; set;}
    }
    public class CollectionSite
    {
        [Display(Name = "Collection Site")]
        public int CollectionSiteID {get; set;}
        [MaxLength(120)]
        [Display(Name = "Collection Site Name")]
        public string SiteName { get; set; }
        [MaxLength(10)]
        [Display(Name = "Collection Site Code")]
        public string SiteCode { get; set; }
        [Display(Name = "Level IV Region")]
        public int Level4RegionID { get; set; }
        [Display(Name = "Level IV Region")]
        public Level4Region Level4Region { get; set; }
        [Column(TypeName = "ntext")]
        [Display(Name = "Notes")]
        public string Notes { get; set; }
    }
}
