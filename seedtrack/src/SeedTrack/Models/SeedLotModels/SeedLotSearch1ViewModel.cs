﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.SeedLotModels
{
    public class SeedLotSearch1ViewModel
    {
        [Display(Name = "Seedlots")]
        public IEnumerable<SeedTrack.Models.SeedLotModels.SeedLot> iEnumResults { get; set; }
        [Display(Name = "Species")]
        public int iSelectedSpeciesID { get; set; }
        [Display(Name = "Lot #")]
        public string strSeedLotNumber { get; set;}
        public int searchType { get; set; }
        [Display(Name = "Source")]
        public int iSourceType { get; set; }
        public int iShowError { get; set; }
        public string strErrorMsg { get; set; }
        [Display(Name = "Sort By")]
        public string strSortBy { get; set; }


        public  SeedLotSearch1ViewModel()
        {
            // default constructor overridden to set sourceType (farm vs. collected) to 
            // blank
            iSourceType = -1;
        }
    }
}
