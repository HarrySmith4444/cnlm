﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.SeedLotModels
{
    public class Organization
    {
        public int OrganizationID { get; set; }
        [MaxLength(120)]
        [Display (Name = "Organization Name")]
        public string OrganizationName { get; set; }
        [Display (Name ="Department")]
        public string Department { get; set; }
        [Display (Name ="Acronym")]
        public string Acronym { get; set; }
        [MaxLength(20)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        [Display (Name = "Main Phone"),DataType("Phone")]
        public string PhoneNumber { get; set; }
        [Display(Name ="Ext")]
        public string Extension { get; set; }
        public int? ContactPersonID { get; set; }
        [ForeignKey("ContactPersonID")]
        public Person ContactPerson { get; set; }
        [Display(Name = "Address First Line")]
        public string FirstLine { get; set; }
        [MaxLength(80)]
        [Display(Name = "Address Second Line")]
        public string SecondLine { get; set; }
        [MaxLength(80)]
        public string City { get; set; }
        [MaxLength(60)]
        public string State { get; set; }
        [MaxLength(60)]
        public string Country { get; set; }
        [MaxLength(12)]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
    }
    public class Person
    {
        public int PersonID { get; set; }
        [MaxLength(80)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [MaxLength(80)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "Email address")]
//        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        [MaxLength(20), DataType("Phone")]
        public string Phone { get; set; }
        [MaxLength(20), DataType("Phone")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid Phone number")]
        [Display(Name = "Ext")]
        public string Extension { get; set; }
        [Display(Name = "Cell Phone")]
        public string CellPhone { get; set; }
        [InverseProperty("Persons")]
        public int? OrganizationID { get; set; }
        public Organization Organization { get; set; }
        [Display(Name ="Title")]
        public string Title { get; set; }
        public bool IsContactPerson { get; set; }
        [InverseProperty("ContactPerson")]
        public Organization ContactPersonFor { get; set; }
        [Display(Name = "Address First Line")]
        public string FirstLine { get; set; }
        [MaxLength(80)]
        [Display(Name = "Address Second Line")]
        public string SecondLine { get; set; }
        [MaxLength(80)]
        public string City { get; set; }
        [MaxLength(60)]
        public string State { get; set; }
        [MaxLength(60)]
        public string Country { get; set; }
        [MaxLength(12)]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

    }
 
    public enum Countries
    {
        [Display(Name = "United States")]
        USA,
        [Display(Name = "Canada")]
        CAN,
        [Display(Name = "Mexico")]
        MX,
    }
    public enum States
    {
           
        [Display(Name = "Alabama")]
        AL,

        [Display(Name = "Alaska")]
        AK,

        [Display(Name = "Arkansas")]
        AR,

        [Display(Name = "Arizona")]
        AZ,

        [Display(Name = "California")]
        CA,

        [Display(Name = "Colorado")]
        CO,

        [Display(Name = "Connecticut")]
        CT,

        [Display(Name = "D.C.")]
        DC,

        [Display(Name = "Delaware")]
        DE,

        [Display(Name = "Florida")]
        FL,

        [Display(Name = "Georgia")]
        GA,

        [Display(Name = "Hawaii")]
        HI,

        [Display(Name = "Iowa")]
        IA,

        [Display(Name = "Idaho")]
        ID,

        [Display(Name = "Illinois")]
        IL,

        [Display(Name = "Indiana")]
        IN,

        [Display(Name = "Kansas")]
        KS,

        [Display(Name = "Kentucky")]
        KY,

        [Display(Name = "Louisiana")]
        LA,

        [Display(Name = "Massachusetts")]
        MA,

        [Display(Name = "Maryland")]
        MD,

        [Display(Name = "Maine")]
        ME,

        [Display(Name = "Michigan")]
        MI,

        [Display(Name = "Minnesota")]
        MN,

        [Display(Name = "Missouri")]
        MO,

        [Display(Name = "Mississippi")]
        MS,

        [Display(Name = "Montana")]
        MT,

        [Display(Name = "North Carolina")]
        NC,

        [Display(Name = "North Dakota")]
        ND,

        [Display(Name = "Nebraska")]
        NE,

        [Display(Name = "New Hampshire")]
        NH,

        [Display(Name = "New Jersey")]
        NJ,

        [Display(Name = "New Mexico")]
        NM,

        [Display(Name = "Nevada")]
        NV,

        [Display(Name = "New York")]
        NY,

        [Display(Name = "Oklahoma")]
        OK,

        [Display(Name = "Ohio")]
        OH,

        [Display(Name = "Oregon")]
        OR,

        [Display(Name = "Pennsylvania")]
        PA,

        [Display(Name = "Rhode Island")]
        RI,

        [Display(Name = "South Carolina")]
        SC,

        [Display(Name = "South Dakota")]
        SD,

        [Display(Name = "Tennessee")]
        TN,

        [Display(Name = "Texas")]
        TX,

        [Display(Name = "Utah")]
        UT,

        [Display(Name = "Virginia")]
        VA,

        [Display(Name = "Vermont")]
        VT,

        [Display(Name = "Washington")]
        WA,

        [Display(Name = "Wisconsin")]
        WI,

        [Display(Name = "West Virginia")]
        WV,

        [Display(Name = "Wyoming")]
        WY

    }
    public class ManageOrganizationViewModel
    {
        public List<SelectListItem> StateList { get; set; }

    }

}
