﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models.SeedLotModels
{
    public partial class SeedLotViewModel
    {
        public SeedLot SeedLot{get;set;}
        public eSourceType eSourceType { get; set; }
        public CollectionSite CollectionSite { get; set; }
        public FarmSite FarmSite { get; set; }
 //       [Display(Name = "Current Amount")]
//        public float fltTotal { get; set; }
        public Dictionary<int,string> dictOrg { get; set; }
        public string FarmBedTag { get; set; }

        public SeedLotViewModel(SeedLot sl,eSourceType eType, CollectionSite colSite,FarmSite farmSite )
        {
            SeedLot = sl;
            eSourceType = eType;
            if(eSourceType.Farm == eSourceType || eSourceType.CNLMFarm == eSourceType)
            {
                FarmSite = farmSite;
            }else
            {
                CollectionSite = colSite;
            }
        }

    }
}
