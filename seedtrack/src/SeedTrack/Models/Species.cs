﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SeedTrack.Models
{
    public class Species
    {

        public int SpeciesID { get; set; }
        public string Genus { get; set; }
        public string SpeciesName { get; set; }
        public string SubTaxa { get; set; }
        public string CommonName { get; set; }
        public string Codon { get; set; }
        public string Synonyms { get; set; }
        public string LifeCycle { get; set; }
        public string RootStructure { get; set; }
        public string Clade { get; set; }
        public float PctCoverOneAdult { get; set; }
        public string SeedDormancyType { get; set; }
        public long RestorationCharacteristics { get; set; }

    }
}
