ALTER TABLE [SeedLotTransaction] DROP CONSTRAINT [FK_SeedLotTransaction_Organization_FromOrgID];
GO
ALTER TABLE [SeedLotTransaction] DROP CONSTRAINT [FK_SeedLotTransaction_Person_PersonID];
GO
ALTER TABLE [SeedLotTransaction] DROP CONSTRAINT [FK_SeedLotTransaction_Organization_ToOrgID];
GO
DROP INDEX [IX_SeedLotTransaction_FromOrgID] ON [SeedLotTransaction];
GO
DROP INDEX [IX_SeedLotTransaction_ToOrgID] ON [SeedLotTransaction];
GO
DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'SeedLotTransaction') AND [c].[name] = N'FromOrgID');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [SeedLotTransaction] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [SeedLotTransaction] DROP COLUMN [FromOrgID];
GO
DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'SeedLotTransaction') AND [c].[name] = N'ToOrgID');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [SeedLotTransaction] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [SeedLotTransaction] DROP COLUMN [ToOrgID];
GO
DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'SeedLotTransaction') AND [c].[name] = N'PersonID');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [SeedLotTransaction] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [SeedLotTransaction] ALTER COLUMN [PersonID] int;
GO
ALTER TABLE [SeedLotTransaction] ADD [OldTransactionID] int;
GO
ALTER TABLE [SeedLotTransaction] ADD [OrganizationID] int;
GO
CREATE INDEX [IX_SeedLotTransaction_OrganizationID] ON [SeedLotTransaction] ([OrganizationID]);
GO
ALTER TABLE [SeedLotTransaction] ADD CONSTRAINT [FK_SeedLotTransaction_Organization_OrganizationID] FOREIGN KEY ([OrganizationID]) REFERENCES [Organization] ([OrganizationID]) ON DELETE NO ACTION;
GO
ALTER TABLE [SeedLotTransaction] ADD CONSTRAINT [FK_SeedLotTransaction_Person_PersonID] FOREIGN KEY ([PersonID]) REFERENCES [Person] ([PersonID]) ON DELETE NO ACTION;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20170426215411_SeedLotTransactionsInitial', N'1.1.1');
GO
ALTER TABLE [SeedLotTransaction] ADD [TransactionDate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20170506150507_TransactionDate', N'1.1.1');
GO
