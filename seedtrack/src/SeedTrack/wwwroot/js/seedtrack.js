﻿// shared javascript code for SeedTrack
//
function AppendUrlParamTokens(url, params) {
    for (var param in params) {
        if (params[param] == null) {
            delete params[param];
        }
    }
    return url + "?" + jQuery.param(params);
};

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
};

