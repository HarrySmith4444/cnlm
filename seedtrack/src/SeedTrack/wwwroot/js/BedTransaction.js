﻿function isValidForm() {
    var errorMsg = "";
    var isValid = true;

    var rbtnPlant = document.getElementById('rbPlant');
    if (rbtnPlant.checked) {
        var plotID = jQuery('#inPlugLotID');
        if ((null == plotID.val()) || (0 == plotID.val())) {
            errorMsg = "Please enter a plug lot";
            isValid = false;
        }
    }
    // SeedlotID
    var rbtnSow = document.getElementById('rbSow');
    if (rbtnSow.checked) {
        var slotID = jQuery('#inSeedLotID')
         // not filled in
        if (slotID.length == 0) {
            errorMsg = "Please enter a seed lot";
            isValid = false;
        }else{
            // not valid
            var target = jQuery('#seedlotTaxon');
            var strTax = target.attr("value");
            if("Not Found"== strTax){
                errorMsg = "Please enter a valid seed lot";
                isValid = false;
            };
        }
        
    }

    var rbtnRemove = document.getElementById('rbRemove');
    if (rbtnRemove.checked) {
        var slotID = jQuery('#inSeedLotID')
        if (slotID.length <= 0 || slotID.val <= 0) {
            errorMsg = "Please enter an amount for percent removed.";
            isValid = false;
        }
    }

    // Conditionally check total bed feet
    if (rbtnSow.checked || rbtnPlant.checked) {
        var inpEmpty = jQuery('#iEmptyFeet');
        var inpEntered = jQuery('#inpBedFeet');
        // use parseInt to get the integer value from the UI string.
        if (parseInt(inpEntered.val(), 10) > parseInt(inpEmpty.val(), 10)) {
            errorMsg = "Not enough room in bed.";
            isValid = false;
        }
    }
    // check the entered planting doesn't exceed that available
    if (rbtnSow.checked) {
        if (parseFloat($('#inpSeedQuantity').val()) > parseFloat($('#inpSeedsAvailable').val())) {
            errorMsg = "Not enough seed available";
            isValid = false;
        }
    }
    if (rbtnPlant.checked) {
        var a = $('#inpPlugQuantity').val();
        var b = $('#inpPlugsAvailable').val();
        if (parseInt($('#inpPlugQuantity').val(),10) > parseInt($('#inpPlugsAvailable').val(),10)) {
            errorMsg = "Not enough plugs available";
            isValid = false;
        }
    }

    var x = document.getElementById('pErrorMsg');
    if (isValid) {
        x.innerHTML = "";
    } else {
        x.innerHTML = "<b><font color='red'>" + errorMsg + "</font></b>";
    }
    return isValid;

};

function FetchAndDisplayPlantings(x) {
    jQuery(".lstPlantings").empty();
    var totBedFeet = 0;
    var emptyBedFeet = 0;
    jQuery.get('/BedTransactions/GetCurrentPlanting/', { bedId: x.value }, function (data) {
            jQuery(data).each(function (index, val) {
                jQuery(".lstPlantings").append("<li>" + val.bedfeet + " : " + val.srcID + " : " + val.taxon + "</li>");
                totBedFeet += val.bedfeet;
            })
            sleep(100);
            $('#iTotalBedFeet').val(totBedFeet);
            var ebf = document.getElementById('bed_length');
            var bedLength = ebf.value;
            emptyBedFeet = bedLength - totBedFeet;
            $('#iEmptyFeet').val(emptyBedFeet);
            $('#inpBedFeet').val(emptyBedFeet);
    });
};



//function SeedLotLookup() {
//    var x = document.getElementById('rbSow');
//    if (true != x.checked) return;

//    ShowSeedLotInfo(true);
//    var val = document.getElementById('inSeedLotID')

//    jQuery.getJSON(AppendUrlParamTokens('@Url.Action("SeedlotNumberLookup", "BedTransactions")',
//        { seedlotNumber: val.value }),
//        function (result) {
//            if (result.taxon) {
//                var target = jQuery('#seedlotTaxon');
//                try {
//                    target.attr(("value"), result.taxon);
//                    target = jQuery('#seedlotSite');
//                    target.attr(("value"), result.site);
//                }
//                catch (err) {
//                    var strErr = err;
//                }
//            } else {
//                var target = jQuery('#seedlotTaxon');
//                target.attr(("value"), "Not Found");
//                target = jQuery('#seedlotSite');
//                target.attr(("value"), "Not Found");
//            }
//        });
//};

function PlugLotDetails(bShow) {
    if (bShow) {

        var x = document.getElementById('lblSourceType');
        x.style.display = 'block';
        x.innerText = "Pluglot";
        // toggle the correct input to be seen
        x = document.getElementById('inPlugLotID');
        x.style.display = 'block';
        x = document.getElementById('inSeedLotID');
        x.style.display = 'none';
    } else {

        x = document.getElementById('inPlugLotID');
        x.style.display = 'none';
    }
};
function ShowLotData(bShow) {
    var y = document.getElementById('lotData');
    if (bShow) {
        y.style.display = 'block';
    } else {
        y.style.display = 'none';
    }
};

function ClearBedClicked() {
    ShowLotData(false);
    ShowPlantingDetails(false);
    var x = document.getElementById('divSeedQuantity');
    x.style.display = 'none';
    x = document.getElementById('divPlugQuantity');
    x.style.display = 'none';

    x = document.getElementById('lblSourceType');
    x.style.display = 'none';
    x = document.getElementById('inSeedLotID');
    x.style.display = 'none';
    x.value = null;
    x = document.getElementById('inPlugLotID');
    x.style.display = 'none';
    x.value = null;
    x = document.getElementById('ActionChoice');
    x.value = 2;
};

function ShowPlantingDetails(bShow) {
    var y = document.getElementById('divPlantingDetails');
    if (bShow) {
        y.style.display = 'block';
    } else {
        y.style.display = 'none';
    }
};

function ShowPlantings() {
    $(".lstPlantings").empty();
    var x = $('#beds_ddl').val();
    //var x= @Model.FarmBedID;
    var plantedBedFeet=0;
    var targetEmpty=jQuery('#iEmptyBedFeet');
    var targetPlanted = jQuery('#iPlantedBedFeet');
    var targetBedLength = jQuery('#bed_length');
    var bedLength = targetBedLength.attr("value");
    jQuery.get('/BedTransactions/GetCurrentPlanting/', { bedId: x }, function (data, status) {
        if ('success' == status) {
            $(data).each(function (index, val) {
                plantedBedFeet += val.bedfeet;
                $(".lstPlantings").append("<li>" + val.bedfeet + " : " + val.srcID + 
                    " : " + val.taxon + " : " + val.siteName + " : " + val.ecoRegion + "</li>");
                //target.attr(("value"), totBedFeet);
                //target.attr(("text"), totBedFeet);

            })
            var empty = bedLength - plantedBedFeet; 
            targetEmpty.attr(("value"), empty);
            targetEmpty.attr(("text"), empty);
            targetPlanted.attr(("value"), plantedBedFeet);
            targetPlanted.attr(("text"), plantedBedFeet);
        }
    });

    //target.attr(("value"), totBedFeet);
}



