DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'BedTransaction') AND [c].[name] = N'PlantSpacing');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [BedTransaction] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [BedTransaction] ALTER COLUMN [PlantSpacing] int;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20170831183706_FieldRecordPlantSpacingNull', N'1.1.1');
GO
DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'PlugLot') AND [c].[name] = N'PlugLotNumber');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [PlugLot] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [PlugLot] DROP COLUMN [PlugLotNumber];
GO
EXEC sp_rename N'PlugLot.LotTag', N'PlugLotTag', N'COLUMN';
GO
ALTER TABLE [PlugLotObservations] ADD [Established] int;
GO
ALTER TABLE [PlugLotObservations] ADD [Germinated] int;
GO
ALTER TABLE [PlugLotObservations] ADD [Overgrown] int;
GO
DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'PlugLot') AND [c].[name] = N'SeedQuantityUsed');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [PlugLot] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [PlugLot] ALTER COLUMN [SeedQuantityUsed] real;
GO
ALTER TABLE [PlugLot] ADD [SeedCover] int NOT NULL DEFAULT 0;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20171121201336_PlugLotsNovember', N'1.1.1');
GO
ALTER TABLE [PlugLot] ADD [PlugLotNumber] int NOT NULL DEFAULT 0;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20171213230243_seedlotLookupInPlugCreate', N'1.1.1');
GO
ALTER TABLE [PlugLot] DROP CONSTRAINT [FK_PlugLot_PlugContainer_PlugContainerID];
GO
ALTER TABLE [PlugLot] DROP CONSTRAINT [FK_PlugLot_PlugSoil_PlugSoilID];
GO
DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'PlugLot') AND [c].[name] = N'SeedsPerPlug');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [PlugLot] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [PlugLot] ALTER COLUMN [SeedsPerPlug] int;
GO
DECLARE @var4 sysname;
SELECT @var4 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'PlugLot') AND [c].[name] = N'PlugSoilID');
IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [PlugLot] DROP CONSTRAINT [' + @var4 + '];');
ALTER TABLE [PlugLot] ALTER COLUMN [PlugSoilID] int;
GO
DECLARE @var5 sysname;
SELECT @var5 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'PlugLot') AND [c].[name] = N'PlugContainerID');
IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [PlugLot] DROP CONSTRAINT [' + @var5 + '];');
ALTER TABLE [PlugLot] ALTER COLUMN [PlugContainerID] int;
GO
CREATE TABLE [PlugLotTransaction] (
    [PlugLotTransactionID] int NOT NULL IDENTITY,
    [CreatedBy] nvarchar(450),
    [CreationDate] datetime2 NOT NULL,
    [ModifiedBy] nvarchar(450),
    [ModifiedDate] datetime2 NOT NULL,
    [Notes] ntext,
    [OrganizationID] int,
    [PersonID] int,
    [PlugLotID] int,
    [PlugQuantity] int,
    [TransactionDate] datetime2 NOT NULL,
    [TransactionType] int NOT NULL,
    CONSTRAINT [PK_PlugLotTransaction] PRIMARY KEY ([PlugLotTransactionID]),
    CONSTRAINT [FK_PlugLotTransaction_Organization_OrganizationID] FOREIGN KEY ([OrganizationID]) REFERENCES [Organization] ([OrganizationID]) ON DELETE NO ACTION,
    CONSTRAINT [FK_PlugLotTransaction_Person_PersonID] FOREIGN KEY ([PersonID]) REFERENCES [Person] ([PersonID]) ON DELETE NO ACTION
);
GO
CREATE INDEX [IX_PlugLotTransaction_OrganizationID] ON [PlugLotTransaction] ([OrganizationID]);
GO
CREATE INDEX [IX_PlugLotTransaction_PersonID] ON [PlugLotTransaction] ([PersonID]);
GO
ALTER TABLE [PlugLot] ADD CONSTRAINT [FK_PlugLot_PlugContainer_PlugContainerID] FOREIGN KEY ([PlugContainerID]) REFERENCES [PlugContainer] ([PlugContainerID]) ON DELETE NO ACTION;
GO
ALTER TABLE [PlugLot] ADD CONSTRAINT [FK_PlugLot_PlugSoil_PlugSoilID] FOREIGN KEY ([PlugSoilID]) REFERENCES [PlugSoil] ([PlugSoilID]) ON DELETE NO ACTION;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180130193138_pluglotnulls', N'1.1.1');
GO
CREATE INDEX [IX_PlugLotTransaction_PlugLotID] ON [PlugLotTransaction] ([PlugLotID]);
GO
ALTER TABLE [PlugLotTransaction] ADD CONSTRAINT [FK_PlugLotTransaction_PlugLot_PlugLotID] FOREIGN KEY ([PlugLotID]) REFERENCES [PlugLot] ([PlugLotID]) ON DELETE NO ACTION;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180130234416_PlugLotNavProperty', N'1.1.1');
GO
ALTER TABLE [SeedLot] ADD [RegionID] int;
GO
ALTER TABLE [PlugLot] ADD [ColdStratTemp] int;
GO
ALTER TABLE [PlugLot] ADD [ImbibeTime] int;
GO
CREATE INDEX [IX_SeedLot_RegionID] ON [SeedLot] ([RegionID]);
GO
ALTER TABLE [SeedLot] ADD CONSTRAINT [FK_SeedLot_Level4Region_RegionID] FOREIGN KEY ([RegionID]) REFERENCES [Level4Region] ([Level4RegionID]) ON DELETE NO ACTION;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180202185106_SL', N'1.1.1');
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180202185233_SlPlNewFields', N'1.1.1');
GO
ALTER TABLE [SeedLot] ADD [BedID] int;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180208184309_BedIDToSeedLot', N'1.1.1');
GO
ALTER TABLE [SeedLot] ADD [InitialQuantity] real;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180210174413_SeedLotInitialQuantity', N'1.1.1');
GO
DECLARE @var6 sysname;
SELECT @var6 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'PlugLot') AND [c].[name] = N'CreatedBy');
IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [PlugLot] DROP CONSTRAINT [' + @var6 + '];');
ALTER TABLE [PlugLot] ALTER COLUMN [CreatedBy] nvarchar(max);
GO
ALTER TABLE [PlugLot] ADD [CurrentQuantity] int;
GO
ALTER TABLE [PlugLot] ADD [State] int NOT NULL DEFAULT 0;
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180220233832_PlugLotQuant', N'1.1.1');
GO
DECLARE @var7 sysname;
SELECT @var7 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'PlugLot') AND [c].[name] = N'CreatedBy');
IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [PlugLot] DROP CONSTRAINT [' + @var7 + '];');
ALTER TABLE [PlugLot] ALTER COLUMN [CreatedBy] nvarchar(450);
GO
CREATE TABLE [SLProvenance] (
    [SLProvenanceID] int NOT NULL IDENTITY,
    [collectionSiteID] int NOT NULL,
    [generation] int NOT NULL,
    [percentContribution] real NOT NULL,
    [seedLotID] int NOT NULL,
    [sourceID] int NOT NULL,
    [speciesID] int NOT NULL,
    CONSTRAINT [PK_SLProvenance] PRIMARY KEY ([SLProvenanceID]),
    CONSTRAINT [FK_SLProvenance_CollectionSite_collectionSiteID] FOREIGN KEY ([collectionSiteID]) REFERENCES [CollectionSite] ([CollectionSiteID]) ON DELETE CASCADE,
    CONSTRAINT [FK_SLProvenance_Species_speciesID] FOREIGN KEY ([speciesID]) REFERENCES [Species] ([SpeciesID]) ON DELETE CASCADE
);
GO
CREATE INDEX [IX_SLProvenance_collectionSiteID] ON [SLProvenance] ([collectionSiteID]);
GO
CREATE INDEX [IX_SLProvenance_speciesID] ON [SLProvenance] ([speciesID]);
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20180308234147_Provenance', N'1.1.1');
GO
