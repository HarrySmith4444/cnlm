﻿CREATE TABLE [BedTransaction] (
    [BedTransactionID] int NOT NULL IDENTITY,
    [BedFeet] int NOT NULL,
    [CreatedBy] nvarchar(450),
    [CreationDate] datetime2 NOT NULL,
    [FarmBedID] int NOT NULL,
    [ModifiedBy] nvarchar(450),
    [ModifiedDate] datetime2 NOT NULL,
    [Notes] ntext,
    [PlantSpacing] int NOT NULL,
    [PlugLotID] int,
    [PlugQuantity] int NOT NULL,
    [RowsPerBed] int NOT NULL,
    [SeedLotID] int,
    [SeedQuantity] real NOT NULL,
    [TransactionDate] datetime2 NOT NULL,
    [TransactionType] int NOT NULL,
    CONSTRAINT [PK_BedTransaction] PRIMARY KEY ([BedTransactionID]),
    CONSTRAINT [FK_BedTransaction_FarmBeds_FarmBedID] FOREIGN KEY ([FarmBedID]) REFERENCES [FarmBeds] ([FarmBedID]) ON DELETE CASCADE,
    CONSTRAINT [FK_BedTransaction_SeedLot_SeedLotID] FOREIGN KEY ([SeedLotID]) REFERENCES [SeedLot] ([SeedLotID]) ON DELETE NO ACTION
);

GO

CREATE TABLE [PlugSoil] (
    [PlugSoilID] int NOT NULL IDENTITY,
    [PlugSoilDescription] nvarchar(max),
    [PlugSoilName] nvarchar(max),
    CONSTRAINT [PK_PlugSoil] PRIMARY KEY ([PlugSoilID])
);

GO

CREATE TABLE [PlugSoilComponent] (
    [PlugSoilComponentID] int NOT NULL IDENTITY,
    [PlugSoilComponentDescription] nvarchar(max),
    [PlugSoilComponentName] nvarchar(max),
    CONSTRAINT [PK_PlugSoilComponent] PRIMARY KEY ([PlugSoilComponentID])
);

GO

CREATE TABLE [PlugSoilFormula] (
    [PlugSoilID] int NOT NULL IDENTITY,
    [PlugSoilComponentID] int NOT NULL,
    [PlugSoilComponentPercentage] real NOT NULL,
    CONSTRAINT [PK_PlugSoilFormula] PRIMARY KEY ([PlugSoilID])
);

GO

CREATE INDEX [IX_BedTransaction_FarmBedID] ON [BedTransaction] ([FarmBedID]);

GO

CREATE INDEX [IX_BedTransaction_SeedLotID] ON [BedTransaction] ([SeedLotID]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20170630173503_PlugSoil', N'1.1.1');

GO

CREATE TABLE [PlugContainer] (
    [PlugContainerID] int NOT NULL IDENTITY,
    [PlugContainerName] nvarchar(max),
    [PlugVolume] real NOT NULL,
    [PlugsPerTray] int NOT NULL,
    CONSTRAINT [PK_PlugContainer] PRIMARY KEY ([PlugContainerID])
);

GO

CREATE TABLE [PlugLocation] (
    [PlugLocationID] int NOT NULL IDENTITY,
    [FarmSiteID] nvarchar(max),
    [PlugLocationName] nvarchar(max),
    CONSTRAINT [PK_PlugLocation] PRIMARY KEY ([PlugLocationID])
);

GO

CREATE TABLE [PlugLot] (
    [PlugLotID] int NOT NULL IDENTITY,
    [LotTag] nvarchar(40),
    [OrganizationID] nvarchar(max),
    [PlugContainerID] int NOT NULL,
    [PlugLotNumber] int NOT NULL,
    [PlugSoilID] int NOT NULL,
    [SeedLotID] int NOT NULL,
    [SeedPretreatment] nvarchar(max),
    [SeedQuantityUsed] real NOT NULL,
    [SeedsPerPlug] int NOT NULL,
    CONSTRAINT [PK_PlugLot] PRIMARY KEY ([PlugLotID])
);

GO

CREATE TABLE [PlugLotLocationTransaction] (
    [PlugLotID] int NOT NULL IDENTITY,
    [PlugLotLocationID] int NOT NULL,
    [PlugLotMovementDate] datetime2 NOT NULL,
    CONSTRAINT [PK_PlugLotLocationTransaction] PRIMARY KEY ([PlugLotID])
);

GO

CREATE TABLE [PlugLotObservations] (
    [PlugLotID] int NOT NULL IDENTITY,
    [PlugLotObservation] nvarchar(max),
    [PlugLotObservationDate] datetime2 NOT NULL,
    CONSTRAINT [PK_PlugLotObservations] PRIMARY KEY ([PlugLotID])
);

GO

CREATE TABLE [PlugMaintenanceAction] (
    [PlugLotID] int NOT NULL IDENTITY,
    [PlugMaintenanceActionDate] datetime2 NOT NULL,
    [PlugMaintenanceActionNotes] nvarchar(max),
    CONSTRAINT [PK_PlugMaintenanceAction] PRIMARY KEY ([PlugLotID])
);

GO

CREATE INDEX [IX_BedTransaction_PlugLotID] ON [BedTransaction] ([PlugLotID]);

GO

ALTER TABLE [BedTransaction] ADD CONSTRAINT [FK_BedTransaction_PlugLot_PlugLotID] FOREIGN KEY ([PlugLotID]) REFERENCES [PlugLot] ([PlugLotID]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20170701063545_PlugLots', N'1.1.1');

GO

ALTER TABLE [PlugSoilFormula] ADD [PlugSoilID1] int;

GO

ALTER TABLE [PlugMaintenanceAction] ADD [PlugLotID1] int;

GO

ALTER TABLE [PlugLotObservations] ADD [PlugLotID1] int;

GO

ALTER TABLE [PlugLotLocationTransaction] ADD [CreatedBy] nvarchar(max);

GO

ALTER TABLE [PlugLotLocationTransaction] ADD [CreationDate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

ALTER TABLE [PlugLotLocationTransaction] ADD [ModifiedBy] nvarchar(450);

GO

ALTER TABLE [PlugLotLocationTransaction] ADD [ModifiedDate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

ALTER TABLE [PlugLotLocationTransaction] ADD [PlugLocationID] int;

GO

DROP INDEX [IX_PlugLot_OrganizationID] ON [PlugLot];
DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'PlugLot') AND [c].[name] = N'OrganizationID');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [PlugLot] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [PlugLot] ALTER COLUMN [OrganizationID] int NOT NULL;
CREATE INDEX [IX_PlugLot_OrganizationID] ON [PlugLot] ([OrganizationID]);

GO

ALTER TABLE [PlugLot] ADD [CreatedBy] nvarchar(450);

GO

ALTER TABLE [PlugLot] ADD [CreationDate] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.000';

GO

ALTER TABLE [PlugLot] ADD [ModifiedBy] nvarchar(450);

GO

ALTER TABLE [PlugLot] ADD [ModifiedDate] datetime2;

GO

ALTER TABLE [PlugLot] ADD [Notes] nvarchar(max);

GO

ALTER TABLE [PlugLocation] ADD [FarmSiteID1] int;

GO

CREATE TABLE [SeedLotFarmBed] (
    [SeedLotID] int NOT NULL,
    [FarmBedID] int NOT NULL,
    CONSTRAINT [PK_SeedLotFarmBed] PRIMARY KEY ([SeedLotID], [FarmBedID]),
    CONSTRAINT [FK_SeedLotFarmBed_FarmBeds_FarmBedID] FOREIGN KEY ([FarmBedID]) REFERENCES [FarmBeds] ([FarmBedID]) ON DELETE CASCADE,
    CONSTRAINT [FK_SeedLotFarmBed_SeedLot_SeedLotID] FOREIGN KEY ([SeedLotID]) REFERENCES [SeedLot] ([SeedLotID]) ON DELETE CASCADE
);

GO

CREATE INDEX [IX_PlugSoilFormula_PlugSoilComponentID] ON [PlugSoilFormula] ([PlugSoilComponentID]);

GO

CREATE INDEX [IX_PlugSoilFormula_PlugSoilID1] ON [PlugSoilFormula] ([PlugSoilID1]);

GO

CREATE INDEX [IX_PlugMaintenanceAction_PlugLotID1] ON [PlugMaintenanceAction] ([PlugLotID1]);

GO

CREATE INDEX [IX_PlugLotObservations_PlugLotID1] ON [PlugLotObservations] ([PlugLotID1]);

GO

CREATE INDEX [IX_PlugLotLocationTransaction_PlugLocationID] ON [PlugLotLocationTransaction] ([PlugLocationID]);

GO

CREATE INDEX [IX_PlugLot_PlugContainerID] ON [PlugLot] ([PlugContainerID]);

GO

CREATE INDEX [IX_PlugLot_PlugSoilID] ON [PlugLot] ([PlugSoilID]);

GO

CREATE INDEX [IX_PlugLot_SeedLotID] ON [PlugLot] ([SeedLotID]);

GO

CREATE INDEX [IX_PlugLocation_FarmSiteID1] ON [PlugLocation] ([FarmSiteID1]);

GO

CREATE INDEX [IX_SeedLotFarmBed_FarmBedID] ON [SeedLotFarmBed] ([FarmBedID]);

GO

ALTER TABLE [PlugLocation] ADD CONSTRAINT [FK_PlugLocation_FarmSite_FarmSiteID1] FOREIGN KEY ([FarmSiteID1]) REFERENCES [FarmSite] ([FarmSiteID]) ON DELETE NO ACTION;

GO

ALTER TABLE [PlugLot] ADD CONSTRAINT [FK_PlugLot_Organization_OrganizationID] FOREIGN KEY ([OrganizationID]) REFERENCES [Organization] ([OrganizationID]) ON DELETE CASCADE;

GO

ALTER TABLE [PlugLot] ADD CONSTRAINT [FK_PlugLot_PlugContainer_PlugContainerID] FOREIGN KEY ([PlugContainerID]) REFERENCES [PlugContainer] ([PlugContainerID]) ON DELETE CASCADE;

GO

ALTER TABLE [PlugLot] ADD CONSTRAINT [FK_PlugLot_PlugSoil_PlugSoilID] FOREIGN KEY ([PlugSoilID]) REFERENCES [PlugSoil] ([PlugSoilID]) ON DELETE CASCADE;

GO

ALTER TABLE [PlugLot] ADD CONSTRAINT [FK_PlugLot_SeedLot_SeedLotID] FOREIGN KEY ([SeedLotID]) REFERENCES [SeedLot] ([SeedLotID]) ON DELETE CASCADE;

GO

ALTER TABLE [PlugLotLocationTransaction] ADD CONSTRAINT [FK_PlugLotLocationTransaction_PlugLocation_PlugLocationID] FOREIGN KEY ([PlugLocationID]) REFERENCES [PlugLocation] ([PlugLocationID]) ON DELETE NO ACTION;

GO

ALTER TABLE [PlugLotObservations] ADD CONSTRAINT [FK_PlugLotObservations_PlugLot_PlugLotID1] FOREIGN KEY ([PlugLotID1]) REFERENCES [PlugLot] ([PlugLotID]) ON DELETE NO ACTION;

GO

ALTER TABLE [PlugMaintenanceAction] ADD CONSTRAINT [FK_PlugMaintenanceAction_PlugLot_PlugLotID1] FOREIGN KEY ([PlugLotID1]) REFERENCES [PlugLot] ([PlugLotID]) ON DELETE NO ACTION;

GO

ALTER TABLE [PlugSoilFormula] ADD CONSTRAINT [FK_PlugSoilFormula_PlugSoilComponent_PlugSoilComponentID] FOREIGN KEY ([PlugSoilComponentID]) REFERENCES [PlugSoilComponent] ([PlugSoilComponentID]) ON DELETE CASCADE;

GO

ALTER TABLE [PlugSoilFormula] ADD CONSTRAINT [FK_PlugSoilFormula_PlugSoil_PlugSoilID1] FOREIGN KEY ([PlugSoilID1]) REFERENCES [PlugSoil] ([PlugSoilID]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20170707184612_SeedLotFarmBed', N'1.1.1');

GO

ALTER TABLE [SeedLot] ADD [CurrentQuantity] real;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20170719181559_SeedLotQuantity', N'1.1.1');

GO

