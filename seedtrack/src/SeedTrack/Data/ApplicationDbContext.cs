﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Models;
using SeedTrack.Models.SeedLotModels;
using SeedTrack.Models.SeedLotTransactionModels;
using SeedTrack.Models.PlugLotModels;
using SeedTrack.Models.FarmModels;


namespace SeedTrack.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        

        public DbSet<Species> Species { get; set; }

        public DbSet<SeedLot> SeedLot { get; set; }

        public DbSet<Organization> Organization { get; set; }

        public DbSet<Person> Person { get; set; }
        public DbSet<FarmBed> FarmBeds { get; set; }
        public DbSet<FarmSite> FarmSite { get; set; }
       // public DbSet<SeedLotBeds> SeedLotBeds { get; set; }
        public DbSet<Level3Region> Level3Region { get; set; }
        public DbSet<Level4Region> Level4Region { get; set; }
        public DbSet<CollectionSite> CollectionSite { get; set; }
        public DbSet<SeedLotTransaction> SeedLotTransaction { get; set; }
        public DbSet<PlugSoilComponent> PlugSoilComponent { get; set; }
        public DbSet<PlugSoilFormula> PlugSoilFormula { get; set; }
        public DbSet<PlugSoil> PlugSoil { get; set; }
        public DbSet<BedTransaction> BedTransaction { get; set; }
        public DbSet<PlugContainer> PlugContainer { get; set; }
        public DbSet<PlugLocation> PlugLocation { get; set; }
        public DbSet<PlugLotTransaction> PlugLotTransaction { get; set; }
        public DbSet<PlugLotLocationTransaction> PlugLotLocationTransaction { get; set; }
        public DbSet<PlugLot> PlugLot { get; set; }
        public DbSet<PlugLotObservations> PlugLotObservations { get; set; }
        public DbSet<PlugMaintenanceAction> PlugMaintenanceAction { get; set; }
        public DbSet<SLProvenance> SLProvenance { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<SeedLotFarmBed>()
            .HasKey(t => new { t.SeedLotID, t.FarmBedID });

            builder.Entity<SeedLotFarmBed>()
                .HasOne(pt => pt.SeedLot)
                .WithMany(p => p.SeedLotFarmBeds)
                .HasForeignKey(pt => pt.SeedLotID);

            builder.Entity<SeedLotFarmBed>()
                .HasOne(pt => pt.FarmBed)
                .WithMany(t => t.SeedLotFarmBeds)
                .HasForeignKey(pt => pt.FarmBedID);
        }
    }
}
