Project SeedTrack (.NETCoreApp,Version=v1.1) was previously compiled. Skipping compilation.
ALTER TABLE [SeedLot] DROP CONSTRAINT [FK_SeedLot_Organization_SourceOrgID];
GO
DROP INDEX [IX_SeedLot_SourceOrgID] ON [SeedLot];
GO
DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'SeedLot') AND [c].[name] = N'HarvestedQuantity');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [SeedLot] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [SeedLot] DROP COLUMN [HarvestedQuantity];
GO
DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'SeedLot') AND [c].[name] = N'SourceOrgID');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [SeedLot] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [SeedLot] DROP COLUMN [SourceOrgID];
GO
DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'SeedLot') AND [c].[name] = N'Notes');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [SeedLot] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [SeedLot] ALTER COLUMN [Notes] nvarchar(max);
GO
INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20170421225426_SeedLotDropHarvestQuantAndSourceOrg', N'1.1.1');
GO
