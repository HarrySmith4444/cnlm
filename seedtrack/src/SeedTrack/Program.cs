﻿// first change to test source control - just this comment
// change in development branch - just this comment
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
//using Microsoft.Extensions.Logging;
using NLog;

namespace SeedTrack
{
    public static class Constants
    {
        public const string Version = "1.0.0.9";
        public const string AdminName = "Administrator";
        public const int MAX_SEED_GENERATIONS = 11;
        public const int MAX_SEED_GRADES = 3;
        public const string STR_EARLIEST_SEARCH = "01/01/1800";
        public const string STR_LATEST_SEARCH = "01/01/2100";
        public const int SENDINBLUE_TIMEOUT = 60000; // one minute
        public const int SEEDLOTQUANTITYDIGITS = 2; // stored and calcs as floats but round to this
        public const int DEFAULT_ROWS_PER_BED = 2;
        public const float DEFAULT_SEED_QUANTITY = 1.0f;
        public const int DEFAULT_BED_FEET_PLANTED = 10;
        public const int DEFAULT_PLANT_SPACING = 6;
        public const int DEFAULT_PLUG_QUANTITY = 10;
        public const int DEFAULT_SEEDS_PER_PLUG = 5;
        public const int MAX_GENERATION = 999;
        public const int MAX_SEEDLOT_SOURCES = 20; // arrays built on this
        public const string CNLM_NAME = "Center for Natural Lands Management";

    }
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
        public string bluekey;
    }
    public static class ProgramState
    {
        static public RunningState State { get; set; }
        public enum RunningState { Normal, CreatingSeedLot };

    }

    public static class StringExt
    {
        public static bool IsNumeric(this string text)
        {
            double test;
            return double.TryParse(text, out test);
        }
    }
}
