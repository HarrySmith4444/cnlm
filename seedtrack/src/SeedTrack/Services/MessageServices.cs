﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using mailinblue;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.Storage.Internal;

namespace SeedTrack.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        private readonly ILogger _logger;

        public AuthMessageSender(IOptions<AuthMessageSenderOptions> optionsAccessor, ILogger<AuthMessageSender> logger)
        {
            
            //if (null != optionsAccessor.Value && optionsAccessor.Value.ToString().Length > 0)
            //    Options = optionsAccessor.Value;
            //else
            //{
                string bluesecret;
                Options = new AuthMessageSenderOptions();
                 bluesecret = System.Environment.GetEnvironmentVariable("TERCESEULBNIDNES");
                Options.SendInBlueKey = bluesecret;
            //}
            
            _logger = logger;
        }
    
        public AuthMessageSenderOptions Options { get; } //set only via Secret Manager
        public  Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            //_logger.LogInformation(2, "Before call to SendInBlue {0}", Options.SendInBlueKey);
            return Execute(Options.SendInBlueKey, subject, message, email);

            //return Task.FromResult(0);
        }

        public Task Execute(string apiKey, string subject, string message, string email)
        {
            API sendinBlue = new mailinblue.API(apiKey, SeedTrack.Constants.SENDINBLUE_TIMEOUT);
            Dictionary<string, Object> data = new Dictionary<string, Object>();
            Dictionary<string, string> to = new Dictionary<string, string>();
            to.Add(email, "New User");
            List<string> from_name = new List<string>();
            from_name.Add("ssmith@cnlm.org");
            from_name.Add("CNLM SeedTrack");

            data.Add("to", to);
            data.Add("from", from_name);
            data.Add("subject", subject);
            data.Add("html", message);
            //data.Add("attachment", attachment);
            // Dump
            //_logger.LogInformation("BEFORE: {0}", data.ToString());
            //Dictionary<string, string> myTo = (Dictionary<string,string>) (data["to"]);
            //for (int i = 0; i < myTo.Count; ++i)
            //{
            //    KeyValuePair<string, string> val = myTo.ElementAt(i);
            //    _logger.LogInformation("To: {0} {1}",val.Key,val.Value);
            //}
            //List<string> myFrom = (List<string>) (data["from"]);
            //_logger.LogInformation("B----: {0}", myFrom.ToString());
            //_logger.LogInformation("B----: {0}",data["subject"]);
            //_logger.LogInformation("B----: {0}", data["html"]);
            try
            {

                JObject sendEmail = sendinBlue.send_email(data);

                if (null == sendEmail)
                {
                    _logger.LogInformation("send_email returned a null JObject");
                    if (null != data)
                        _logger.LogInformation("FAILED: {0}", data.ToString());
                }
                else {
                    _logger.LogInformation("Email sent to {0}", email);
                    //_logger.LogInformation(sendEmail.ToString());
                    //_logger.LogInformation("WORKED: {0}", data.ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Exception sending email. {0} {1}", ex.HResult, ex.Message);
            }
            return Task.FromResult(0);
        }


        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}
