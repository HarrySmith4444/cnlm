﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class FarmBedTagAlpha : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FarmBedNumber",
                table: "FarmBeds");

            migrationBuilder.AddColumn<string>(
                name: "FarmBedTag",
                table: "FarmBeds",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FarmBedTag",
                table: "FarmBeds");

            migrationBuilder.AddColumn<int>(
                name: "FarmBedNumber",
                table: "FarmBeds",
                nullable: false,
                defaultValue: 0);
        }
    }
}
