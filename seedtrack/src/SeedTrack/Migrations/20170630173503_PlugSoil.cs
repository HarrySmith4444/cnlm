﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeedTrack.Migrations
{
    public partial class PlugSoil : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BedTransaction",
                columns: table => new
                {
                    BedTransactionID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BedFeet = table.Column<int>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 450, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    FarmBedID = table.Column<int>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 450, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Notes = table.Column<string>(type: "ntext", nullable: true),
                    PlantSpacing = table.Column<int>(nullable: false),
                    PlugLotID = table.Column<int>(nullable: true),
                    PlugQuantity = table.Column<int>(nullable: false),
                    RowsPerBed = table.Column<int>(nullable: false),
                    SeedLotID = table.Column<int>(nullable: true),
                    SeedQuantity = table.Column<float>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BedTransaction", x => x.BedTransactionID);
                    table.ForeignKey(
                        name: "FK_BedTransaction_FarmBeds_FarmBedID",
                        column: x => x.FarmBedID,
                        principalTable: "FarmBeds",
                        principalColumn: "FarmBedID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BedTransaction_SeedLot_SeedLotID",
                        column: x => x.SeedLotID,
                        principalTable: "SeedLot",
                        principalColumn: "SeedLotID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlugSoil",
                columns: table => new
                {
                    PlugSoilID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlugSoilDescription = table.Column<string>(nullable: true),
                    PlugSoilName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugSoil", x => x.PlugSoilID);
                });

            migrationBuilder.CreateTable(
                name: "PlugSoilComponent",
                columns: table => new
                {
                    PlugSoilComponentID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlugSoilComponentDescription = table.Column<string>(nullable: true),
                    PlugSoilComponentName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugSoilComponent", x => x.PlugSoilComponentID);
                });

            migrationBuilder.CreateTable(
                name: "PlugSoilFormula",
                columns: table => new
                {
                    PlugSoilID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlugSoilComponentID = table.Column<int>(nullable: false),
                    PlugSoilComponentPercentage = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugSoilFormula", x => x.PlugSoilID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BedTransaction_FarmBedID",
                table: "BedTransaction",
                column: "FarmBedID");

            migrationBuilder.CreateIndex(
                name: "IX_BedTransaction_SeedLotID",
                table: "BedTransaction",
                column: "SeedLotID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BedTransaction");

            migrationBuilder.DropTable(
                name: "PlugSoil");

            migrationBuilder.DropTable(
                name: "PlugSoilComponent");

            migrationBuilder.DropTable(
                name: "PlugSoilFormula");
        }
    }
}
