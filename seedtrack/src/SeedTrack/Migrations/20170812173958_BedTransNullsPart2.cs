﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class BedTransNullsPart2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BedTransaction_FarmBeds_FarmBedID",
                table: "BedTransaction");

            migrationBuilder.AlterColumn<int>(
                name: "FarmBedID",
                table: "BedTransaction",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_BedTransaction_FarmBeds_FarmBedID",
                table: "BedTransaction",
                column: "FarmBedID",
                principalTable: "FarmBeds",
                principalColumn: "FarmBedID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BedTransaction_FarmBeds_FarmBedID",
                table: "BedTransaction");

            migrationBuilder.AlterColumn<int>(
                name: "FarmBedID",
                table: "BedTransaction",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_BedTransaction_FarmBeds_FarmBedID",
                table: "BedTransaction",
                column: "FarmBedID",
                principalTable: "FarmBeds",
                principalColumn: "FarmBedID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
