﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class PlugLotNavProperty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PlugLotTransaction_PlugLotID",
                table: "PlugLotTransaction",
                column: "PlugLotID");

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLotTransaction_PlugLot_PlugLotID",
                table: "PlugLotTransaction",
                column: "PlugLotID",
                principalTable: "PlugLot",
                principalColumn: "PlugLotID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlugLotTransaction_PlugLot_PlugLotID",
                table: "PlugLotTransaction");

            migrationBuilder.DropIndex(
                name: "IX_PlugLotTransaction_PlugLotID",
                table: "PlugLotTransaction");
        }
    }
}
