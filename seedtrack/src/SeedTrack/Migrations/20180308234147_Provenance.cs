﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeedTrack.Migrations
{
    public partial class Provenance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "PlugLot",
                maxLength: 450,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "SLProvenance",
                columns: table => new
                {
                    SLProvenanceID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    collectionSiteID = table.Column<int>(nullable: false),
                    generation = table.Column<int>(nullable: false),
                    percentContribution = table.Column<float>(nullable: false),
                    seedLotID = table.Column<int>(nullable: false),
                    sourceID = table.Column<int>(nullable: false),
                    speciesID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SLProvenance", x => x.SLProvenanceID);
                    table.ForeignKey(
                        name: "FK_SLProvenance_CollectionSite_collectionSiteID",
                        column: x => x.collectionSiteID,
                        principalTable: "CollectionSite",
                        principalColumn: "CollectionSiteID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SLProvenance_Species_speciesID",
                        column: x => x.speciesID,
                        principalTable: "Species",
                        principalColumn: "SpeciesID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SLProvenance_collectionSiteID",
                table: "SLProvenance",
                column: "collectionSiteID");

            migrationBuilder.CreateIndex(
                name: "IX_SLProvenance_speciesID",
                table: "SLProvenance",
                column: "speciesID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SLProvenance");

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "PlugLot",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 450,
                oldNullable: true);
        }
    }
}
