﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class SeedLotDropHarvestQuantAndSourceOrg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SeedLot_Organization_SourceOrgID",
                table: "SeedLot");

            migrationBuilder.DropIndex(
                name: "IX_SeedLot_SourceOrgID",
                table: "SeedLot");

            migrationBuilder.DropColumn(
                name: "HarvestedQuantity",
                table: "SeedLot");

            migrationBuilder.DropColumn(
                name: "SourceOrgID",
                table: "SeedLot");

            migrationBuilder.AlterColumn<string>(
                name: "Notes",
                table: "SeedLot",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "ntext",
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Notes",
                table: "SeedLot",
                type: "ntext",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<float>(
                name: "HarvestedQuantity",
                table: "SeedLot",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SourceOrgID",
                table: "SeedLot",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SeedLot_SourceOrgID",
                table: "SeedLot",
                column: "SourceOrgID");

            migrationBuilder.AddForeignKey(
                name: "FK_SeedLot_Organization_SourceOrgID",
                table: "SeedLot",
                column: "SourceOrgID",
                principalTable: "Organization",
                principalColumn: "OrganizationID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
