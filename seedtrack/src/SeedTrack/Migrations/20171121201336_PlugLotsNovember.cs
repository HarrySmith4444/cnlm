﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class PlugLotsNovember : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlugLotNumber",
                table: "PlugLot");

            migrationBuilder.RenameColumn(
                name: "LotTag",
                table: "PlugLot",
                newName: "PlugLotTag");

            migrationBuilder.AddColumn<int>(
                name: "Established",
                table: "PlugLotObservations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Germinated",
                table: "PlugLotObservations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Overgrown",
                table: "PlugLotObservations",
                nullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "SeedQuantityUsed",
                table: "PlugLot",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AddColumn<int>(
                name: "SeedCover",
                table: "PlugLot",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Established",
                table: "PlugLotObservations");

            migrationBuilder.DropColumn(
                name: "Germinated",
                table: "PlugLotObservations");

            migrationBuilder.DropColumn(
                name: "Overgrown",
                table: "PlugLotObservations");

            migrationBuilder.DropColumn(
                name: "SeedCover",
                table: "PlugLot");

            migrationBuilder.RenameColumn(
                name: "PlugLotTag",
                table: "PlugLot",
                newName: "LotTag");

            migrationBuilder.AlterColumn<float>(
                name: "SeedQuantityUsed",
                table: "PlugLot",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlugLotNumber",
                table: "PlugLot",
                nullable: false,
                defaultValue: 0);
        }
    }
}
