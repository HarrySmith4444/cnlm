﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotModels;
using SeedTrack.Models.SeedLotTransactionModels;

namespace SeedTrack.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170402184317_Initial-2")]
    partial class Initial2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("SeedTrack.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.Address", b =>
                {
                    b.Property<int>("AddressID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("City")
                        .HasMaxLength(80);

                    b.Property<string>("Country")
                        .HasMaxLength(60);

                    b.Property<string>("FirstLine")
                        .HasMaxLength(80);

                    b.Property<string>("SecondLine")
                        .HasMaxLength(80);

                    b.Property<string>("State")
                        .HasMaxLength(60);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(12);

                    b.HasKey("AddressID");

                    b.ToTable("Address");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.FarmBed", b =>
                {
                    b.Property<int>("FarmBedID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BedType");

                    b.Property<int>("FarmSiteID");

                    b.Property<float>("IrrigationFlowRate");

                    b.Property<string>("IrrigationType");

                    b.Property<float>("Length");

                    b.Property<float>("Width");

                    b.HasKey("FarmBedID");

                    b.HasIndex("FarmSiteID");

                    b.ToTable("FarmBeds");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.FarmSite", b =>
                {
                    b.Property<int>("FarmSiteID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FarmCode")
                        .HasMaxLength(10);

                    b.Property<string>("FarmName")
                        .HasMaxLength(120);

                    b.HasKey("FarmSiteID");

                    b.ToTable("FarmSite");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.Level3Region", b =>
                {
                    b.Property<int>("Level3RegionID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .HasMaxLength(120);

                    b.HasKey("Level3RegionID");

                    b.ToTable("Level3Region");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.Organization", b =>
                {
                    b.Property<int>("OrganizationID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AddressID");

                    b.Property<int?>("ContactPersonID");

                    b.Property<string>("OrganizationName")
                        .HasMaxLength(120);

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(20);

                    b.HasKey("OrganizationID");

                    b.HasIndex("AddressID");

                    b.HasIndex("ContactPersonID")
                        .IsUnique();

                    b.ToTable("Organization");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.Person", b =>
                {
                    b.Property<int>("PersonID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AddressID");

                    b.Property<string>("CellPhone")
                        .HasMaxLength(20);

                    b.Property<string>("Email");

                    b.Property<string>("FirstName")
                        .HasMaxLength(80);

                    b.Property<bool>("IsContactPerson");

                    b.Property<string>("LastName")
                        .HasMaxLength(80);

                    b.Property<int?>("OrganizationID");

                    b.Property<string>("Phone")
                        .HasMaxLength(20);

                    b.HasKey("PersonID");

                    b.HasIndex("AddressID");

                    b.HasIndex("OrganizationID");

                    b.ToTable("Person");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.SeedLot", b =>
                {
                    b.Property<int>("SeedLotID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(450);

                    b.Property<DateTime>("CreationDate");

                    b.Property<DateTime>("FinalHarvestDate");

                    b.Property<DateTime>("FirstHarvestDate");

                    b.Property<int>("Generation");

                    b.Property<string>("Grade")
                        .HasMaxLength(1);

                    b.Property<float>("HarvestedQuantity");

                    b.Property<string>("LotTag")
                        .HasMaxLength(40);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(450);

                    b.Property<DateTime>("ModifiedDate");

                    b.Property<string>("Notes")
                        .HasColumnType("ntext");

                    b.Property<string>("OldLotNo")
                        .HasMaxLength(40);

                    b.Property<int>("SourceOrgID");

                    b.Property<int>("SourceSiteID");

                    b.Property<int>("SourceType");

                    b.Property<int>("SpeciesID");

                    b.Property<int>("Status");

                    b.HasKey("SeedLotID");

                    b.HasIndex("SourceOrgID");

                    b.HasIndex("SpeciesID");

                    b.ToTable("SeedLot");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.SeedLotBeds", b =>
                {
                    b.Property<int>("SeedLotID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("FarmBedID");

                    b.Property<int?>("SeedLotID1");

                    b.HasKey("SeedLotID");

                    b.HasIndex("SeedLotID1");

                    b.ToTable("SeedLotBets");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.SeedLotRegion", b =>
                {
                    b.Property<int>("SeedLotID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Level4RegionID");

                    b.Property<int?>("SeedLotID1");

                    b.HasKey("SeedLotID");

                    b.HasIndex("SeedLotID1");

                    b.ToTable("SeedLotRegion");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotTransactionModels.SeedLotTransaction", b =>
                {
                    b.Property<int>("SeedLotTransactionID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(450);

                    b.Property<DateTime>("CreationDate");

                    b.Property<int>("FromOrgID");

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(450);

                    b.Property<DateTime>("ModifiedDate");

                    b.Property<string>("Notes")
                        .HasColumnType("ntext");

                    b.Property<int>("PersonID");

                    b.Property<float>("Quantity");

                    b.Property<int>("SeedLotID");

                    b.Property<int>("ToOrgID");

                    b.Property<int>("TransactionType");

                    b.HasKey("SeedLotTransactionID");

                    b.HasIndex("FromOrgID");

                    b.HasIndex("PersonID");

                    b.HasIndex("SeedLotID");

                    b.HasIndex("ToOrgID");

                    b.ToTable("SeedLotTransaction");
                });

            modelBuilder.Entity("SeedTrack.Models.Species", b =>
                {
                    b.Property<int>("SpeciesID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Clade");

                    b.Property<string>("Codon");

                    b.Property<string>("CommonName");

                    b.Property<string>("Genus");

                    b.Property<string>("LifeCycle");

                    b.Property<float>("PctCoverOneAdult");

                    b.Property<long>("RestorationCharacteristics");

                    b.Property<string>("RootStructure");

                    b.Property<string>("SeedDormancyType");

                    b.Property<string>("SpeciesName");

                    b.Property<string>("SubTaxa");

                    b.Property<string>("Synonyms");

                    b.HasKey("SpeciesID");

                    b.ToTable("Species");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("SeedTrack.Models.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("SeedTrack.Models.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SeedTrack.Models.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.FarmBed", b =>
                {
                    b.HasOne("SeedTrack.Models.SeedLotModels.FarmSite", "FarmSite")
                        .WithMany()
                        .HasForeignKey("FarmSiteID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.Organization", b =>
                {
                    b.HasOne("SeedTrack.Models.SeedLotModels.Address", "OrganizationAddress")
                        .WithMany()
                        .HasForeignKey("AddressID");

                    b.HasOne("SeedTrack.Models.SeedLotModels.Person", "ContactPerson")
                        .WithOne("ContactPersonFor")
                        .HasForeignKey("SeedTrack.Models.SeedLotModels.Organization", "ContactPersonID");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.Person", b =>
                {
                    b.HasOne("SeedTrack.Models.SeedLotModels.Address", "Address")
                        .WithMany()
                        .HasForeignKey("AddressID");

                    b.HasOne("SeedTrack.Models.SeedLotModels.Organization", "Organization")
                        .WithMany("Persons")
                        .HasForeignKey("OrganizationID");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.SeedLot", b =>
                {
                    b.HasOne("SeedTrack.Models.SeedLotModels.Organization", "Organization")
                        .WithMany()
                        .HasForeignKey("SourceOrgID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SeedTrack.Models.Species", "Species")
                        .WithMany()
                        .HasForeignKey("SpeciesID")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.SeedLotBeds", b =>
                {
                    b.HasOne("SeedTrack.Models.SeedLotModels.SeedLot")
                        .WithMany("SeedLotBeds")
                        .HasForeignKey("SeedLotID1");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotModels.SeedLotRegion", b =>
                {
                    b.HasOne("SeedTrack.Models.SeedLotModels.SeedLot")
                        .WithMany("SeedLotRegions")
                        .HasForeignKey("SeedLotID1");
                });

            modelBuilder.Entity("SeedTrack.Models.SeedLotTransactionModels.SeedLotTransaction", b =>
                {
                    b.HasOne("SeedTrack.Models.SeedLotModels.Organization", "FromOrg")
                        .WithMany()
                        .HasForeignKey("FromOrgID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SeedTrack.Models.SeedLotModels.Person", "UserReference")
                        .WithMany()
                        .HasForeignKey("PersonID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SeedTrack.Models.SeedLotModels.SeedLot")
                        .WithMany("SeedLotTransactions")
                        .HasForeignKey("SeedLotID")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("SeedTrack.Models.SeedLotModels.Organization", "ToOrg")
                        .WithMany()
                        .HasForeignKey("ToOrgID")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
