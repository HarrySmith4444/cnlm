﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeedTrack.Migrations
{
    public partial class SeedLotIDsAndMore : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organization_Address_AddressID",
                table: "Organization");

            migrationBuilder.DropForeignKey(
                name: "FK_Person_Address_AddressID",
                table: "Person");

            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropIndex(
                name: "IX_Person_AddressID",
                table: "Person");

            migrationBuilder.DropIndex(
                name: "IX_Organization_AddressID",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "OldLotNo",
                table: "SeedLot");

            migrationBuilder.DropColumn(
                name: "AddressID",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "AddressID",
                table: "Organization");

            migrationBuilder.AddColumn<int>(
                name: "ADNumber",
                table: "SeedLot",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FMNumber",
                table: "SeedLot",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SeedLotNumber",
                table: "SeedLot",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "CellPhone",
                table: "Person",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Person",
                maxLength: 80,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Person",
                maxLength: 60,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Extension",
                table: "Person",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstLine",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecondLine",
                table: "Person",
                maxLength: 80,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Person",
                maxLength: 60,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ZipCode",
                table: "Person",
                maxLength: 12,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Organization",
                maxLength: 80,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "Organization",
                maxLength: 60,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Department",
                table: "Organization",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Extension",
                table: "Organization",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstLine",
                table: "Organization",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecondLine",
                table: "Organization",
                maxLength: 80,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "Organization",
                maxLength: 60,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ZipCode",
                table: "Organization",
                maxLength: 12,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CombinedRegionCode",
                table: "Level4Region",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Code",
                table: "Level3Region",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FarmBedNumber",
                table: "FarmBeds",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ADNumber",
                table: "SeedLot");

            migrationBuilder.DropColumn(
                name: "FMNumber",
                table: "SeedLot");

            migrationBuilder.DropColumn(
                name: "SeedLotNumber",
                table: "SeedLot");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "Extension",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "FirstLine",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "SecondLine",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "ZipCode",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "City",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "Department",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "Extension",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "FirstLine",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "SecondLine",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "State",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "ZipCode",
                table: "Organization");

            migrationBuilder.DropColumn(
                name: "CombinedRegionCode",
                table: "Level4Region");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Level3Region");

            migrationBuilder.DropColumn(
                name: "FarmBedNumber",
                table: "FarmBeds");

            migrationBuilder.AddColumn<string>(
                name: "OldLotNo",
                table: "SeedLot",
                maxLength: 40,
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CellPhone",
                table: "Person",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressID",
                table: "Person",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AddressID",
                table: "Organization",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    AddressID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    City = table.Column<string>(maxLength: 80, nullable: true),
                    Country = table.Column<string>(maxLength: 60, nullable: true),
                    FirstLine = table.Column<string>(maxLength: 80, nullable: true),
                    SecondLine = table.Column<string>(maxLength: 80, nullable: true),
                    State = table.Column<string>(maxLength: 60, nullable: true),
                    ZipCode = table.Column<string>(maxLength: 12, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.AddressID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Person_AddressID",
                table: "Person",
                column: "AddressID");

            migrationBuilder.CreateIndex(
                name: "IX_Organization_AddressID",
                table: "Organization",
                column: "AddressID");

            migrationBuilder.AddForeignKey(
                name: "FK_Organization_Address_AddressID",
                table: "Organization",
                column: "AddressID",
                principalTable: "Address",
                principalColumn: "AddressID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Person_Address_AddressID",
                table: "Person",
                column: "AddressID",
                principalTable: "Address",
                principalColumn: "AddressID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
