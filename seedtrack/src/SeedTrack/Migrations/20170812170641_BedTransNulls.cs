﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class BedTransNulls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "SeedQuantity",
                table: "BedTransaction",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<int>(
                name: "PlugQuantity",
                table: "BedTransaction",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<float>(
                name: "SeedQuantity",
                table: "BedTransaction",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PlugQuantity",
                table: "BedTransaction",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
