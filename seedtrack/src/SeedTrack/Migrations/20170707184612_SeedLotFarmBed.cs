﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeedTrack.Migrations
{
    public partial class SeedLotFarmBed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropTable(
            //    name: "SeedLotBets");

            migrationBuilder.AddColumn<int>(
                name: "PlugSoilID1",
                table: "PlugSoilFormula",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlugLotID1",
                table: "PlugMaintenanceAction",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PlugLotID1",
                table: "PlugLotObservations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "PlugLotLocationTransaction",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "PlugLotLocationTransaction",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "PlugLotLocationTransaction",
                maxLength: 450,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDate",
                table: "PlugLotLocationTransaction",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "PlugLocationID",
                table: "PlugLotLocationTransaction",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "OrganizationID",
                table: "PlugLot",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "PlugLot",
                maxLength: 450,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreationDate",
                table: "PlugLot",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "PlugLot",
                maxLength: 450,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ModifiedDate",
                table: "PlugLot",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Notes",
                table: "PlugLot",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FarmSiteID1",
                table: "PlugLocation",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SeedLotFarmBed",
                columns: table => new
                {
                    SeedLotID = table.Column<int>(nullable: false),
                    FarmBedID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeedLotFarmBed", x => new { x.SeedLotID, x.FarmBedID });
                    table.ForeignKey(
                        name: "FK_SeedLotFarmBed_FarmBeds_FarmBedID",
                        column: x => x.FarmBedID,
                        principalTable: "FarmBeds",
                        principalColumn: "FarmBedID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SeedLotFarmBed_SeedLot_SeedLotID",
                        column: x => x.SeedLotID,
                        principalTable: "SeedLot",
                        principalColumn: "SeedLotID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlugSoilFormula_PlugSoilComponentID",
                table: "PlugSoilFormula",
                column: "PlugSoilComponentID");

            migrationBuilder.CreateIndex(
                name: "IX_PlugSoilFormula_PlugSoilID1",
                table: "PlugSoilFormula",
                column: "PlugSoilID1");

            migrationBuilder.CreateIndex(
                name: "IX_PlugMaintenanceAction_PlugLotID1",
                table: "PlugMaintenanceAction",
                column: "PlugLotID1");

            migrationBuilder.CreateIndex(
                name: "IX_PlugLotObservations_PlugLotID1",
                table: "PlugLotObservations",
                column: "PlugLotID1");

            migrationBuilder.CreateIndex(
                name: "IX_PlugLotLocationTransaction_PlugLocationID",
                table: "PlugLotLocationTransaction",
                column: "PlugLocationID");

            //migrationBuilder.CreateIndex(
            //    name: "IX_PlugLot_OrganizationID",
            //    table: "PlugLot",
            //    column: "OrganizationID");

            migrationBuilder.CreateIndex(
                name: "IX_PlugLot_PlugContainerID",
                table: "PlugLot",
                column: "PlugContainerID");

            migrationBuilder.CreateIndex(
                name: "IX_PlugLot_PlugSoilID",
                table: "PlugLot",
                column: "PlugSoilID");

            migrationBuilder.CreateIndex(
                name: "IX_PlugLot_SeedLotID",
                table: "PlugLot",
                column: "SeedLotID");

            migrationBuilder.CreateIndex(
                name: "IX_PlugLocation_FarmSiteID1",
                table: "PlugLocation",
                column: "FarmSiteID1");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotFarmBed_FarmBedID",
                table: "SeedLotFarmBed",
                column: "FarmBedID");

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLocation_FarmSite_FarmSiteID1",
                table: "PlugLocation",
                column: "FarmSiteID1",
                principalTable: "FarmSite",
                principalColumn: "FarmSiteID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLot_Organization_OrganizationID",
                table: "PlugLot",
                column: "OrganizationID",
                principalTable: "Organization",
                principalColumn: "OrganizationID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLot_PlugContainer_PlugContainerID",
                table: "PlugLot",
                column: "PlugContainerID",
                principalTable: "PlugContainer",
                principalColumn: "PlugContainerID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLot_PlugSoil_PlugSoilID",
                table: "PlugLot",
                column: "PlugSoilID",
                principalTable: "PlugSoil",
                principalColumn: "PlugSoilID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLot_SeedLot_SeedLotID",
                table: "PlugLot",
                column: "SeedLotID",
                principalTable: "SeedLot",
                principalColumn: "SeedLotID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLotLocationTransaction_PlugLocation_PlugLocationID",
                table: "PlugLotLocationTransaction",
                column: "PlugLocationID",
                principalTable: "PlugLocation",
                principalColumn: "PlugLocationID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLotObservations_PlugLot_PlugLotID1",
                table: "PlugLotObservations",
                column: "PlugLotID1",
                principalTable: "PlugLot",
                principalColumn: "PlugLotID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugMaintenanceAction_PlugLot_PlugLotID1",
                table: "PlugMaintenanceAction",
                column: "PlugLotID1",
                principalTable: "PlugLot",
                principalColumn: "PlugLotID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugSoilFormula_PlugSoilComponent_PlugSoilComponentID",
                table: "PlugSoilFormula",
                column: "PlugSoilComponentID",
                principalTable: "PlugSoilComponent",
                principalColumn: "PlugSoilComponentID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugSoilFormula_PlugSoil_PlugSoilID1",
                table: "PlugSoilFormula",
                column: "PlugSoilID1",
                principalTable: "PlugSoil",
                principalColumn: "PlugSoilID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlugLocation_FarmSite_FarmSiteID1",
                table: "PlugLocation");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugLot_Organization_OrganizationID",
                table: "PlugLot");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugLot_PlugContainer_PlugContainerID",
                table: "PlugLot");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugLot_PlugSoil_PlugSoilID",
                table: "PlugLot");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugLot_SeedLot_SeedLotID",
                table: "PlugLot");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugLotLocationTransaction_PlugLocation_PlugLocationID",
                table: "PlugLotLocationTransaction");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugLotObservations_PlugLot_PlugLotID1",
                table: "PlugLotObservations");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugMaintenanceAction_PlugLot_PlugLotID1",
                table: "PlugMaintenanceAction");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugSoilFormula_PlugSoilComponent_PlugSoilComponentID",
                table: "PlugSoilFormula");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugSoilFormula_PlugSoil_PlugSoilID1",
                table: "PlugSoilFormula");

            migrationBuilder.DropTable(
                name: "SeedLotFarmBed");

            migrationBuilder.DropIndex(
                name: "IX_PlugSoilFormula_PlugSoilComponentID",
                table: "PlugSoilFormula");

            migrationBuilder.DropIndex(
                name: "IX_PlugSoilFormula_PlugSoilID1",
                table: "PlugSoilFormula");

            migrationBuilder.DropIndex(
                name: "IX_PlugMaintenanceAction_PlugLotID1",
                table: "PlugMaintenanceAction");

            migrationBuilder.DropIndex(
                name: "IX_PlugLotObservations_PlugLotID1",
                table: "PlugLotObservations");

            migrationBuilder.DropIndex(
                name: "IX_PlugLotLocationTransaction_PlugLocationID",
                table: "PlugLotLocationTransaction");

            migrationBuilder.DropIndex(
                name: "IX_PlugLot_OrganizationID",
                table: "PlugLot");

            migrationBuilder.DropIndex(
                name: "IX_PlugLot_PlugContainerID",
                table: "PlugLot");

            migrationBuilder.DropIndex(
                name: "IX_PlugLot_PlugSoilID",
                table: "PlugLot");

            migrationBuilder.DropIndex(
                name: "IX_PlugLot_SeedLotID",
                table: "PlugLot");

            migrationBuilder.DropIndex(
                name: "IX_PlugLocation_FarmSiteID1",
                table: "PlugLocation");

            migrationBuilder.DropColumn(
                name: "PlugSoilID1",
                table: "PlugSoilFormula");

            migrationBuilder.DropColumn(
                name: "PlugLotID1",
                table: "PlugMaintenanceAction");

            migrationBuilder.DropColumn(
                name: "PlugLotID1",
                table: "PlugLotObservations");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "PlugLotLocationTransaction");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "PlugLotLocationTransaction");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "PlugLotLocationTransaction");

            migrationBuilder.DropColumn(
                name: "ModifiedDate",
                table: "PlugLotLocationTransaction");

            migrationBuilder.DropColumn(
                name: "PlugLocationID",
                table: "PlugLotLocationTransaction");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "PlugLot");

            migrationBuilder.DropColumn(
                name: "CreationDate",
                table: "PlugLot");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "PlugLot");

            migrationBuilder.DropColumn(
                name: "ModifiedDate",
                table: "PlugLot");

            migrationBuilder.DropColumn(
                name: "Notes",
                table: "PlugLot");

            migrationBuilder.DropColumn(
                name: "FarmSiteID1",
                table: "PlugLocation");

            migrationBuilder.AlterColumn<string>(
                name: "OrganizationID",
                table: "PlugLot",
                nullable: true,
                oldClrType: typeof(int));

            //migrationBuilder.CreateTable(
            //    name: "SeedLotBets",
            //    columns: table => new
            //    {
            //        SeedLotID = table.Column<int>(nullable: false)
            //            .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
            //        FarmBedID = table.Column<int>(nullable: false),
            //        SeedLotID1 = table.Column<int>(nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_SeedLotBets", x => x.SeedLotID);
            //        table.ForeignKey(
            //            name: "FK_SeedLotBets_SeedLot_SeedLotID1",
            //            column: x => x.SeedLotID1,
            //            principalTable: "SeedLot",
            //            principalColumn: "SeedLotID",
            //            onDelete: ReferentialAction.Restrict);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_SeedLotBets_SeedLotID1",
            //    table: "SeedLotBets",
            //    column: "SeedLotID1");
        }
    }
}
