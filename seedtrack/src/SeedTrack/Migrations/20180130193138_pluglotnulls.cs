﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeedTrack.Migrations
{
    public partial class pluglotnulls : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlugLot_PlugContainer_PlugContainerID",
                table: "PlugLot");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugLot_PlugSoil_PlugSoilID",
                table: "PlugLot");

            migrationBuilder.AlterColumn<int>(
                name: "SeedsPerPlug",
                table: "PlugLot",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PlugSoilID",
                table: "PlugLot",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PlugContainerID",
                table: "PlugLot",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.CreateTable(
                name: "PlugLotTransaction",
                columns: table => new
                {
                    PlugLotTransactionID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 450, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 450, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Notes = table.Column<string>(type: "ntext", nullable: true),
                    OrganizationID = table.Column<int>(nullable: true),
                    PersonID = table.Column<int>(nullable: true),
                    PlugLotID = table.Column<int>(nullable: true),
                    PlugQuantity = table.Column<int>(nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugLotTransaction", x => x.PlugLotTransactionID);
                    table.ForeignKey(
                        name: "FK_PlugLotTransaction_Organization_OrganizationID",
                        column: x => x.OrganizationID,
                        principalTable: "Organization",
                        principalColumn: "OrganizationID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PlugLotTransaction_Person_PersonID",
                        column: x => x.PersonID,
                        principalTable: "Person",
                        principalColumn: "PersonID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlugLotTransaction_OrganizationID",
                table: "PlugLotTransaction",
                column: "OrganizationID");

            migrationBuilder.CreateIndex(
                name: "IX_PlugLotTransaction_PersonID",
                table: "PlugLotTransaction",
                column: "PersonID");

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLot_PlugContainer_PlugContainerID",
                table: "PlugLot",
                column: "PlugContainerID",
                principalTable: "PlugContainer",
                principalColumn: "PlugContainerID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLot_PlugSoil_PlugSoilID",
                table: "PlugLot",
                column: "PlugSoilID",
                principalTable: "PlugSoil",
                principalColumn: "PlugSoilID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PlugLot_PlugContainer_PlugContainerID",
                table: "PlugLot");

            migrationBuilder.DropForeignKey(
                name: "FK_PlugLot_PlugSoil_PlugSoilID",
                table: "PlugLot");

            migrationBuilder.DropTable(
                name: "PlugLotTransaction");

            migrationBuilder.AlterColumn<int>(
                name: "SeedsPerPlug",
                table: "PlugLot",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PlugSoilID",
                table: "PlugLot",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PlugContainerID",
                table: "PlugLot",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLot_PlugContainer_PlugContainerID",
                table: "PlugLot",
                column: "PlugContainerID",
                principalTable: "PlugContainer",
                principalColumn: "PlugContainerID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PlugLot_PlugSoil_PlugSoilID",
                table: "PlugLot",
                column: "PlugSoilID",
                principalTable: "PlugSoil",
                principalColumn: "PlugSoilID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
