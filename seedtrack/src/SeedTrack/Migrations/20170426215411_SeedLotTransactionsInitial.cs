﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class SeedLotTransactionsInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SeedLotTransaction_Organization_FromOrgID",
                table: "SeedLotTransaction");

            migrationBuilder.DropForeignKey(
                name: "FK_SeedLotTransaction_Person_PersonID",
                table: "SeedLotTransaction");

            migrationBuilder.DropForeignKey(
                name: "FK_SeedLotTransaction_Organization_ToOrgID",
                table: "SeedLotTransaction");

            migrationBuilder.DropIndex(
                name: "IX_SeedLotTransaction_FromOrgID",
                table: "SeedLotTransaction");

            migrationBuilder.DropIndex(
                name: "IX_SeedLotTransaction_ToOrgID",
                table: "SeedLotTransaction");

            migrationBuilder.DropColumn(
                name: "FromOrgID",
                table: "SeedLotTransaction");

            migrationBuilder.DropColumn(
                name: "ToOrgID",
                table: "SeedLotTransaction");

            migrationBuilder.AlterColumn<int>(
                name: "PersonID",
                table: "SeedLotTransaction",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "OldTransactionID",
                table: "SeedLotTransaction",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "OrganizationID",
                table: "SeedLotTransaction",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotTransaction_OrganizationID",
                table: "SeedLotTransaction",
                column: "OrganizationID");

            migrationBuilder.AddForeignKey(
                name: "FK_SeedLotTransaction_Organization_OrganizationID",
                table: "SeedLotTransaction",
                column: "OrganizationID",
                principalTable: "Organization",
                principalColumn: "OrganizationID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SeedLotTransaction_Person_PersonID",
                table: "SeedLotTransaction",
                column: "PersonID",
                principalTable: "Person",
                principalColumn: "PersonID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SeedLotTransaction_Organization_OrganizationID",
                table: "SeedLotTransaction");

            migrationBuilder.DropForeignKey(
                name: "FK_SeedLotTransaction_Person_PersonID",
                table: "SeedLotTransaction");

            migrationBuilder.DropIndex(
                name: "IX_SeedLotTransaction_OrganizationID",
                table: "SeedLotTransaction");

            migrationBuilder.DropColumn(
                name: "OldTransactionID",
                table: "SeedLotTransaction");

            migrationBuilder.DropColumn(
                name: "OrganizationID",
                table: "SeedLotTransaction");

            migrationBuilder.AlterColumn<int>(
                name: "PersonID",
                table: "SeedLotTransaction",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FromOrgID",
                table: "SeedLotTransaction",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ToOrgID",
                table: "SeedLotTransaction",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotTransaction_FromOrgID",
                table: "SeedLotTransaction",
                column: "FromOrgID");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotTransaction_ToOrgID",
                table: "SeedLotTransaction",
                column: "ToOrgID");

            migrationBuilder.AddForeignKey(
                name: "FK_SeedLotTransaction_Organization_FromOrgID",
                table: "SeedLotTransaction",
                column: "FromOrgID",
                principalTable: "Organization",
                principalColumn: "OrganizationID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SeedLotTransaction_Person_PersonID",
                table: "SeedLotTransaction",
                column: "PersonID",
                principalTable: "Person",
                principalColumn: "PersonID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SeedLotTransaction_Organization_ToOrgID",
                table: "SeedLotTransaction",
                column: "ToOrgID",
                principalTable: "Organization",
                principalColumn: "OrganizationID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
