﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeedTrack.Migrations
{
    public partial class PlugLots : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PlugContainer",
                columns: table => new
                {
                    PlugContainerID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlugContainerName = table.Column<string>(nullable: true),
                    PlugVolume = table.Column<float>(nullable: false),
                    PlugsPerTray = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugContainer", x => x.PlugContainerID);
                });

            migrationBuilder.CreateTable(
                name: "PlugLocation",
                columns: table => new
                {
                    PlugLocationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FarmSiteID = table.Column<string>(nullable: true),
                    PlugLocationName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugLocation", x => x.PlugLocationID);
                });

            migrationBuilder.CreateTable(
                name: "PlugLot",
                columns: table => new
                {
                    PlugLotID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    LotTag = table.Column<string>(maxLength: 40, nullable: true),
                    OrganizationID = table.Column<string>(nullable: true),
                    PlugContainerID = table.Column<int>(nullable: false),
                    PlugLotNumber = table.Column<int>(nullable: false),
                    PlugSoilID = table.Column<int>(nullable: false),
                    SeedLotID = table.Column<int>(nullable: false),
                    SeedPretreatment = table.Column<string>(nullable: true),
                    SeedQuantityUsed = table.Column<float>(nullable: false),
                    SeedsPerPlug = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugLot", x => x.PlugLotID);
                });

            migrationBuilder.CreateTable(
                name: "PlugLotLocationTransaction",
                columns: table => new
                {
                    PlugLotID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlugLotLocationID = table.Column<int>(nullable: false),
                    PlugLotMovementDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugLotLocationTransaction", x => x.PlugLotID);
                });

            migrationBuilder.CreateTable(
                name: "PlugLotObservations",
                columns: table => new
                {
                    PlugLotID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlugLotObservation = table.Column<string>(nullable: true),
                    PlugLotObservationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugLotObservations", x => x.PlugLotID);
                });

            migrationBuilder.CreateTable(
                name: "PlugMaintenanceAction",
                columns: table => new
                {
                    PlugLotID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlugMaintenanceActionDate = table.Column<DateTime>(nullable: false),
                    PlugMaintenanceActionNotes = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlugMaintenanceAction", x => x.PlugLotID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BedTransaction_PlugLotID",
                table: "BedTransaction",
                column: "PlugLotID");

            migrationBuilder.AddForeignKey(
                name: "FK_BedTransaction_PlugLot_PlugLotID",
                table: "BedTransaction",
                column: "PlugLotID",
                principalTable: "PlugLot",
                principalColumn: "PlugLotID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BedTransaction_PlugLot_PlugLotID",
                table: "BedTransaction");

            migrationBuilder.DropTable(
                name: "PlugContainer");

            migrationBuilder.DropTable(
                name: "PlugLocation");

            migrationBuilder.DropTable(
                name: "PlugLot");

            migrationBuilder.DropTable(
                name: "PlugLotLocationTransaction");

            migrationBuilder.DropTable(
                name: "PlugLotObservations");

            migrationBuilder.DropTable(
                name: "PlugMaintenanceAction");

            migrationBuilder.DropIndex(
                name: "IX_BedTransaction_PlugLotID",
                table: "BedTransaction");
        }
    }
}
