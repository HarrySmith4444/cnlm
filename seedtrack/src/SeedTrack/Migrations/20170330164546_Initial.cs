﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeedTrack.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    AddressID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    City = table.Column<string>(maxLength: 80, nullable: true),
                    Country = table.Column<string>(maxLength: 60, nullable: true),
                    FirstLine = table.Column<string>(maxLength: 80, nullable: true),
                    SecondLine = table.Column<string>(maxLength: 80, nullable: true),
                    State = table.Column<string>(maxLength: 60, nullable: true),
                    ZipCode = table.Column<string>(maxLength: 12, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.AddressID);
                });

            migrationBuilder.CreateTable(
                name: "FarmSite",
                columns: table => new
                {
                    FarmSiteID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FarmCode = table.Column<string>(maxLength: 10, nullable: true),
                    FarmName = table.Column<string>(maxLength: 120, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FarmSite", x => x.FarmSiteID);
                });

            migrationBuilder.CreateTable(
                name: "Species",
                columns: table => new
                {
                    SpeciesID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Clade = table.Column<string>(nullable: true),
                    Codon = table.Column<string>(nullable: true),
                    CommonName = table.Column<string>(nullable: true),
                    Genus = table.Column<string>(nullable: true),
                    LifeCycle = table.Column<string>(nullable: true),
                    PctCoverOneAdult = table.Column<float>(nullable: false),
                    RestorationCharacteristics = table.Column<long>(nullable: false),
                    RootStructure = table.Column<string>(nullable: true),
                    SeedDormancyType = table.Column<string>(nullable: true),
                    SpeciesName = table.Column<string>(nullable: true),
                    SubTaxa = table.Column<string>(nullable: true),
                    Synonyms = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Species", x => x.SpeciesID);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FarmBeds",
                columns: table => new
                {
                    FarmBedID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BedType = table.Column<string>(nullable: true),
                    FarmSiteID = table.Column<int>(nullable: false),
                    IrrigationFlowRate = table.Column<float>(nullable: false),
                    IrrigationType = table.Column<string>(nullable: true),
                    Length = table.Column<float>(nullable: false),
                    Width = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FarmBeds", x => x.FarmBedID);
                    table.ForeignKey(
                        name: "FK_FarmBeds_FarmSite_FarmSiteID",
                        column: x => x.FarmSiteID,
                        principalTable: "FarmSite",
                        principalColumn: "FarmSiteID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Organization",
                columns: table => new
                {
                    OrganizationID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddressID = table.Column<int>(nullable: true),
                    ContactPersonID = table.Column<int>(nullable: true),
                    OrganizationName = table.Column<string>(maxLength: 120, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Organization", x => x.OrganizationID);
                    table.ForeignKey(
                        name: "FK_Organization_Address_AddressID",
                        column: x => x.AddressID,
                        principalTable: "Address",
                        principalColumn: "AddressID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Person",
                columns: table => new
                {
                    PersonID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddressID = table.Column<int>(nullable: true),
                    CellPhone = table.Column<string>(maxLength: 20, nullable: true),
                    Email = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(maxLength: 80, nullable: true),
                    IsContactPerson = table.Column<bool>(nullable: false),
                    LastName = table.Column<string>(maxLength: 80, nullable: true),
                    OrganizationID = table.Column<int>(nullable: true),
                    Phone = table.Column<string>(maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.PersonID);
                    table.ForeignKey(
                        name: "FK_Person_Address_AddressID",
                        column: x => x.AddressID,
                        principalTable: "Address",
                        principalColumn: "AddressID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Person_Organization_OrganizationID",
                        column: x => x.OrganizationID,
                        principalTable: "Organization",
                        principalColumn: "OrganizationID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SeedLot",
                columns: table => new
                {
                    SeedLotID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 450, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    FinalHarvestDate = table.Column<DateTime>(nullable: false),
                    FirstHarvestDate = table.Column<DateTime>(nullable: false),
                    Generation = table.Column<int>(nullable: false),
                    Grade = table.Column<string>(maxLength: 1, nullable: true),
                    HarvestedQuantity = table.Column<float>(nullable: false),
                    LotTag = table.Column<string>(maxLength: 40, nullable: true),
                    ModifiedBy = table.Column<string>(maxLength: 450, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Notes = table.Column<string>(type: "ntext", nullable: true),
                    OldLotNo = table.Column<string>(maxLength: 40, nullable: true),
                    SourceOrgID = table.Column<int>(nullable: false),
                    SourceSiteID = table.Column<int>(nullable: false),
                    SourceType = table.Column<int>(nullable: false),
                    SpeciesID = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeedLot", x => x.SeedLotID);
                    table.ForeignKey(
                        name: "FK_SeedLot_Organization_SourceOrgID",
                        column: x => x.SourceOrgID,
                        principalTable: "Organization",
                        principalColumn: "OrganizationID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SeedLot_Species_SpeciesID",
                        column: x => x.SpeciesID,
                        principalTable: "Species",
                        principalColumn: "SpeciesID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SeedLotBets",
                columns: table => new
                {
                    SeedLotID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FarmBedID = table.Column<int>(nullable: false),
                    SeedLotID1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeedLotBets", x => x.SeedLotID);
                    table.ForeignKey(
                        name: "FK_SeedLotBets_SeedLot_SeedLotID1",
                        column: x => x.SeedLotID1,
                        principalTable: "SeedLot",
                        principalColumn: "SeedLotID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SeedLotRegion",
                columns: table => new
                {
                    SeedLotID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Level4RegionID = table.Column<int>(nullable: false),
                    SeedLotID1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeedLotRegion", x => x.SeedLotID);
                    table.ForeignKey(
                        name: "FK_SeedLotRegion_SeedLot_SeedLotID1",
                        column: x => x.SeedLotID1,
                        principalTable: "SeedLot",
                        principalColumn: "SeedLotID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SeedLotTransaction",
                columns: table => new
                {
                    SeedLotTransactionID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(maxLength: 450, nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    FromOrgID = table.Column<int>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 450, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: false),
                    Notes = table.Column<string>(type: "ntext", nullable: true),
                    PersonID = table.Column<int>(nullable: false),
                    Quantity = table.Column<float>(nullable: false),
                    SeedLotID = table.Column<int>(nullable: false),
                    ToOrgID = table.Column<int>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeedLotTransaction", x => x.SeedLotTransactionID);
                    table.ForeignKey(
                        name: "FK_SeedLotTransaction_Organization_FromOrgID",
                        column: x => x.FromOrgID,
                        principalTable: "Organization",
                        principalColumn: "OrganizationID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SeedLotTransaction_Person_PersonID",
                        column: x => x.PersonID,
                        principalTable: "Person",
                        principalColumn: "PersonID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SeedLotTransaction_SeedLot_SeedLotID",
                        column: x => x.SeedLotID,
                        principalTable: "SeedLot",
                        principalColumn: "SeedLotID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SeedLotTransaction_Organization_ToOrgID",
                        column: x => x.ToOrgID,
                        principalTable: "Organization",
                        principalColumn: "OrganizationID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_FarmBeds_FarmSiteID",
                table: "FarmBeds",
                column: "FarmSiteID");

            migrationBuilder.CreateIndex(
                name: "IX_Organization_AddressID",
                table: "Organization",
                column: "AddressID");

            migrationBuilder.CreateIndex(
                name: "IX_Organization_ContactPersonID",
                table: "Organization",
                column: "ContactPersonID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Person_AddressID",
                table: "Person",
                column: "AddressID");

            migrationBuilder.CreateIndex(
                name: "IX_Person_OrganizationID",
                table: "Person",
                column: "OrganizationID");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLot_SourceOrgID",
                table: "SeedLot",
                column: "SourceOrgID");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLot_SpeciesID",
                table: "SeedLot",
                column: "SpeciesID");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotBets_SeedLotID1",
                table: "SeedLotBets",
                column: "SeedLotID1");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotRegion_SeedLotID1",
                table: "SeedLotRegion",
                column: "SeedLotID1");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotTransaction_FromOrgID",
                table: "SeedLotTransaction",
                column: "FromOrgID");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotTransaction_PersonID",
                table: "SeedLotTransaction",
                column: "PersonID");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotTransaction_SeedLotID",
                table: "SeedLotTransaction",
                column: "SeedLotID");

            migrationBuilder.CreateIndex(
                name: "IX_SeedLotTransaction_ToOrgID",
                table: "SeedLotTransaction",
                column: "ToOrgID");

            migrationBuilder.AddForeignKey(
                name: "FK_Organization_Person_ContactPersonID",
                table: "Organization",
                column: "ContactPersonID",
                principalTable: "Person",
                principalColumn: "PersonID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Organization_Address_AddressID",
                table: "Organization");

            migrationBuilder.DropForeignKey(
                name: "FK_Person_Address_AddressID",
                table: "Person");

            migrationBuilder.DropForeignKey(
                name: "FK_Organization_Person_ContactPersonID",
                table: "Organization");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "FarmBeds");

            migrationBuilder.DropTable(
                name: "SeedLotBets");

            migrationBuilder.DropTable(
                name: "SeedLotRegion");

            migrationBuilder.DropTable(
                name: "SeedLotTransaction");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "FarmSite");

            migrationBuilder.DropTable(
                name: "SeedLot");

            migrationBuilder.DropTable(
                name: "Species");

            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropTable(
                name: "Person");

            migrationBuilder.DropTable(
                name: "Organization");
        }
    }
}
