﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class PlugLotQuant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "PlugLot",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 450,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CurrentQuantity",
                table: "PlugLot",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "State",
                table: "PlugLot",
                maxLength: 450,
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentQuantity",
                table: "PlugLot");

            migrationBuilder.DropColumn(
                name: "State",
                table: "PlugLot");

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "PlugLot",
                maxLength: 450,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
