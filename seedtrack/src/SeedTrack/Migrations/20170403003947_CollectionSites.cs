﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SeedTrack.Migrations
{
    public partial class CollectionSites : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CollectionSite",
                columns: table => new
                {
                    CollectionSiteID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Level4RegionID = table.Column<int>(nullable: false),
                    Notes = table.Column<string>(type: "ntext", nullable: true),
                    SiteCode = table.Column<string>(maxLength: 10, nullable: true),
                    SiteName = table.Column<string>(maxLength: 120, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CollectionSite", x => x.CollectionSiteID);
                    table.ForeignKey(
                        name: "FK_CollectionSite_Level4Region_Level4RegionID",
                        column: x => x.Level4RegionID,
                        principalTable: "Level4Region",
                        principalColumn: "Level4RegionID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CollectionSite_Level4RegionID",
                table: "CollectionSite",
                column: "Level4RegionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CollectionSite");
        }
    }
}
