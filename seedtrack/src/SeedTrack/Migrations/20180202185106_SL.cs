﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SeedTrack.Migrations
{
    public partial class SL : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RegionID",
                table: "SeedLot",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ColdStratTemp",
                table: "PlugLot",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ImbibeTime",
                table: "PlugLot",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SeedLot_RegionID",
                table: "SeedLot",
                column: "RegionID");

            migrationBuilder.AddForeignKey(
                name: "FK_SeedLot_Level4Region_RegionID",
                table: "SeedLot",
                column: "RegionID",
                principalTable: "Level4Region",
                principalColumn: "Level4RegionID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SeedLot_Level4Region_RegionID",
                table: "SeedLot");

            migrationBuilder.DropIndex(
                name: "IX_SeedLot_RegionID",
                table: "SeedLot");

            migrationBuilder.DropColumn(
                name: "RegionID",
                table: "SeedLot");

            migrationBuilder.DropColumn(
                name: "ColdStratTemp",
                table: "PlugLot");

            migrationBuilder.DropColumn(
                name: "ImbibeTime",
                table: "PlugLot");
        }
    }
}
