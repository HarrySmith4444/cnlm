using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotModels;
using Microsoft.AspNetCore.Authorization;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class FarmBedsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FarmBedsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: FarmBeds
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.FarmBeds.Include(f => f.FarmSite);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: FarmBeds/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var applicationDbContext = _context.FarmBeds.Include(f => f.FarmSite);
            var farmBed = await applicationDbContext.SingleOrDefaultAsync(m => m.FarmBedID == id);
            if (farmBed == null)
            {
                return NotFound();
            }

            return View(farmBed);
        }

        // GET: FarmBeds/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult Create()
        {
            ViewData["FarmSiteID"] = new SelectList(_context.FarmSite, "FarmSiteID", "FarmName");
           
            List<SelectListItem> bedTypeList = MakeEnumDropDown(typeof( BedTypes), "");
            List<SelectListItem> irrigationTypeList = MakeEnumDropDown(typeof(IrrigationTypes), "");

            ViewBag.BedTypes = bedTypeList;
            ViewBag.IrrigationTypes = irrigationTypeList;
            return View();
        }

        // POST: FarmBeds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Create([Bind("FarmBedID,FarmBedTag,BedType,FarmSiteID,IrrigationFlowRate,IrrigationType,Length,Width")] FarmBed farmBed)
        {
            if (ModelState.IsValid)
            {
                _context.Add(farmBed);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["FarmSiteID"] = new SelectList(_context.FarmSite, "FarmSiteID", "FarmName", farmBed.FarmSiteID);
            return View(farmBed);
        }

        // GET: FarmBeds/Edit/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var farmBed = await _context.FarmBeds.SingleOrDefaultAsync(m => m.FarmBedID == id);
            if (farmBed == null)
            {
                return NotFound();
            }
            List<SelectListItem> bedTypeList = MakeEnumDropDown(typeof(BedTypes), farmBed.BedType);
            List<SelectListItem> irrigationTypeList = MakeEnumDropDown(typeof(IrrigationTypes),farmBed.IrrigationType);

            ViewBag.BedTypes = bedTypeList;
            ViewBag.IrrigationTypes = irrigationTypeList;

            ViewData["FarmSiteID"] = new SelectList(_context.FarmSite, "FarmSiteID", "FarmName", farmBed.FarmSiteID);
            return View(farmBed);
        }

        [Authorize(Roles = "Administrator, PowerUser")]
        // POST: FarmBeds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FarmBedID,FarmBedTag,BedType,FarmSiteID,IrrigationFlowRate,IrrigationType,Length,Width")] FarmBed farmBed)
        {
            if (id != farmBed.FarmBedID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(farmBed);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FarmBedExists(farmBed.FarmBedID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["FarmSiteID"] = new SelectList(_context.FarmSite, "FarmSiteID", "FarmSiteID", farmBed.FarmSiteID);
            return View(farmBed);
        }

        // GET: FarmBeds/Delete/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var farmBed = await _context.FarmBeds.SingleOrDefaultAsync(m => m.FarmBedID == id);
            if (farmBed == null)
            {
                return NotFound();
            }
            FarmLotViewModel model = new FarmLotViewModel(farmBed);
            return View(model);
        }

        // POST: FarmBeds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var farmBed = await _context.FarmBeds.SingleOrDefaultAsync(m => m.FarmBedID == id);
            _context.FarmBeds.Remove(farmBed);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool FarmBedExists(int id)
        {
            return _context.FarmBeds.Any(e => e.FarmBedID == id);
        }
        public List<SelectListItem> MakeEnumDropDown(Type eType, string selectedString="")
        {
            List<SelectListItem> selList = new List<SelectListItem>();
            foreach (var value in Enum.GetNames(eType))
            {
                SelectListItem selItem = new SelectListItem();
                selItem.Text = value;
                selItem.Value = value;
                if (selItem.Value == selectedString)
                    selItem.Selected = true;
                selList.Add(selItem);
            }
            return selList;
        }
    }
}

