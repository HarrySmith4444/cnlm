using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotModels;
using Microsoft.AspNetCore.Authorization;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class Level4RegionController : Controller
    {
        private readonly ApplicationDbContext _context;

        public Level4RegionController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Level4Region
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Level4Region.Include(l => l.Level3Region);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Level4Region/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var applicationDbContext = _context.Level4Region.Include(p => p.Level3Region);
            var level4Region = await applicationDbContext.SingleOrDefaultAsync(m => m.Level4RegionID == id);

//            var level4Region = await _context.Level4Region.SingleOrDefaultAsync(m => m.Level4RegionID == id);
            if (level4Region == null)
            {
                return NotFound();
            }

            return View(level4Region);
        }

        // GET: Level4Region/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult Create()
        {
            ViewData["Level3RegionID"] = new SelectList(_context.Level3Region, "Level3RegionID", "Name");
            return View();
        }

        // POST: Level4Region/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Create([Bind("Level4RegionID,Level3RegionID,Name,RegionCode")] Level4Region level4Region)
        {
            if (ModelState.IsValid)
            {
                _context.Add(level4Region);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["Level3RegionID"] = new SelectList(_context.Level3Region, "Level3RegionID", "Name", level4Region.Level3RegionID);
            return View(level4Region);
        }

        // GET: Level4Region/Edit/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var level4Region = await _context.Level4Region.SingleOrDefaultAsync(m => m.Level4RegionID == id);
            if (level4Region == null)
            {
                return NotFound();
            }
            ViewData["Level3RegionID"] = new SelectList(_context.Level3Region, "Level3RegionID", "Name", level4Region.Level3RegionID);
            return View(level4Region);
        }

        // POST: Level4Region/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int id, [Bind("Level4RegionID,Level3RegionID,Name,RegionCode")] Level4Region level4Region)
        {
            if (id != level4Region.Level4RegionID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(level4Region);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Level4RegionExists(level4Region.Level4RegionID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            
            ViewData["Level3RegionID"] = new SelectList(_context.Level3Region, "Level3RegionID", "Name", level4Region.Level3RegionID);
            return View(level4Region);
        }

        // GET: Level4Region/Delete/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var applicationDbContext = _context.Level4Region.Include(p => p.Level3Region);
            var level4Region = await applicationDbContext.SingleOrDefaultAsync(m => m.Level4RegionID == id);
//            var level4Region = await _context.Level4Region.SingleOrDefaultAsync(m => m.Level4RegionID == id);
            if (level4Region == null)
            {
                return NotFound();
            }

            return View(level4Region);
        }

        // POST: Level4Region/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var level4Region = await _context.Level4Region.SingleOrDefaultAsync(m => m.Level4RegionID == id);
            _context.Level4Region.Remove(level4Region);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool Level4RegionExists(int id)
        {
            return _context.Level4Region.Any(e => e.Level4RegionID == id);
        }
        //GET: /Level4Region/RegionsAndSites
        [HttpGet]
        public IActionResult RegionsAndSites()
        {
            return View();
        }
    }

}
