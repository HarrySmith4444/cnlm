using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.PlugLotModels;

namespace SeedTrack.Controllers
{
    public class PlugLocationsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlugLocationsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: PlugLocations
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlugLocation.ToListAsync());
        }

        // GET: PlugLocations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLocation = await _context.PlugLocation.SingleOrDefaultAsync(m => m.PlugLocationID == id);
            if (plugLocation == null)
            {
                return NotFound();
            }

            return View(plugLocation);
        }

        // GET: PlugLocations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlugLocations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlugLocationID,FarmSiteID,PlugLocationName")] PlugLocation plugLocation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plugLocation);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(plugLocation);
        }

        // GET: PlugLocations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLocation = await _context.PlugLocation.SingleOrDefaultAsync(m => m.PlugLocationID == id);
            if (plugLocation == null)
            {
                return NotFound();
            }
            return View(plugLocation);
        }

        // POST: PlugLocations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlugLocationID,FarmSiteID,PlugLocationName")] PlugLocation plugLocation)
        {
            if (id != plugLocation.PlugLocationID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plugLocation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlugLocationExists(plugLocation.PlugLocationID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(plugLocation);
        }

        // GET: PlugLocations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLocation = await _context.PlugLocation.SingleOrDefaultAsync(m => m.PlugLocationID == id);
            if (plugLocation == null)
            {
                return NotFound();
            }

            return View(plugLocation);
        }

        // POST: PlugLocations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plugLocation = await _context.PlugLocation.SingleOrDefaultAsync(m => m.PlugLocationID == id);
            _context.PlugLocation.Remove(plugLocation);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlugLocationExists(int id)
        {
            return _context.PlugLocation.Any(e => e.PlugLocationID == id);
        }
    }
}
