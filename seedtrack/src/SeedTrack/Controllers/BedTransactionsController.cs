using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models;
using SeedTrack.Models.FarmModels;
using SeedTrack.Models.SeedLotModels;
using SeedTrack.Models.PlugLotModels;
using Microsoft.AspNetCore.Authorization;
using SeedTrack.Models.SeedLotTransactionModels;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class BedTransactionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BedTransactionsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: BedTransactions
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.BedTransaction.Include(b => b.FarmBed).Include(b => b.PlugLot).Include(b => b.SeedLot);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: BedTransactions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bedTransaction = await _context.BedTransaction.SingleOrDefaultAsync(m => m.BedTransactionID == id);
            if (bedTransaction == null)
            {
                return NotFound();
            }

            return View(bedTransaction);
        }

        // GET: BedTransactions/Create
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public IActionResult Create()
        {

            // find just the farms that have beds
            var res = from f in _context.FarmSite
                      join b in _context.FarmBeds on f.FarmSiteID equals b.FarmSiteID
                      select  f ;
            // and only one each of those please
            ViewData["FarmList"] = new SelectList(res.Distinct(),
                "FarmSiteID", "FarmName");
            ViewData["Error"] = "";
            // constructors do all the initialization of the bed transaction
            BedTransactionViewModel vw = new BedTransactionViewModel();
            // set some initial conditions
            //vw.bedTransaction.BedFeet = Constants.DEFAULT_BED_FEET_PLANTED;
            ////vw.bedTransaction.FarmBedID = 0;
            //vw.bedTransaction.PlantSpacing = Constants.DEFAULT_PLANT_SPACING;
            //vw.bedTransaction.PlugLotID = 0;
            //vw.bedTransaction.SeedLotID = 0;
            //vw.bedTransaction.PlugQuantity = Constants.DEFAULT_PLUG_QUANTITY;
            //vw.bedTransaction.RowsPerBed = Constants.DEFAULT_ROWS_PER_BED;
            //vw.bedTransaction.SeedQuantity = Constants.DEFAULT_SEED_QUANTITY;
            //vw.bedTransaction.TransactionType = BedTransactionType.Planted;
            //vw.bedTransaction.TransactionDate = DateTime.Now;
            //vw.iSelectedFarm = 0;
            //vw.iSelectedBed = 0;
            //vw.iSelectedAction = (int)BedTransactionType.Sowed;                //.Planted;
            //vw.iTotalBedFeet = 0;

            ViewData["SelectedFarmIndex"] = 0;
            ViewData["SelectedBedIndex"] = 0;
            return View(vw);
        }

      

        // POST: BedTransactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        //       public async Task<IActionResult> Create(BedTransaction Item1, SeedLot Item2)
        public async Task<IActionResult> Create(BedTransactionViewModel model)
        //      public async Task<IActionResult> Create([Bind("BedTransactionID,BedFeet,FarmBedID,Notes,PlantSpacing,PlugLotID,PlugQuantity,RowsPerBed,SeedLotID,SeedQuantity,TransactionDate,TransactionType")] BedTransaction bedTransaction)
        {
            string strError="";
            // set up dropdowns etc.  for return
            var res = from f in _context.FarmSite
                      join b in _context.FarmBeds on f.FarmSiteID equals b.FarmSiteID
                      select f;
            // and only one each of those please
            var x = ViewData["SelectedFarmIndex"];
            ViewData["FarmList"] = new SelectList(res.Distinct(),
                "FarmSiteID", "FarmName");
            ViewData["Error"] = "";
            ViewData["SelectedFarmIndex"] = model.iSelectedFarm;
            ViewData["SelectedBedIndex"] = model.iSelectedBed;
            // null out the transaction id for return trips here, the server handles it
            model.bedTransaction.BedTransactionID = 0;
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //BedTransaction bedTransaction = Item1;
            if (ModelState.IsValid)
            {
                model.bedTransaction.CreatedBy = User.Identity.Name;
                model.bedTransaction.CreationDate = DateTime.Now;
                model.bedTransaction.ModifiedBy = User.Identity.Name;
                model.bedTransaction.ModifiedDate = DateTime.Now;
                switch (model.bedTransaction.TransactionType)
                {
                    case BedTransactionType.Cleared:
                    case BedTransactionType.Removed:
                            model.bedTransaction.PlugLotID = null;
                            model.bedTransaction.BedFeet = 0;
                        // percent removed
                        if (model.bedTransaction.SeedLotID >= 0)
                        {
                            model.bedTransaction.SeedQuantity = (float)(model.bedTransaction.SeedLotID)/(float)100.0;
                        }
                            model.bedTransaction.SeedLotID = null;
                        break;
                    case BedTransactionType.Sowed:
                        model.bedTransaction.PlugLotID = null;
                        // if seedlot ID is passed it is actually the seedlot number and we need to correct it
                        try
                        {
                            if (null != model.bedTransaction.SeedLotID)
                            {
                                var seedLot = await _context.SeedLot.Where(m => m.SeedLotNumber == model.bedTransaction.SeedLotID).FirstOrDefaultAsync();
                                if (null != seedLot)
                                    model.bedTransaction.SeedLotID = seedLot.SeedLotID;
                            }
                        }
                        catch (Exception ex)
                        {
                            strError = "Failed to find seed lot number " + model.bedTransaction.SeedLotID.ToString() + " : " + ex.Message;
                            goto earlyerrorexit;
                        }
                        break;
                    case BedTransactionType.Planted:
                        try { 
                            if (null != model.bedTransaction.PlugLotID)
                            {
                                // change pluglot number to its ID - set the seedlot for provenance and plantings
                                var plugLot = await _context.PlugLot.Where(m => m.PlugLotNumber == model.bedTransaction.PlugLotID).FirstOrDefaultAsync();
                                if (null != plugLot) { 
                                    model.bedTransaction.PlugLotID = plugLot.PlugLotID;
                                    model.bedTransaction.SeedLotID = plugLot.SeedLotID;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            strError = "Failed to find plug lot number " + model.bedTransaction.PlugLotID.ToString() + " : " + ex.Message;
                            goto earlyerrorexit;
                        }
                        break;
                    default:
                        break;
                }
                // make a copy of the model to return as we don't trust the scope of this one
                BedTransactionViewModel model2 = new BedTransactionViewModel();
                model2 = model.Clone();

                // now save the transaction
                try
                {
                    _context.Add(model.bedTransaction);
                    await _context.SaveChangesAsync();
                }
                catch(Exception ex)
                {
                    strError = "Failed to save bed transaction. " + ex.Message;
                    if(null != ex.InnerException) strError += " : " + ex.InnerException.Message;
                    goto earlyerrorexit;
                }
                // we have recorded the bed transaction but if we sowed we need to decrement
                // the seedlot, if we planted we need to decrement the pluglot
                if (model.bedTransaction.TransactionType == BedTransactionType.Sowed)
                {
                    SeedLotTransaction slt = new SeedLotTransaction();
                    slt.CreatedBy = User.Identity.Name;
                    slt.CreationDate = DateTime.Now;
                    slt.Notes = "Seeds used to sow bed " + model.farmName + " " + model.bedTag;
                    slt.TransactionType = SeedLotTransactionType.Disbursement;
                    slt.SeedLotID = (int)model.bedTransaction.SeedLotID;
                    slt.Quantity = (float)(-1.0 * model.bedTransaction.SeedQuantity);
                    slt.TransactionDate = DateTime.Now;
                    slt.OrganizationID = await GetCNLMOrgID();
                    try
                    {
                        _context.SeedLotTransaction.Add(slt);
                        await _context.SaveChangesAsync();
                    }
                    catch(Exception ex)
                    {
                        strError = "Failed to save seed lot transaction to decrement seed lot quantity after sowing." + ex.Message; 
                        if(null != ex.InnerException) strError += " : " + ex.InnerException.Message;
                        goto earlyerrorexit;
                    }
                    // also decrement the quantities stored in the seedlot
                    try { 
                        await SeedLotTransactionsController.UpdateCurrentQuantity(_context, slt.SeedLotID, slt.Quantity, (float)0.0);
                    }
                    catch(Exception ex)
                    {
                        strError = "Failed to update current quantity in seed lot after saving seed lot transaction. " + ex.Message;
                        if (null != ex.InnerException) strError += " : " + ex.InnerException.Message;
                        goto earlyerrorexit;

                    }
                }
             if(model.bedTransaction.TransactionType == BedTransactionType.Planted)
                {
                    // decrement the pluglot
                    PlugLotTransaction plt = new PlugLotTransaction();
                    plt.CreatedBy = User.Identity.Name;
                    plt.CreationDate = DateTime.Now;
                    plt.Notes = "Pluglot used to sow bed " + model.farmName + " " + model.bedTag;
                    plt.TransactionType = PlugLotTransaction.PlugLotTransactionType.Allocated;
                    plt.TransactionDate = DateTime.Now;
                    plt.OrganizationID = await GetCNLMOrgID();
                    plt.PlugLotID = model.bedTransaction.PlugLotID;
                    plt.PlugQuantity = -1 * model.bedTransaction.PlugQuantity;
                    try
                    {
                        _context.PlugLotTransaction.Add(plt);
                        await _context.SaveChangesAsync();
                    }
                    catch (Exception ex) {
                        strError = "Failed to save plug lot transaction to decrement plug lot quantity after planting." + ex.Message;
                        if (null != ex.InnerException) strError += " : " + ex.InnerException.Message;
                        goto earlyerrorexit;
                    }
                    try { 
                    // also decrement the quantities stored in the seedlot
                        await PlugLotsController.UpdateCurrentQuantity(_context, (int)plt.PlugLotID, (int)plt.PlugQuantity);
                    }
                    catch (Exception ex)
                    {
                        strError = "Failed to update current quantity in seed lot after saving seed lot transaction. " + ex.Message;
                        if (null != ex.InnerException) strError += " : " + ex.InnerException.Message;
                        goto earlyerrorexit;
                    }
                }

                int iBedID = (int)model.bedTransaction.FarmBedID;
                object objBedID = iBedID;
                
                return RedirectToAction("EditBed", "BedTransactions", new { bedID = iBedID });
            }
            else
            {
                strError = "Model is invalid when attempting to save bed transaction.";
                //return View(model);
                goto earlyerrorexit;
            }
            earlyerrorexit:
            {
                ViewData["Error"] = strError;
                return View("Create", model);

            }

        }

        // GET: BedTransactions/AddBedActions
        [HttpGet]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        // show a screen to allow more actions against the bed
        public async Task<IActionResult> AddBedActions(int? bedID)
        {
            // build the model from the bedID and call the Create post
            if (null == bedID)
            {
                this.Create();
                return View();
            }
            // get the bed info
            FarmBed fb = await _context.FarmBeds.Where(a => a.FarmBedID == bedID).SingleAsync();
            BedTransactionViewModel btvModel = new BedTransactionViewModel();
            btvModel.bedID = bedID;
            btvModel.bedTag = fb.FarmBedTag;
            
            btvModel.bedTransaction = new BedTransaction();
            btvModel.bedTransaction.FarmBedID = fb.FarmBedID;
            // build the viewbag elements
            var res = from f in _context.FarmSite
                      join b in _context.FarmBeds on f.FarmSiteID equals b.FarmSiteID
                      select f;
            // and only one each of those please
            ViewData["FarmList"] = new SelectList(res.Distinct(),
                "FarmSiteID", "FarmName");
            FarmSite fs = await _context.FarmSite.Where(w => w.FarmSiteID == fb.FarmSiteID).SingleAsync();
            btvModel.farmName = fs.FarmName;

            ViewData["Error"] = "";
            SelectList sl = (SelectList)ViewData["FarmList"];
            // walk the farm list for the selected farm
            int i = 0;
            foreach (SelectListItem sli in sl)
            {
                if(sli.Value == fb.FarmSiteID.ToString())
                {
                    ViewData["SelectedFarmIndex"] = i;
                }
                i++;
            }
            // walk the bed list for the selected bed
            List<FarmBed> fbl = await _context.FarmBeds.Where(b => b.FarmSiteID == fb.FarmSiteID).ToListAsync();
            i = 0;
            foreach(FarmBed b in fbl)
            {
                if(b.FarmBedID == fb.FarmBedID)
                {
                    ViewData["SelectedBedIndex"] = i;
                  
                }
                i++;
            }
            //return View(btvModel);
            BedTransactionViewModel btvNew = btvModel.Clone();
            return View(btvNew);
            //return await Create(btvNew);
            //return R
            ////return RedirectToAction("Create","BedTransactions",btvNew);
        }


        // GET: BedTransactions/EditBed/bedID
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> EditBed(int? bedID)
        {
            if (bedID == null)
            {
                return NotFound();
            }
            // get the details for this bedID
            FarmBed fb = await _context.FarmBeds.Where(f => f.FarmBedID == (int)bedID).SingleOrDefaultAsync();
            if (null == fb)
                return NotFound();

            // get the farm name
            FarmSite fs = await _context.FarmSite.Where(g => g.FarmSiteID == fb.FarmSiteID).SingleOrDefaultAsync();
            if (null == fs)
                return NotFound();

            ViewData["FarmName"] = fs.FarmName;
            ViewData["BedTag"] = fb.FarmBedTag;
            return View(fb);
        }



        // GET: BedTransactions/Edit/5
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bedTransaction = await _context.BedTransaction.SingleOrDefaultAsync(m => m.BedTransactionID == id);
            if (bedTransaction == null)
            {
                return NotFound();
            }
            ViewData["FarmBedID"] = new SelectList(_context.FarmBeds, "FarmBedID", "FarmBedID", bedTransaction.FarmBedID);
            return View(bedTransaction);
        }

        // POST: BedTransactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Edit(int id, [Bind("BedTransactionID,BedFeet,CreatedBy,CreationDate,FarmBedID,ModifiedBy,ModifiedDate,Notes,PlantSpacing,PlugLotID,PlugQuantity,RowsPerBed,SeedLotID,SeedQuantity,TransactionDate,TransactionType")] BedTransaction bedTransaction)
        {
            if (id != bedTransaction.BedTransactionID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bedTransaction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BedTransactionExists(bedTransaction.BedTransactionID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["FarmBedID"] = new SelectList(_context.FarmBeds, "FarmBedID", "FarmBedID", bedTransaction.FarmBedID);
            return View(bedTransaction);
        }

        // GET: BedTransactions/Delete/5
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bedTransaction = await _context.BedTransaction.SingleOrDefaultAsync(m => m.BedTransactionID == id);
            if (bedTransaction == null)
            {
                return NotFound();
            }

            return View(bedTransaction);
        }

        // POST: BedTransactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bedTransaction = await _context.BedTransaction.SingleOrDefaultAsync(m => m.BedTransactionID == id);
            _context.BedTransaction.Remove(bedTransaction);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public ActionResult GetFarmBeds(short? farmId)
        {
            if (!farmId.HasValue)
            {
                return Json(new List<object>());
            }
            var data = _context.FarmBeds.Where(b => b.FarmSiteID == farmId).Select(o => new { Text = o.FarmBedTag, Value = o.FarmBedID });
            //var data = GetAllStatesForCountry(countryId.Value).Select(o => new { Text = o.StateName, Value = o.StateId });
            return Json(data);
        }
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public ActionResult GetFarmBedDetails(short? bedId)
        {
            if (!bedId.HasValue)
            {
                return Json(new List<object>());
            }
            var data = _context.FarmBeds.Where(b => b.FarmBedID == bedId).Select(o => new { length = o.Length, width = o.Width });
            return Json(data);

        }
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        // GetCurrentPlanting internal
        //
        public List<Planting> GetCurrentPlantingInternal(int bedId)
        {
            //var data =  _context.BedTransaction.Where(b => b.FarmBedID == bedId).
            //            Select(o => new { BedFeet = o.BedFeet, SeedLotID = o.SeedLotID }).ToArray();
//            return Json(data);
            //  Walk all the transactions in time order and build 
            var data2 = _context.BedTransaction.Where(b => b.FarmBedID == bedId).
                OrderBy(d => d.TransactionDate).ToList();
            List<Planting> plantings = new List<Planting>();
            foreach(BedTransaction bt in data2)
            {
                // ignore invalid transactions in the db - maybe log later ???
                if (bt.TransactionType == BedTransactionType.Sowed && (null == bt.SeedLotID)
                    || bt.TransactionType == BedTransactionType.Planted && (null == bt.PlugLotID))
                    continue;
                /////////////////////////////
                // get a taxon for the lot
                // first get the source seedlot
                int intSeedLot = 0;
                string strTaxon;
                string strEcoRegion;
                string strSiteName;
                int iSeedLotNo = 0;
                int iGeneration = -1;
                int speciesID = 0;
                // get the seedlotID as the info we want to return depends on it

                if (bt.TransactionType == BedTransactionType.Planted)
                {
                    var plg = _context.PlugLot.Where(p => p.PlugLotID == bt.PlugLotID).Single();
                    intSeedLot = plg.SeedLotID;
                }
                if (bt.TransactionType == BedTransactionType.Sowed)
                {
                    intSeedLot = (int)bt.SeedLotID;
                }
                strSiteName = "";
                strEcoRegion = "";
                if(0 == intSeedLot)
                    {
                        strTaxon = "";
                        iGeneration = -1;
                }
                else
                    {
                    // now get the seedlot record so we can get the species Id and thus the taxon
                    try {
                        var seedLot = _context.SeedLot.Where(l => l.SeedLotID == intSeedLot).Single();
                        speciesID = seedLot.SpeciesID;
                        iSeedLotNo = seedLot.SeedLotNumber;
                        iGeneration = seedLot.Generation;
                        speciesID = seedLot.SpeciesID;
                        switch (seedLot.SourceType)
                        {
                            case eSourceType.Farm:
                                strEcoRegion = "Farm";
                                var farmSite = _context.FarmSite.Where(c => c.FarmSiteID == seedLot.SourceSiteID).Single();
                                strSiteName = farmSite.FarmName;
                                var eReg = _context.Level4Region.Where(d => d.Level4RegionID == seedLot.RegionID).Single();
                                strEcoRegion = eReg.Name;
                                break;
                            case eSourceType.Wild:
                                var collectionSite = _context.CollectionSite.Where(a => a.CollectionSiteID == seedLot.SourceSiteID).Single();
                                var ecoRegion = _context.Level4Region.Where(b => b.Level4RegionID == collectionSite.Level4RegionID).Single();
                                strEcoRegion = ecoRegion.Name;
                                strSiteName = collectionSite.SiteName;
                                break;
                            case eSourceType.CNLMFarm:
                                if (null != seedLot.RegionID) {
                                    var level4EcoRegion = _context.Level4Region.Where(c => c.Level4RegionID == seedLot.RegionID).Single();
                                    //var farmSite = _context.FarmSite.Where(c => c.FarmSiteID == seedLot.SourceSiteID).Single();
                                    strEcoRegion = level4EcoRegion.Name;
                                }else
                                {
                                    strEcoRegion = "Region unidentified";
                                }
                                if (null != seedLot.SourceSiteID) { 
                                    farmSite = _context.FarmSite.Where(c => c.FarmSiteID == seedLot.SourceSiteID).Single();
                                    strSiteName = farmSite.FarmName;
                                }else
                                {
                                    strSiteName = "CNLM";
                                }
                                break;
                        }
                        var species = _context.Species.Where(s => s.SpeciesID == speciesID).Single();
                            // Must use hyphens to agree with taxon dropdown;
                        strTaxon = species.Genus + "_" + species.SpeciesName + "_" + species.SubTaxa;
                    } 
                    catch(Exception ex)
                    {
                        ViewBag.ErrorMessage = ex.Message;
                        return plantings;

                    }
                }
                //add to our returned list and update our totals for removes. 
                switch(bt.TransactionType)
                {
                    case BedTransactionType.Planted:
                        plantings.Add(new Planting(PlantingType.Plug, (int)iSeedLotNo, bt.BedFeet,strTaxon, 
                            strSiteName, strEcoRegion,iGeneration,speciesID));
                        break;
                    case BedTransactionType.Sowed:
                        plantings.Add(new Planting(PlantingType.Sow, (int)iSeedLotNo, bt.BedFeet,strTaxon, 
                            strSiteName, strEcoRegion,iGeneration,speciesID));
                        break;
                    case BedTransactionType.Cleared:
                        plantings.Clear();
                        break;
                    case BedTransactionType.Removed:
                        // walk current list and scale
                        foreach(Planting plt in plantings)
                        {
                            plt.bedfeet = plt.bedfeet * (float)(1.0 - (double)bt.SeedQuantity);
                        }
                        break;
                }
            }
            return plantings;
        }

        // GET: BedTransactions/GetCurrentPlanting
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public  IActionResult GetCurrentPlanting(short? bedId)
        {
            if (!bedId.HasValue)
            {
                return Json(new List<object>());
            }
            List<Planting> plantings = GetCurrentPlantingInternal((int)bedId);
            return Json(plantings);
        }


        // GET: BedTransactions/GetTransactions
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> GetTransactions(short? bedId)
        {
            if (!bedId.HasValue)
            {
                return Json(new List<object>());
            }

            var data2 = _context.BedTransaction.Where(b => b.FarmBedID == bedId).OrderByDescending(d => d.TransactionDate).ToList();
            // replace the seedlot or pluglot ID with the number for the user
            List<BedTransaction> trans = data2;

            for (int i=0;i< trans.Count;i++)
            {
                
                switch (trans[i].TransactionType)
                {
                    case BedTransactionType.Planted:
                        PlugLot pl = await _context.PlugLot.Where(a => a.PlugLotID == trans[i].PlugLotID).SingleOrDefaultAsync();
                        if(null != pl) {
                            trans[i].PlugLotID = pl.PlugLotNumber;
                            SeedLot slPL = await _context.SeedLot.Where(a => a.SeedLotID == pl.SeedLotID).SingleAsync();
                            trans[i].SeedLotID = slPL.SeedLotNumber; 
                        }
                        break;
                    case BedTransactionType.Sowed:
                        SeedLot sl = await _context.SeedLot.Where(a => a.SeedLotID == trans[i].SeedLotID).SingleOrDefaultAsync();
                        if (null != sl)
                            trans[i].SeedLotID = sl.SeedLotNumber;
                        break;
                    case BedTransactionType.Cleared:
                        break;
                    case BedTransactionType.Removed:
                        // put the percent removed in the bedfeet field for display
                        // show as integer as entered
                        trans[i].BedFeet = (int)(100.0 * trans[i].SeedQuantity);
                        break;

                }
            }
            //return Json(data2);
            return Json(trans);
        }

        // GET: BedTransactions/SeedlotNumberLookup
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public IActionResult SeedlotNumberLookup(int? seedlotNumber,bool isPlugLot)
        {
            if (!seedlotNumber.HasValue  || 0 == seedlotNumber)
            {
                JsonResult retval = new JsonResult(new List<object>());
                return retval;
            }
            // Get the seedlot
            SeedLot sl;
            int iPlugsAvailable = 0;
            float fSeedsAvailable = (float)0.0;
            string strLotTag;
            if (!isPlugLot)
            {
                try
                {
                    sl = _context.SeedLot.Where(s => s.SeedLotNumber == (int)(seedlotNumber)).Single();
                    fSeedsAvailable = (float)sl.CurrentQuantity;
                    strLotTag = sl.LotTag;
                }
                catch (Exception e)
                {
                    string strMessage = e.Message;
                    return Json(new List<object>());
                }
            }
            else
            {
                try
                {
                    // get the seedlot from the pluglot number so we can get its data
                    PlugLot pl = _context.PlugLot.Where(p => p.PlugLotNumber == (int)(seedlotNumber)).Single();
                    iPlugsAvailable = (int)pl.CurrentQuantity;
                    strLotTag = pl.PlugLotTag;
                    sl = _context.SeedLot.Where(s => s.SeedLotID == (int)(pl.SeedLotID)).Single();

                }
                catch (Exception e)
                {
                    string strMessage = e.Message;
                    return Json(new List<object>());
                }
            }
            // Get the species data
            Species sp;
            sp = _context.Species.Where(s => s.SpeciesID == sl.SpeciesID).Single();
            // Build results
            string strTaxon = sp.Genus + " " + sp.SpeciesName + " " + sp.SubTaxa;
            string strSite;
            int siteID = (int)sl.SourceSiteID;
            try { 
                if (sl.SourceType == eSourceType.Farm)
                {
                    FarmSite fs = _context.FarmSite.Where(f => f.FarmSiteID == siteID).Single();
                    strSite = fs.FarmName;
                }
                else
                {
                    CollectionSite cl = _context.CollectionSite.Where(c => c.CollectionSiteID == siteID).Single();
                    strSite = cl.SiteName;
                }
            }
            catch(Exception ex)
            {
                string strMsg = ex.Message;
                if (null != ex.InnerException)
                    strMsg += ex.InnerException.Message;
                strSite = "Not set";
            }
            // put in Json object to return
            //var data = _context.BedTransaction.Where(b => b.FarmBedID == bedId).
            //            Select(o => new { BedFeet = o.BedFeet, SeedLotID = o.SeedLotID }).ToArray();
            if(isPlugLot)
                return Json(new { Taxon = strTaxon, Site = strSite, Plugs = iPlugsAvailable , LotTag = strLotTag});
            else
                return Json(new { Taxon = strTaxon, Site = strSite, Seeds = fSeedsAvailable, LotTag = strLotTag });

        }

        private bool BedTransactionExists(int id)
        {
            return _context.BedTransaction.Any(e => e.BedTransactionID == id);
        }
        private async Task<Dictionary<int, string>> BuildPersonIDOrgNameDict()
        {
            List<SelectListItem> personlistdata = new List<SelectListItem>();
            List<Person> listPerson = await _context.Person.Include(o => o.Organization).ToListAsync();
            Dictionary<int, string> dictPToOrg = new Dictionary<int, string>();
            dictPToOrg.Add(0, "Unknown");
            foreach (Person p in listPerson)
            {
                if (null == p.Organization)
                {
                    dictPToOrg.Add(p.PersonID, "No Organization");
                }
                else
                {
                    dictPToOrg.Add(p.PersonID, p.Organization.OrganizationName);
                }
            }
            return dictPToOrg;
        }
        private async Task<List<SelectListItem>> BuildFarmList(bool bAddBlank = false, int iSelected = 0)
        {
            List<SelectListItem> farmlistdata = new List<SelectListItem>();
            List<FarmSite> listFarm = await _context.FarmSite.OrderBy(o => o.FarmName).ToListAsync();
            if (bAddBlank)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "0";
                farmlistdata.Add(sli);
            }
            foreach (FarmSite f in listFarm)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = f.FarmName;
                sli.Value = f.FarmSiteID.ToString();
                if (iSelected != 0 && iSelected == f.FarmSiteID)
                    sli.Selected = true;
                farmlistdata.Add(sli);
            }
            return farmlistdata;
        }
        // GET: BedTransactions/PlantingRecords
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> PlantingRecords()
        {
            // find just the farms that have beds
            var res = from f in _context.FarmSite
                      join b in _context.FarmBeds on f.FarmSiteID equals b.FarmSiteID
                      select f;
            // and only one each of those please
            ViewData["FarmList"] = new SelectList(res.Distinct(),
                "FarmSiteID", "FarmName");
            BedIDModel m = new BedIDModel();
            m.bedID = 2;
            return View(m);
        }

        public async Task<int> GetCNLMOrgID()
        {
            int orgID = await _context.Organization.Where(b => b.OrganizationName == Constants.CNLM_NAME).Select(a => a.OrganizationID).SingleAsync();
            return orgID;
        }

    }
        public class Planting
        {
            public PlantingType srcType { set; get; }
            public int srcID { set; get; }
            public float bedfeet { set; get; }
            public string taxon { set; get; }
            public string siteName { set; get; }
            public string ecoRegion { set; get; }
            public int generation { set; get; }
            public int speciesID { set; get; }
            public  Planting() {
                srcType = PlantingType.Plug;
            }
             public Planting(PlantingType pType, int pID,float bdFt,string strTaxon,string strSiteName, 
                string strEcoRegion, int iGeneration,int iSpecies)
            {
                srcID = pID;
                bedfeet = bdFt;
                srcType = pType;
                taxon = strTaxon;
                ecoRegion = strEcoRegion;
                siteName = strSiteName;
                generation = iGeneration;
                speciesID = iSpecies;
            }

        }
    public enum PlantingType { Sow = 0, Plug = 1 };
}
