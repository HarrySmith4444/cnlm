using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models;
using Microsoft.AspNetCore.Authorization;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class SpeciesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SpeciesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Species
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            ProgramState.State = ProgramState.RunningState.Normal;
            return View(await _context.Species.ToListAsync());
        }

        // GET: Species/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var species = await _context.Species.SingleOrDefaultAsync(m => m.SpeciesID == id);
            if (species == null)
            {
                return NotFound();
            }

            return View(species);
        }

        // GET: Species/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Species/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Create([Bind("SpeciesName,SpeciesID,Clade,Codon,FlowerPhoto,Genus,LifeCycle,PctCoverOneAdult,RestorationCharacteristics,RootStructure,SeedDormancyType,SeedPhoto,SeedlingPhoto,SubTaxa,Synonmyms,WholePlantPhoto")] Species species)
        {
            if (ModelState.IsValid)
            {
                ViewData["ErrorMessage"] = "";
                _context.Add(species);
                await _context.SaveChangesAsync();
                if(ProgramState.State == ProgramState.RunningState.CreatingSeedLot)
                {
                   return RedirectToAction("Create", "SeedLots");
                }
                return RedirectToAction("Index");
            }else  
            {
                var message = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                    ViewData["ErrorMessage"] = message;
            }
            return View(species);
        }

        // GET: Species/Edit/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var species = await _context.Species.SingleOrDefaultAsync(m => m.SpeciesID == id);
            if (species == null)
            {
                return NotFound();
            }
            return View(species);
        }

        // POST: Species/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int id, [Bind("SpeciesName,SpeciesID,Clade,Codon,FlowerPhoto,Genus,LifeCycle,PctCoverOneAdult,RestorationCharacteristics,RootStructure,SeedDormancyType,SeedPhoto,SeedlingPhoto,SubTaxa,Synonmyms,WholePlantPhoto")] Species species)
        {
            if (id != species.SpeciesID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(species);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SpeciesExists(species.SpeciesID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(species);
        }

        // GET: Species/Delete/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var species = await _context.Species.SingleOrDefaultAsync(m => m.SpeciesID == id);
            if (species == null)
            {
                return NotFound();
            }

            return View(species);
        }

        // POST: Species/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var species = await _context.Species.SingleOrDefaultAsync(m => m.SpeciesID == id);
            _context.Species.Remove(species);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool SpeciesExists(int id)
        {
            return _context.Species.Any(e => e.SpeciesID == id);
        }
    }
}
