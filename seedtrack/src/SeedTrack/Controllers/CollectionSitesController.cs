using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotModels;
using Microsoft.AspNetCore.Authorization;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class CollectionSitesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CollectionSitesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: CollectionSites
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.CollectionSite.Include(c => c.Level4Region);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: CollectionSites/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collectionSite = await _context.CollectionSite.SingleOrDefaultAsync(m => m.CollectionSiteID == id);
            if (collectionSite == null)
            {
                return NotFound();
            }

            return View(collectionSite);
        }

        // GET: CollectionSites/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult Create()
        {
            ViewData["Level4RegionID"] = new SelectList(_context.Level4Region, "Level4RegionID", "Name");
            return View();
        }

        // POST: CollectionSites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Create([Bind("CollectionSiteID,Level4RegionID,Notes,SiteCode,SiteName")] CollectionSite collectionSite)
        {
            if (ModelState.IsValid)
            {
                _context.Add(collectionSite);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["Level4RegionID"] = new SelectList(_context.Level4Region, "Level4RegionID", "Level4RegionID", collectionSite.Level4RegionID);
            return View(collectionSite);
        }

        // GET: CollectionSites/Edit/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collectionSite = await _context.CollectionSite.SingleOrDefaultAsync(m => m.CollectionSiteID == id);
            if (collectionSite == null)
            {
                return NotFound();
            }
            ViewData["Level4RegionID"] = new SelectList(_context.Level4Region, "Level4RegionID", "Name", collectionSite.Level4RegionID);
            return View(collectionSite);
        }

        // POST: CollectionSites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Administrator, PowerUser")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CollectionSiteID,Level4RegionID,Notes,SiteCode,SiteName")] CollectionSite collectionSite)
        {
            if (id != collectionSite.CollectionSiteID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(collectionSite);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CollectionSiteExists(collectionSite.CollectionSiteID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["Level4RegionID"] = new SelectList(_context.Level4Region, "Level4RegionID", "Level4RegionID", collectionSite.Level4RegionID);
            return View(collectionSite);
        }

        // GET: CollectionSites/Delete/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var collectionSite = await _context.CollectionSite.SingleOrDefaultAsync(m => m.CollectionSiteID == id);
            if (collectionSite == null)
            {
                return NotFound();
            }

            return View(collectionSite);
        }

        // POST: CollectionSites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var collectionSite = await _context.CollectionSite.SingleOrDefaultAsync(m => m.CollectionSiteID == id);
            _context.CollectionSite.Remove(collectionSite);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CollectionSiteExists(int id)
        {
            return _context.CollectionSite.Any(e => e.CollectionSiteID == id);
        }
    }
}
