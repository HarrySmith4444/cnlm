using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotModels;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Authorization;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class PersonsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly DbContextOptions<ApplicationDbContext> _dbcOptions;
        private bool bWasContactPerson;
       public PersonsController(ApplicationDbContext context,
                        DbContextOptions<ApplicationDbContext> dbcOptions)

        {
            _context = context;
            _dbcOptions = dbcOptions;
            bWasContactPerson = false;
        }

        // GET: Persons
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Person.Include(p => p.Organization);
            return View(await applicationDbContext.ToListAsync());

        }

        // GET: Persons/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var applicationDBContext = _context.Person.Include(p => p.Organization);
            var person = await applicationDBContext.SingleOrDefaultAsync(m => m.PersonID == id);
            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        // GET: Persons/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult Create(int? id)
        {
            List<SelectListItem> orgdropdownlistdata;
            if (null != id)
            {
                orgdropdownlistdata = MakeOrganizationDropDown((int)id);
            }
            else
            {
                orgdropdownlistdata = MakeOrganizationDropDown();
            }
            ViewData["OrgList"] = orgdropdownlistdata;
            ViewData["SelectedOrg"] = id;
            ViewData["InitialStateIndex"] = (Int32)States.WA;  // set to display Washington by default
            //find CNLM record to set as default
            ViewData["InitialOrgIndex"] = 0;
            string strTarget = "Center for Natural Lands Management";
            int indx = 0;
            foreach (SelectListItem itm in orgdropdownlistdata) { 
                if(itm.Text.Equals(strTarget,StringComparison.OrdinalIgnoreCase))
                    ViewData["InitialOrgIndex"] = indx;  // set to display CNLM by default
                indx++;
            }
            return View();
        }
        // GET: Persons/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult CreateWOrg(int id)

        {
            List<SelectListItem> orgdropdownlistdata = MakeOrganizationDropDown(id);
            ViewData["OrgList"] = orgdropdownlistdata;
            ViewBag.OrgList = orgdropdownlistdata;
            ViewBag.SelectedOrg = id;
            ViewData["SelectedOrg"] = id;
            RouteValueDictionary args = new RouteValueDictionary();
            args.Add("id", id);
            ViewData["InitialStateIndex"] = (Int32)States.WA;  // set to display Washington by default
            return RedirectToAction("Create",args);
        }
            
        // POST: Persons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Create(Person Item1)
        {
            if (ModelState.IsValid)
            {
                Int32 stateNo;
                stateNo = int.Parse(Item1.State);
                States eState = (States)stateNo;
                Item1.State = eState.ToString();
                Int32 countryNo = int.Parse(Item1.Country);
                Countries eCountry = (Countries)countryNo;
                Item1.Country = eCountry.ToString();

                _context.Add(Item1);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        // GET: Persons/Edit/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var applicationDbContext = _context.Person.Include(p => p.Organization);
            var person = await applicationDbContext.SingleAsync(m => m.PersonID == id);
            if (person == null)
            {
                return NotFound();
            }
            Organization organization = null;
            // retrieve Organization record
            if(null != person.OrganizationID)
            {
                organization = await _context.Organization.FindAsync(person.OrganizationID);
            }
            if (organization == null)
            {
                bWasContactPerson = false;
                person.IsContactPerson = false;
            }else {
                if (organization.ContactPersonID == person.PersonID)
                {
                    bWasContactPerson = true;
                    person.IsContactPerson = true;
                }
                else
                {
                    bWasContactPerson = false;
                    person.IsContactPerson = false;
                }
            }
            States eState = (States)Enum.Parse(typeof(States), person.State, true);
            ViewData["SelectedStateIndex"] = (Int32)eState;
            Countries eCountry = (Countries)Enum.Parse(typeof(Countries), person.Country, true);
            ViewData["SelectedCountryIndex"] = (Int32)eCountry;

            ViewData["OrganizationID"] = MakeOrganizationDropDown();

            return View(person);
        }

        // POST: Persons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int id, [Bind("PersonID,Title,CellPhone,FirstName,LastName,Email,"+
            "OrganizationID,Phone,Extension,FirstLine,SecondLine,City,State,Country,ZipCode,IsContactPerson")] Person person)
        {
            if (id != person.PersonID)
            {
                return NotFound();
            }

            States eState = (States)Enum.Parse(typeof(States), person.State, true);
            ViewData["SelectedStateIndex"] = (Int32)eState;
            Countries eCountry = (Countries)Enum.Parse(typeof(Countries), person.Country, true);
            ViewData["SelectedCountryIndex"] = (Int32)eCountry;
            ViewData["OrganizationID"] = MakeOrganizationDropDown();
            // data comes back as selected index, need to convert to string for storage
            person.State = eState.ToString();
            person.Country = eCountry.ToString();

            if (ModelState.IsValid)
            {
                _context.Update(person);
                await _context.SaveChangesAsync();

                bool bIsContactPerson = person.IsContactPerson;
                if ((bIsContactPerson && !bWasContactPerson) || (!bIsContactPerson && bWasContactPerson))
                {
                    // Need to update organization data
                    var organization = await _context.Organization.FindAsync(person.OrganizationID);
                    if (organization != null)
                    {
                        organization.ContactPersonID = person.PersonID;
                        await _context.SaveChangesAsync();
                    }
                    return RedirectToAction("Index");
                }
            }
            return View(person);

        }

        // GET: Persons/Delete/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var applicationDBContext = _context.Person.Include(p => p.Organization);
            var person = await applicationDBContext.SingleOrDefaultAsync(m => m.PersonID == id);

            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        // POST: Persons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var person = await _context.Person.SingleOrDefaultAsync(m => m.PersonID == id);
            _context.Person.Remove(person);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PersonExists(int id)
        {
            return _context.Person.Any(e => e.PersonID == id);
        }
        private List<SelectListItem> MakeOrganizationDropDown(int orgID=-1)
        {
            List<SelectListItem> orgdropdownlistdata = new List<SelectListItem>();
            var context = new ApplicationDbContext(_dbcOptions);

            using (ApplicationDbContext localcontext = new ApplicationDbContext(_dbcOptions))
            {
                var query_where2 = from a in localcontext.Organization
                                   select a;

                foreach (var a in query_where2)
                {
                    SelectListItem sli = new SelectListItem();
                    sli.Text = a.OrganizationName;
                    sli.Value = a.OrganizationID.ToString();
                    if (orgID != -1 && orgID == a.OrganizationID)
                        sli.Selected = true;
                    orgdropdownlistdata.Add(sli);

                }
            }
            return orgdropdownlistdata;
        }

    }
}
