using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.PlugLotModels;

namespace SeedTrack.Controllers
{
    public class PlugContainersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlugContainersController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: PlugContainers
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlugContainer.ToListAsync());
        }

        // GET: PlugContainers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugContainer = await _context.PlugContainer.SingleOrDefaultAsync(m => m.PlugContainerID == id);
            if (plugContainer == null)
            {
                return NotFound();
            }

            return View(plugContainer);
        }

        // GET: PlugContainers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlugContainers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlugContainerID,PlugContainerName,PlugVolume,PlugsPerTray")] PlugContainer plugContainer)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plugContainer);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(plugContainer);
        }

        // GET: PlugContainers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugContainer = await _context.PlugContainer.SingleOrDefaultAsync(m => m.PlugContainerID == id);
            if (plugContainer == null)
            {
                return NotFound();
            }
            return View(plugContainer);
        }

        // POST: PlugContainers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlugContainerID,PlugContainerName,PlugVolume,PlugsPerTray")] PlugContainer plugContainer)
        {
            if (id != plugContainer.PlugContainerID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plugContainer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlugContainerExists(plugContainer.PlugContainerID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(plugContainer);
        }

        // GET: PlugContainers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugContainer = await _context.PlugContainer.SingleOrDefaultAsync(m => m.PlugContainerID == id);
            if (plugContainer == null)
            {
                return NotFound();
            }

            return View(plugContainer);
        }

        // POST: PlugContainers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plugContainer = await _context.PlugContainer.SingleOrDefaultAsync(m => m.PlugContainerID == id);
            _context.PlugContainer.Remove(plugContainer);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlugContainerExists(int id)
        {
            return _context.PlugContainer.Any(e => e.PlugContainerID == id);
        }
    }
}
