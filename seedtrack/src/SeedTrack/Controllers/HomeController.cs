﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SeedTrack.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Version"] = SeedTrack.Constants.Version;
            ViewData["Message"] = "CNLM Seed Tracker";
            ViewData["IsAdmin"] = User.IsInRole("Administrator"); 
            return View();
            //            return RedirectToAction("Index", "Species");
        }

        public IActionResult About()
        {
            ViewData["Version"] = SeedTrack.Constants.Version;
            ViewData["Message"] = "CNLM Seed Tracker";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "CNLM Seed Tracker";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
        //GET: /Index/AllTables
        [HttpGet]
        public IActionResult AllTables()
        {
            return View();
        }
        //GET: /Index/ComingSoon
        [HttpGet]
        public IActionResult ComingSoon()
        {
            return View();
        }

    }
}
