using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotModels;
using System.Collections;
using Microsoft.Extensions.Primitives;
using Microsoft.AspNetCore.Authorization;
using System.Linq.Expressions;

namespace SeedTrack.Controllers
{

    [Authorize]
    public class OrganizationsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly DbContextOptions<ApplicationDbContext> _dbcOptions;
        public OrganizationsController(ApplicationDbContext context,
            DbContextOptions<ApplicationDbContext> dbcOptions)

        {
            _context = context;
            _dbcOptions = dbcOptions;
        }

        // GET: Organizations
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Organization.Include(o => o.ContactPerson);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Organizations/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var applicationDbContext = _context.Organization.Include(p => p.ContactPerson).Include(p => p.Persons);

            var organization = await applicationDbContext.SingleOrDefaultAsync(m => m.OrganizationID == id);

            //            var organization = await _context.Organization.SingleOrDefaultAsync(m => m.OrganizationID == id);
            if (organization == null)
            {
                return NotFound();
            }

            return View(organization);
        }

        // GET: Organizations/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult Create()
        {
            //List<SelectListItem> stateList = new List<SelectListItem>();
            //Organization org = new Organization();
            //Address adr = new Address();
            //Person per = new Person();
            //Tuple<Organization, Address, Person, List<SelectListItem>> tuple = new Tuple<Organization, Address, Person, List<SelectListItem>>(org,adr,per,stateList);
            //return View(tuple);
            ViewData["InitialStateIndex"] = (Int32)States.WA;  // set to display Washington by default
            ViewData["OrgCreated"] = 0;
            return View();
        }

        // POST: Organizations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("OrganizationID,AddressID,ContactPersonID,OrganizationName,PhoneNumber")] Organization organization)
        //public async Task<IActionResult> Create(string OrganizationName,string PhoneNumber,string FirstLine,string SecondLine,
        //    string City,string State,string ZipCode, string Country)
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Create(Organization Item1, Person Item2)
        {

            if (ModelState.IsValid)
            {
                // check if org already exists
                Organization foundorg = await _context.Organization.FirstOrDefaultAsync(x => x.OrganizationName == Item1.OrganizationName);
                if(null != foundorg)
                {
                    ViewData["OrgCreated"] = 1;
                    ViewData["OrganizationID"] = foundorg.OrganizationID;
                    return View(new Tuple<Organization, Person>(Item1, Item2));
                }
                ViewData["OrgCreated"] = 0;
                Int32 stateNo;
                stateNo = int.Parse(Item1.State);
                States eState = (States)stateNo;
                Item1.State = eState.ToString();
                Int32 countryNo = int.Parse(Item1.Country);
                Countries eCountry = (Countries)countryNo;
                Item1.Country = eCountry.ToString();
                //organization.OrganizationName = Item1.OrganizationName;
                //organization.PhoneNumber = Item1.PhoneNumber;
                //organization.Extension = Item1.Extension;
                //organization.AddressID = address.AddressID;
                _context.Add(Item1);
                await _context.SaveChangesAsync();
                ViewData["OrgCreated"] = 1;
                ViewData["OrganizationID"] = Item1.OrganizationID;
                return View(new Tuple<Organization, Person>(Item1, Item2));

//                return RedirectToAction("Index");

            }
            return View(new Tuple<Organization, Person>(Item1,  Item2));
        }

        // GET: Organizations/Edit/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            //           var organization = await _context.Organization.SingleOrDefaultAsync(o => o.OrganizationID == id);

            var applicationDbContext = _context.Organization.Include(p => p.ContactPerson).Include(p => p.Persons);
            var organization = await applicationDbContext.SingleOrDefaultAsync(m => m.OrganizationID == id);
            //var organization = await _context.Organization.SingleOrDefaultAsync(m => m.OrganizationID == id);
            if (organization == null)
            {
                return NotFound();
            }
            if (null != organization.State)
            {
                States eState = (States)Enum.Parse(typeof(States), organization.State, true);
                ViewData["SelectedStateIndex"] = (Int32)eState;
            }else
            {
                // default to Washington for those not filled in
                ViewData["SelectedStateIndex"] = (Int32)States.WA;
            }
            if (null != organization.Country)
            {
                Countries eCountry = (Countries)Enum.Parse(typeof(Countries), organization.Country, true);
                ViewData["SelectedCountryIndex"] = (Int32)eCountry;
            }else
            {
                ViewData["SelectedCountryIndex"] = (Int32)Countries.USA;
            }
            List<SelectListItem> membersList = new List<SelectListItem>();

            foreach( Person p in organization.Persons)
            {
                SelectListItem selItem = new SelectListItem();
                selItem.Text = p.LastName + ", " + p.FirstName;
                selItem.Value = p.PersonID.ToString();
                if((null != organization.ContactPerson) && p.PersonID == organization.ContactPerson.PersonID)
                {
                    selItem.Selected = true;
                }else
                {
                    selItem.Selected = false;
                }
                membersList.Add(selItem);

            }
            ViewBag.Members = membersList;
 
            return View(organization);
        }

        // POST: Organizations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("OrganizationID,ContactPerson,OrganizationName,PhoneNumber,OranizationAddress.FirstLine,"+
        //    "OrganizationAddress.SecondLine,OrganizationAddress.City,OrganizationAddress.State,OrganizationAddress.ZipCode,"+
        //    "OrganizationAddress.Country,ContactPerson")] Organization organization)
        //        public async Task<IActionResult> Edit(int id,  Organization organization)
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(Microsoft.AspNetCore.Http.IFormCollection values)
        {
            int id = Convert.ToInt32(values["OrganizationID"]);
            if (0 == id)
            {
                return NotFound();
            }
            Organization org = await _context.Organization.FindAsync(id);
            if (null != org)
            {
                org.OrganizationName = Convert.ToString(values["OrganizationName"]);
                org.PhoneNumber = Convert.ToString(values["PhoneNumber"]);
                org.Extension = Convert.ToString(values["Extension"]);
                org.Department = Convert.ToString(values["Department"]);
                StringValues strvals = values["ContactPersonID"];
                String strContactPersonID = strvals.ElementAt(1);
                org.ContactPersonID = Convert.ToInt32(strContactPersonID);
                org.FirstLine = Convert.ToString(values["FirstLine"]);
                org.SecondLine = Convert.ToString(values["SecondLine"]);
                org.City = Convert.ToString(values["City"]);
                // data for state and country come back as selected index, need to convert to string for storage
                int iState = Convert.ToInt32(values["State"]);
                org.State = ((States)(iState)).ToString();
                //org.State = Convert.ToString(values["State"]);
                int iCountry = Convert.ToInt32(values["Country"]);
                org.Country = ((Countries)(iCountry)).ToString();
                //org.Country = Convert.ToString(values["Country"]);
                org.ZipCode = Convert.ToString(values["ZipCode"]);

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(org);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!OrganizationExists(org.OrganizationID))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction("Index");
                }
            }


            //ViewData["ContactPersonID"] = new SelectList(_context.Set<Person>(), "PersonID", "PersonID", organization.ContactPersonID);
            //ViewData["AddressID"] = new SelectList(_context.Set<Address>(), "AddressID", "AddressID", organization.AddressID);
            return View(org);
        }

        // GET: Organizations/Delete/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var organization = await _context.Organization.SingleOrDefaultAsync(m => m.OrganizationID == id);
            if (organization == null)
            {
                return NotFound();
            }

            return View(organization);
        }

        // POST: Organizations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var organization = await _context.Organization.SingleOrDefaultAsync(m => m.OrganizationID == id);
            // find all the people with links to this org and null out the link
            IQueryable<Person> iqp = _context.Person.AsQueryable();
            List<Person> pList = await iqp.Where(p => p.OrganizationID == id).ToListAsync();
            foreach (Person pn in pList)
            {
                _context.Person.Update(pn);
                pn.OrganizationID = null;
            }
            await _context.SaveChangesAsync();
            _context.Organization.Remove(organization);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool OrganizationExists(int id)
        {
            return _context.Organization.Any(e => e.OrganizationID == id);
        }
        // GET: Organizations/AddPerson/
        public async Task<IActionResult> AddPerson(int id)
        {
            List<Person> personList = await _context.Person.OrderBy(x=> x.LastName).ToListAsync();
            // build a select list here to pass to the view
            // list must use combined lastname, firstname so user can pick the right "smith"
            List<SelectListItem> wnList = new List<SelectListItem>();
            foreach (Person p in personList)
            {
                string strWholeName = p.LastName + ", " + p.FirstName;
                SelectListItem selItem = new SelectListItem();
                selItem.Text = strWholeName;
                selItem.Value = p.PersonID.ToString();
                wnList.Add(selItem);
            }
            Organization org = _context.Organization
                .Where(o => o.OrganizationID == id).FirstOrDefault();
            OrganizationAddPersonViewModel model = new OrganizationAddPersonViewModel(org, wnList);
            return View(model);
        }
        // POST: /Organizations/AddPerson
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> AddPerson(OrganizationAddPersonViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            foreach (int i in model.selectedIDs)
            {
                Person p = await _context.Person.FirstOrDefaultAsync(m => m.PersonID == i);
                p.OrganizationID = model.orgID;
                await _context.SaveChangesAsync();
            }
            //object o = model.orgID;
            //return RedirectToAction("Details", "Organizations", o);
            return RedirectToAction("Details", "Organizations", new { id = model.orgID });
        }
        // POST: /Organizations/RemovePerson
        //[HttpPost]
        //        [ValidateAntiForgeryToken]
        //        public async Task<IActionResult> RemovePerson(string strPersonID)
        //public async Task<IActionResult> RemovePerson(int personID)
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> RemovePerson(int? id)
        {
            if (null == id)
                return View(); 
            int personID = (int)id;
            int orgID=0;
            //  ViewData("PersonID);
            Person person = await _context.Person.FirstOrDefaultAsync(m => m.PersonID == personID);
            if (null != person)
            {
                orgID = (int)person.OrganizationID;
                person.OrganizationID = null;
                await _context.SaveChangesAsync();
            }
            return RedirectToAction("Edit", "Organizations",orgID);
        }
    [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public ActionResult GetOrgsPeople(short? orgID)
        {
            if (!orgID.HasValue)
            {
                return Json(new List<object>());
            }
            var data = _context.Person.Where(b => b.OrganizationID == orgID).OrderBy(a => a.LastName).Select(o => new { Text = o.LastName + ", " + o.FirstName, Value = o.PersonID });
            data.Prepend(new { Text = "  ", Value = -1 });
            data.Append(new { Text = " ", Value = -1 });
            return Json(data);
        }

    }

    public class WholeNamePerson
    {
        public string name;
        public int id;
        public WholeNamePerson(string Name,int ID)
        {
            name = Name;
            id = ID;
        }
    }
}


