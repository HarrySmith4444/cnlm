using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.PlugLotModels;
using SeedTrack.Models.SeedLotModels;
using Microsoft.AspNetCore.Authorization;
using SeedTrack.Models.SeedLotTransactionModels;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class PlugLotsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlugLotsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: PlugLots
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlugLot.ToListAsync());
        }

        // GET: PlugLots/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLot = await _context.PlugLot.Include(a => a.Organization).Include(b => b.PlugContainer).
                Include(c => c.PlugSoil).Include(d => d.SeedLot).Include(e=> e.PlugLotTransactions).SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLot == null)
            {
                return NotFound();
            }
            return View(plugLot);
        }

        // GET: PlugLots/Create


        public async Task<IActionResult> Create(int iAction = 0)
        {

            // Initially we want to show CNLM so look for it to pass the id to the list builder
            int orgID = 0;
            string strPreSelectedOrg = "Center for Natural Lands Management";

            try
            {
                var org = _context.Organization.Where(a => a.OrganizationName == strPreSelectedOrg).Single();
                orgID = org.OrganizationID;
            }
            catch (Exception ex)
            {
                string strMessage = ex.Message;
                // ignore for now - missing cnlm in org list

            }
            // turns out building a preselected list and passing it doesn't work.  It is dependent
            // on OrganizationID in the model (asp-for)
            List<SelectListItem> Orgslist = await BuildOrgslist(_context.Organization, false, orgID);
            ViewBag.Orgs = Orgslist;
            //List<SelectListItem> SeedLotlist = await BuildLotTagSelector(_context.SeedLot);
            //ViewBag.SeedLotTags = SeedLotlist;
            List<SelectListItem> TrayList = await BuildTraySelector(_context.PlugContainer, false, 0);
            ViewBag.TrayTypes = TrayList;
            List<SelectListItem> SoilMixList = await BuildSoilMixSelector(_context.PlugSoil, true, 0);
            ViewBag.SoilMixes = SoilMixList;
            ViewData["ErrorMessage"] = "";
            ViewData["SeedCover"] = new SelectList(Enum.GetNames(typeof(eSoilSurfaceCover)));

            // build a list of pluglots in cold strat
#if false
            // This seems inefficient ????  Should we add a Pluglot state?
            var plglttrns = await _context.PlugLotTransaction.Where(a => a.TransactionType == PlugLotTransaction.PlugLotTransactionType.ColdStrat)
                .Include(p => p.PlugLot).ToListAsync();
            List<SelectListItem> ColdStratLots = new List<SelectListItem>();
            SelectListItem itmBlank = new SelectListItem();
            itmBlank.Value = "-1";
            itmBlank.Text = " ";
            ColdStratLots.Add(itmBlank);
            foreach (PlugLotTransaction t in plglttrns)
            {
                try
                {
                    var trns = await _context.PlugLotTransaction.Where(b => b.PlugLotID == t.PlugLotID &&
                        b.TransactionDate > t.TransactionDate &&
                        t.TransactionType == PlugLotTransaction.PlugLotTransactionType.Sowed)
                        .AnyAsync();
                    if (!trns)
                    {
                        // We didn't find a sow so must still be in cold strat - add it
                        SelectListItem itm = new SelectListItem();
                        itm.Text = t.PlugLot.PlugLotTag.ToString();
                        itm.Value = t.PlugLot.PlugLotID.ToString();
                        ColdStratLots.Add(itm);

                    }
                }
                catch (Exception ex)
                {
                    ViewData["ErrorMessage"] = ex.Message;
                }
            }
            ViewData["ColdStratLots"] = ColdStratLots;
#endif
            List<SelectListItem> ColdStratLots = new List<SelectListItem>();
            SelectListItem itmBlank = new SelectListItem();
            itmBlank.Value = "-1";
            itmBlank.Text = " ";
            ColdStratLots.Add(itmBlank);
            List<PlugLot> lstPlgLots = await _context.PlugLot.Where(a => a.State == ePluglotState.ColdStrat).ToListAsync();
            if (null != lstPlgLots)
            {
                foreach (PlugLot p in lstPlgLots)
                {
                    SelectListItem itm = new SelectListItem();
                    itm.Text = p.PlugLotTag.ToString();
                    itm.Value = p.PlugLotID.ToString();
                    ColdStratLots.Add(itm);
                }
            }
            ViewData["ColdStratLots"] = ColdStratLots;

            PlugLot pl = new PlugLot();
            pl.SeedCover = eSoilSurfaceCover.NoCover;
            pl.SeedPretreatment = "none";
            //pl.SeedQuantityUsed = (float).2;
            pl.SeedsPerPlug = Constants.DEFAULT_SEEDS_PER_PLUG;
            pl.OrganizationID = orgID;
            pl.SeedCover = eSoilSurfaceCover.Grit;
            pl.ImbibeTime = 0;
            pl.ColdStratTemp = 0;
            pl.State = ePluglotState.ColdStrat; // will be rest by transaction type 
            PlugLotTransactionViewModel pltv = new PlugLotTransactionViewModel();
            pltv.pl = pl;
            pltv.csplID = 0; // no cold strat pluglot yet
            pltv.plt = new PlugLotTransaction();
            pltv.plt.TransactionType = PlugLotTransaction.PlugLotTransactionType.ColdStrat;
            pltv.plt.TransactionDate = DateTime.Now;
            pltv.plt.TransactionType = (PlugLotTransaction.PlugLotTransactionType)iAction;

            return View(pltv);
        }



        // POST: PlugLots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PlugLotTransactionViewModel pltv)
        //public async Task<IActionResult> Create([Bind("PlugLotID,LotTag,OrganizationID,PlugContainerID,PlugLotNumber,PlugSoilID,SeedLotID,SeedPretreatment,SeedQuantityUsed,SeedsPerPlug")] PlugLot plugLot)
        {
            if (ModelState.IsValid)
            {
                // the pluglot
                pltv.pl.CreatedBy = User.Identity.Name;
                pltv.pl.CreationDate = DateTime.Now;
                pltv.pl.ModifiedBy = User.Identity.Name;
                pltv.pl.ModifiedDate = DateTime.Now;
                // and the transaction
                pltv.plt.CreatedBy = User.Identity.Name;
                pltv.plt.CreationDate = DateTime.Now;
                pltv.plt.ModifiedBy = User.Identity.Name;
                pltv.plt.ModifiedDate = DateTime.Now;

                // swap the passed seedLotID which is a seedLot Number, for the pluglotID
                if (pltv.pl.SeedLotID == 0)
                {
                    ViewData["ErrorMessage"] = "Invalid seedlot number";
                    return View(pltv);
                }
                var seedlot = _context.SeedLot.Where(a => a.SeedLotNumber == pltv.pl.SeedLotID).Single();
                if (null == seedlot)
                {
                    ViewData["ErrorMessage"] = "Seedlot not found";
                    return View(pltv);
                }
                pltv.pl.SeedLotID = seedlot.SeedLotID;
                // if we didn't get a pluglot number, set it to the pluglotID?
                if (pltv.pl.PlugLotNumber <= 0)
                {
                    pltv.pl.PlugLotNumber = GetNextPlugLotNumber();
                    // ???
                    if (0 == pltv.pl.PlugLotNumber) pltv.pl.PlugLotNumber = pltv.pl.PlugLotID;
                }
                pltv.pl.PlugLotTag = await BuildPlugLotTagfromSeedLot(pltv.pl.SeedLotID, pltv.pl.PlugLotNumber);
                // seed pretreatment of "none" means null
                if ("none" == pltv.pl.SeedPretreatment)
                    pltv.pl.SeedPretreatment = null;

                // calculate actual number of plugs to store if we are sowing - given trays
                if (pltv.plt.TransactionType == PlugLotTransaction.PlugLotTransactionType.Sowed)
                {
                    var plugContainer = await _context.PlugContainer.Where(a => a.PlugContainerID == pltv.pl.PlugContainerID).SingleAsync();
                    if (null != plugContainer)
                    {
                        pltv.plt.PlugQuantity = plugContainer.PlugsPerTray * pltv.plt.PlugQuantity;
                        pltv.pl.CurrentQuantity = pltv.plt.PlugQuantity;
                    }
                }
                // handle sign of quantity.  Always input by user as positive 
                // set state as appropriate
                switch (pltv.plt.TransactionType)
                {
                    case PlugLotTransaction.PlugLotTransactionType.ColdStrat:
                        {
                            pltv.pl.State = ePluglotState.ColdStrat;
                            // We used the seed quantity to collect the info for the
                            // pluglot but the pluglot transaction for coldstrat only has 
                            // plugquantity which is an int.
                            // not sure if we need it but for now lets put milligrams in the 
                            // transaction and deal with it on display
                            float fMilliGrams = (float)(-1000.0 * pltv.pl.SeedQuantityUsed);
                            int quantRounded = (int)Math.Round(fMilliGrams, 0, MidpointRounding.AwayFromZero);
                            int iSeedUsed = quantRounded;
                            pltv.plt.PlugQuantity = iSeedUsed;
                        }
                        break;
                    case PlugLotTransaction.PlugLotTransactionType.Allocated:
                    case PlugLotTransaction.PlugLotTransactionType.Dumped:
                        {
                            pltv.plt.PlugQuantity = -1 * pltv.plt.PlugQuantity;
                            pltv.pl.State = ePluglotState.Sowed;  // basically not in coldstrat
                        }
                        break;
                    case PlugLotTransaction.PlugLotTransactionType.Returned:
                    case PlugLotTransaction.PlugLotTransactionType.Sowed:
                    case PlugLotTransaction.PlugLotTransactionType.Received:
                        pltv.pl.State = ePluglotState.Sowed;  // basically not in coldstrat
                        break;
                    default:
                        pltv.pl.State = ePluglotState.Sowed;  
                        break;
                }


                // pluglot is ready for creation
                try
                {
                    if(pltv.csplID > 0)
                    {
                        pltv.pl.PlugLotID = (int)pltv.csplID;
                        _context.Update(pltv.pl);
                    }else {
                        _context.Add(pltv.pl);
                    }
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    ViewData["ErrorMessage"] = ex.Message;
                    if (null != ex.InnerException)
                        ViewData["ErrorMessage"] = ex.InnerException.Message;
                    return View(pltv);
                }
                // we have a vaild pluglot - now do the transaction
                // put the ID in the transaction
                pltv.plt.PlugLotID = pltv.pl.PlugLotID;
                pltv.plt.OrganizationID = pltv.pl.OrganizationID;
                try
                {
                    _context.PlugLotTransaction.Add(pltv.plt);
                    await _context.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    ViewData["ErrorMessage"] = ex.Message;
                    if (null != ex.InnerException)
                        ViewData["ErrorMessage"] = ex.InnerException.Message;
                    return View(pltv);
                }
                // if this wasn't from coldstrat we need to dock the seedlot with a 
                // seedlot transaction and update it's current and initial value
                if (0 == pltv.csplID)
                {
                    try
                    {
                        SeedLotTransaction slt = new SeedLotTransaction();
                        slt.CreationDate = pltv.plt.CreationDate;
                        slt.CreatedBy = pltv.plt.CreatedBy;
                        slt.Quantity = (float)(-1.0)*(float)pltv.pl.SeedQuantityUsed;  // entered as positive float
                        slt.SeedLotID = pltv.pl.SeedLotID;
                        slt.TransactionDate = pltv.plt.TransactionDate;
                        slt.TransactionType = SeedLotTransactionType.Disbursement;
                        slt.OrganizationID = pltv.plt.OrganizationID;
                        slt.PersonID = pltv.plt.PersonID;
                        slt.Notes = "Pluglot " + pltv.pl.PlugLotTag + " planted.";
                        _context.Add(slt);
                        await _context.SaveChangesAsync();
                        // Update the seedlots numbers as well as the transaction above
                        SeedLotTransactionsController.UpdateSeedLotQuantity(_context,seedlot,(float)(-1.0 *
                            (float)pltv.pl.SeedQuantityUsed), SeedLotTransactionType.Disbursement);
                    }
                    catch (Exception ex)
                    {
                        ViewData["ErrorMessage"] = ex.Message;
                        if (null != ex.InnerException)
                            ViewData["ErrorMessage"] = ex.InnerException.Message;
                        return View(pltv);
                    }
                }


                return RedirectToAction("Details", new { id = pltv.pl.PlugLotID });
            }
            return View(pltv);
        }
        private int GetNextPlugLotNumber()
        {
            int lastCounter = 0;
            lastCounter = _context.PlugLot.Max(a => a.PlugLotNumber);
            // FromSql("select MAX('PlugLotNumber') from PlugLot");
            return ++lastCounter;
        }
    public async Task<int> RecalcAll(bool bDoSave = true, string strUntilDate = "")
    {
        string strMessage = "";
        List<int> lstPlugLots = await _context.PlugLot.Select(a => a.PlugLotID).ToListAsync();
        foreach (int id in lstPlugLots)
        {
            try
            {
                await CalcCurrentQuantity(id, bDoSave, strUntilDate);
            }
            catch (Exception ex)
            {
                strMessage = ex.Message;
                return 0;
            }
        }
        return 1;
    }
    public async Task<int> CalcCurrentQuantity(int plugLotID, Boolean bDoSave = true, string strUntilDate = "")
        {
            List<PlugLotTransaction> lstTrans = await _context.PlugLotTransaction.Where(i => i.PlugLotID == plugLotID).OrderBy(j => j.TransactionDate).ToListAsync();
            int quant = 0;
            int initQuant = 0;
            if (strUntilDate.Length < 1)
                strUntilDate = DateTime.Now.ToString();
            DateTime dtUntil = Convert.ToDateTime(strUntilDate);
            foreach (PlugLotTransaction plt in lstTrans)
            {
                if (plt.TransactionDate <= dtUntil)
                    quant += (int)plt.PlugQuantity;
                if (plt.TransactionType == PlugLotTransaction.PlugLotTransactionType.Received
                    || plt.TransactionType == PlugLotTransaction.PlugLotTransactionType.Sowed)
                    initQuant += (int)plt.PlugQuantity;
            }
            if (bDoSave)
            {
                var plugLot = await _context.PlugLot.FirstOrDefaultAsync(s => s.PlugLotID == plugLotID);
                // update the db
                plugLot.CurrentQuantity = quant;
                _context.PlugLot.Update(plugLot);
                await _context.SaveChangesAsync();
            }
            return quant;
        }

        private async Task<string> BuildPlugLotTagfromSeedLot(int iSeedLotID, int iPlugLotNumber)
        {
            // make a temporary seedlotcontroller to do this.  Questionable???
            SeedLotsController slc = new SeedLotsController(_context);
            // get the seedlot with all the required pieces
            string strTag = "";
            strTag = await slc.BuildSeedLotTag(iSeedLotID, true, iPlugLotNumber);
            return strTag;
        }

        // GET: PlugLots/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLot = await _context.PlugLot.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLot == null)
            {
                return NotFound();
            }
            return View(plugLot);
        }

        // POST: PlugLots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Edit(int id, [Bind("PlugLotID,LotTag,OrganizationID,PlugContainerID,PlugLotNumber,PlugSoilID,SeedLotID,SeedPretreatment,SeedQuantityUsed,SeedsPerPlug")] PlugLot plugLot)
        {
            if (id != plugLot.PlugLotID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plugLot);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlugLotExists(plugLot.PlugLotID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(plugLot);
        }

        // GET: PlugLots/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLot = await _context.PlugLot.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLot == null)
            {
                return NotFound();
            }

            return View(plugLot);
        }

        // POST: PlugLots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plugLot = await _context.PlugLot.SingleOrDefaultAsync(m => m.PlugLotID == id);
            _context.PlugLot.Remove(plugLot);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlugLotExists(int id)
        {
            return _context.PlugLot.Any(e => e.PlugLotID == id);
        }

        public static async Task<int> UpdateCurrentQuantity(ApplicationDbContext cntxt, int plugLotID, int currentDelta)
        {
            try
            {
                PlugLot pl = await cntxt.PlugLot.Where(a => a.PlugLotID == plugLotID).SingleAsync();
                pl.CurrentQuantity += currentDelta;
                cntxt.Update(pl);
                await cntxt.SaveChangesAsync();
            }
            catch (Exception) { return -1; }
            return 0;
        }

        public static async Task<List<SelectListItem>> BuildOrgslist(DbSet<Organization> Organization, bool bAddBlank = true, int organizationid = -2)
        {
            List<SelectListItem> Orgsdropdownlistdata = new List<SelectListItem>();
            List<Organization> Orgslist = await Organization.OrderBy(s => s.OrganizationName).ToListAsync();
            if (bAddBlank)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "-1";
                sli.Selected = true;
                Orgsdropdownlistdata.Add(sli);
            }
            foreach (Organization org in Orgslist)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = org.OrganizationName.ToString();
                sli.Value = org.OrganizationID.ToString();
                if (organizationid != -2 && organizationid == org.OrganizationID)
                    sli.Selected = true;
                Orgsdropdownlistdata.Add(sli);
            }
            return Orgsdropdownlistdata;
        }

        // Not used I believe ???
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public ActionResult GetseedLotDetails(short? seedLot)
        {
            if (!seedLot.HasValue)
            {
                return Json(new List<object>());
            }
            var data = _context.SeedLot.Where(b => b.SeedLotNumber == seedLot).Select(p => new { LotTag = p.LotTag, SeedLotID = p.SeedLotID });
            return Json(data);


        }


        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public ActionResult GetPlugLotDetails(short? plugLotID)
        {
            if (!plugLotID.HasValue)
            {
                return Json(new List<object>());
            }
            var data = _context.PlugLot.Where(b => b.PlugLotID == plugLotID)
                .Include(b => b.SeedLot)
                .Select(p => new
                {
                    PlugLotTag = p.PlugLotTag,
                    SeedLotNumber = p.SeedLot.SeedLotNumber,
                    SeedLotTag = p.SeedLot.LotTag,
                    Quantity = p.SeedQuantityUsed,
                    Pretreatment = p.SeedPretreatment,
                    ImbibeTime = p.ImbibeTime,
                    ColdStratTemp = p.ColdStratTemp,
                    Notes = p.Notes
                });
            return Json(data);


        }
        // GET: BedTransactions/SeedlotNumberLookup
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public IActionResult SeedlotNumberLookup(int? seedlotNumber)
        {
            if (!seedlotNumber.HasValue)
            {
                return Json(new { lotTag = "" });
                //return Json(new List<object>());
            }
            // Get the seedlot
            SeedLot sl;
            try
            {
                sl = _context.SeedLot.Where(s => s.SeedLotNumber == (int)(seedlotNumber)).Single();
            }
            catch (Exception e)
            {
                string strMessage = e.Message;
                return Json(new { lotTag = "" });
            }

            // Build results

            return Json(new { lotTag = sl.LotTag, CurrentQuantity = sl.CurrentQuantity.ToString() });


        }
        public IActionResult Search1()
        {
            PlugLotSearch pls = new PlugLotSearch();
            pls.iPlugLotNumber = 0;
            return View(pls);
        }
        // POST: /PlugLots/Search1
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Search1(PlugLotSearch pls)
        {
            if (pls.iPlugLotNumber <= 0) {
                ViewData["ErrorMessage"] = "Plug Lot number " + pls.iPlugLotNumber + " not found";
                return View(pls);
            }
            PlugLot pl = await _context.PlugLot.Where(a => a.PlugLotNumber == pls.iPlugLotNumber).SingleAsync();
            if(null == pl)
            {
                ViewData["ErrorMessage"] = "Plug Lot number " + pls.iPlugLotNumber + " not found";
                return View(pls);
            }
            return RedirectToAction("Details", new { id = pl.PlugLotID });

        }

        private async Task<List<SelectListItem>> BuildLotTagSelector(DbSet<SeedLot> SeedLotTag, bool bAddBlank = true, int lotid = -2)
        {
            List<SelectListItem> SeedLotdropdownlistdata = new List<SelectListItem>();
            List<SeedLot> SeedLotlist = await SeedLotTag.OrderBy(s => s.SeedLotID).ToListAsync();
            if (bAddBlank)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "-1";
                SeedLotdropdownlistdata.Add(sli);
            }
            foreach (SeedLot lot in SeedLotlist)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = lot.SeedLotNumber.ToString();
                sli.Value = lot.SeedLotID.ToString();
                if (lotid != -2 && lotid == lot.SeedLotID)
                    sli.Selected = true;
                SeedLotdropdownlistdata.Add(sli);
            }
            return SeedLotdropdownlistdata;
        }
        private async Task<List<SelectListItem>> BuildTraySelector(DbSet<PlugContainer> Container, bool bAddBlank = true, int Containerid = -2)
        {
            List<SelectListItem> Traylistdata = new List<SelectListItem>();
            List<PlugContainer> Traylist = await Container.OrderBy(s => s.PlugContainerName).ToListAsync();
            if (bAddBlank)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "-1";
                Traylistdata.Add(sli);
            }
            foreach (PlugContainer tray in Traylist)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = tray.PlugContainerName.ToString();
                sli.Value = tray.PlugContainerID.ToString();
                if (Containerid != -2 && Containerid == tray.PlugContainerID)
                    sli.Selected = true;
                Traylistdata.Add(sli);
            }
            return Traylistdata;
        }
        private async Task<List<SelectListItem>> BuildSoilMixSelector(DbSet<PlugSoil> Soils, bool bAddBlank = true, int SoilMixID = -2)
        {
            List<SelectListItem> SoilMixlistdata = new List<SelectListItem>();
            List<PlugSoil> Soillist = await Soils.OrderBy(s => s.PlugSoilName).ToListAsync();
            if (bAddBlank)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "-1";
                SoilMixlistdata.Add(sli);
            }
            foreach (PlugSoil soilmix in Soillist)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = soilmix.PlugSoilName.ToString();
                sli.Value = soilmix.PlugSoilID.ToString();
                if (SoilMixID != -2 && SoilMixID == soilmix.PlugSoilID)
                    sli.Selected = true;
                SoilMixlistdata.Add(sli);
            }
            return SoilMixlistdata;
        }
    }
}
