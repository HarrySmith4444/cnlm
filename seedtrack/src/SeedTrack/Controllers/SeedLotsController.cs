using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack;
using SeedTrack.Models.SeedLotModels;
using SeedTrack.Models.SeedLotTransactionModels;
using SeedTrack.Models;
using System.Data.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using SeedTrack.Models.FarmModels;
using Newtonsoft.Json;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class SeedLotsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SeedLotsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SeedLots
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            ProgramState.State = ProgramState.RunningState.Normal;
            var applicationDbContext = _context.SeedLot.Include(s => s.Species);
            List<SeedLot> seedlots = await applicationDbContext.ToListAsync();
            // walk the list and build a table of sourceID and soure names
            Dictionary<int, string> dictSiteCodes = new Dictionary<int, string>();
            foreach (SeedLot sl in seedlots)
            {
                if (null != sl.SourceSiteID)
                {
                    string strSrcName;
                    CollectionSite colSite;
                    FarmSite frmSite;
                    if (sl.SourceType == eSourceType.Wild)
                    {
                        colSite = await _context.CollectionSite.FirstOrDefaultAsync(m => m.CollectionSiteID == sl.SourceSiteID);
                        strSrcName = colSite.SiteCode;
                    }
                    else
                    {
                        frmSite = await _context.FarmSite.FirstOrDefaultAsync(m => m.FarmSiteID == sl.SourceSiteID);
                        strSrcName = frmSite.FarmCode;
                    }
                    dictSiteCodes.Add((int)sl.SeedLotID, strSrcName);
                }
            }
            ViewData["SiteCodes"] = dictSiteCodes;
            return View(seedlots);
            //return View(await applicationDbContext.ToListAsync());
        }

        // GET: SeedLots/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seedLot = await _context.SeedLot.Include(t => t.SeedLotTransactions).Include(b => b.Region).SingleOrDefaultAsync(m => m.SeedLotID == id);
            if (seedLot == null)
            {
                return NotFound();
            }
            CollectionSite collSite = null;
            FarmSite farmSite = null;

            SeedLot sl = seedLot;
            switch(sl.SourceType)
            {
                case eSourceType.Wild:
                     collSite = await _context.CollectionSite.SingleOrDefaultAsync(c => c.CollectionSiteID == (int)(sl.SourceSiteID));
                    break;
                case eSourceType.Farm:
                case eSourceType.CNLMFarm:
                    farmSite = await _context.FarmSite.SingleOrDefaultAsync(f => f.FarmSiteID == (int)(sl.SourceSiteID));
                    break;
            }
            SeedLotViewModel slView = new SeedLotViewModel(sl,(eSourceType)(sl.SourceType),collSite,farmSite);
            //slView.fltTotal = await GetQuantity(sl.SeedLotID);
            slView.dictOrg = await BuildOrgDictionary();
            if(sl.SourceType == eSourceType.CNLMFarm) {
                FarmBed fb = await _context.FarmBeds.Where(b => b.FarmBedID == sl.BedID).SingleAsync();
               slView.FarmBedTag = fb.FarmBedTag;
            } else
            {
                slView.FarmBedTag = "";
             }
            return View(slView);
        }

        // GET: SeedLots/Create
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Create()
        {
            // build species select list with ID and Taxon
            List<SelectListItem> speciesSelData = await BuildSpeciesSelect(_context.Species);
            ViewData["SpeciesID"] = speciesSelData;
            List<SelectListItem> collSiteSelData = await BuildCollSiteList(-1);
            ViewData["CollectionSites"] = collSiteSelData;
            List<SelectListItem> farmlSiteSelData = await BuildFarmlSiteList();
            ViewData["FarmSites"] = farmlSiteSelData;
            List<SelectListItem> gradeListData = BuildGradeList();
            ViewData["Grades"] = gradeListData;
            ViewData["StateList"] = new SelectList(Enum.GetNames(typeof(eSeedLotState)));
            List<SelectListItem> regionsSelData = await BuildRegionsSelect(_context.Level4Region);
            ViewData["Regions"] = regionsSelData;
            // find just the farms that have beds
            var res = from f in _context.FarmSite
                      join b in _context.FarmBeds on f.FarmSiteID equals b.FarmSiteID
                      select f;
            // and only one each of those please
            ViewData["CNLMFarms"] = new SelectList(res.Distinct(),
                "FarmSiteID", "FarmName");
            ViewData["ErrorMessage"] = "";

            List < SelectListItem > Orgslist = await PlugLotsController.BuildOrgslist(_context.Organization, true);
            ViewData["Organizations"] = Orgslist;
            List<SelectListItem> selPeople = await SeedLotTransactionsController.BuildPersonSelect(_context, true);
            ViewData["People"] = selPeople;

            ProgramState.State = ProgramState.RunningState.CreatingSeedLot;
            SeedLotCreateViewModel slvw = new SeedLotCreateViewModel();
            slvw.sl = new SeedLot();
            slvw.sl.Generation = Constants.MAX_GENERATION;
            slvw.sl.SourceType = eSourceType.Wild;
            slvw.sl.Status = eSeedLotState.InStock;
            slvw.sl.FirstHarvestDate = null;
            slvw.sl.CurrentQuantity = (float)0.0;
            slvw.sl.InitialQuantity = (float)0.0;
            slvw.sl.RegionID = -1;
            slvw.sl.SourceSiteID = -1;
            slvw.percent1 = 0;
            slvw.percent2 = 0;
            slvw.percent3 = 0;
            slvw.percent4 = 0;
            slvw.percent5 = 0;
            slvw.percent6 = 0;
            slvw.siteID1 = 0;
            slvw.siteID2 = 0;
            slvw.siteID3 = 0;
            slvw.siteID4 = 0;
            slvw.siteID5 = 0;
            slvw.siteID6 = 0;
            //slvw.sl.FinalHarvestDate = DateTime.Now;
            slvw.slt = new SeedLotTransaction();
            return View(slvw);
        }

        // POST: SeedLots/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        //public async Task<IActionResult> Create([Bind("FinalHarvestDate,FirstHarvestDate,Generation,Grade,Notes,RegionID,SourceSiteID,SourceType,SpeciesID,Status")] SeedLot seedLot)
       public async Task<IActionResult> Create( SeedLotCreateViewModel svm)
        {
            // add created by and modified by
            DateTime dt = DateTime.Now;
            svm.sl.CreatedBy = User.Identity.Name;
            svm.sl.CreationDate = dt;
            svm.sl.ModifiedBy = User.Identity.Name;
            svm.sl.ModifiedDate = dt;

            // Handle regionID based on source type
            switch (svm.sl.SourceType)
            {
                case eSourceType.CNLMFarm:
                    // Region shoud be set on client side by code
                    if (-1 == svm.sl.RegionID)
                    {
                        ViewData["ErrorMessage"] = "No region provided.";
                        goto errorexit;
                    }
                    break;
                case eSourceType.Farm:
                    // region is that of first collection site
                    if (svm.siteID1 > 0)
                    {
                        try
                        {
                            CollectionSite reg = await _context.CollectionSite.Where(a => a.CollectionSiteID == svm.siteID1).
                                Include(b => b.Level4Region).SingleAsync();
                            svm.sl.RegionID = reg.Level4RegionID;
                        }
                        catch (Exception ex)
                        {
                            ViewData["ErrorMessage"] = "Unable to find collection site. " + svm.siteID1.ToString() + ex.Message;
                            goto errorexit;
                        }
                    }
                    else
                    {
                        ViewData["ErrorMessage"] = "No collection site provided";
                        goto errorexit;
                    }
                    break;
                case eSourceType.Wild:
                    try
                    {
                        CollectionSite reg = await _context.CollectionSite.Where(a => a.CollectionSiteID == svm.sl.SourceSiteID).
                            Include(b => b.Level4Region).SingleAsync();
                        svm.sl.RegionID = reg.Level4RegionID;
                    }
                    catch (Exception ex)
                    {
                        ViewData["ErrorMessage"] = "Unable to find collection site. " + svm.sl.SourceSiteID.ToString() + ex.Message;
                        goto errorexit;
                    }
                    break;
            }

            // seedlot number is next highest
            svm.sl.SeedLotNumber = await GetNextSeedLotNumber();
            svm.sl.LotTag = await BuildSeedLotTag(svm.sl);
            // quantity fields in the seedlot - transaction is later
            svm.sl.CurrentQuantity = svm.sl.InitialQuantity = svm.slt.Quantity;


            if (!ModelState.IsValid)
            {
                goto errorexit;
            }
            // Validate harvest dates
            // if they're both empty then its an error
            // if only one, fill the other with the same
            // if final less than first, swap
            if (null == svm.sl.FinalHarvestDate && null == svm.sl.FirstHarvestDate)
            {
                ViewData["ErrorMessage"] = "No dates provided. ";
                goto errorexit;
            }
            if (null == svm.sl.FinalHarvestDate)
            {
                svm.sl.FinalHarvestDate = svm.sl.FirstHarvestDate;
            }
            else
            {
                svm.sl.FirstHarvestDate = svm.sl.FinalHarvestDate;
            }
            if (DateTime.Compare((DateTime)svm.sl.FirstHarvestDate, (DateTime)svm.sl.FinalHarvestDate) > 0)
            {
                DateTime dtTemp = (DateTime)svm.sl.FirstHarvestDate;
                svm.sl.FirstHarvestDate = svm.sl.FinalHarvestDate;
                svm.sl.FinalHarvestDate = dtTemp;
            }

            // Add the seedlot record to the database
            try
            {
                _context.Add(svm.sl);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = "Failed adding the seedlot to the DB. " + ex.Message;
                goto errorexit;
            }
            // seedlot saved
            // set up and save transaction - quantity set on input screen
            svm.slt.SeedLotID = svm.sl.SeedLotID;
            svm.slt.CreatedBy = svm.sl.CreatedBy;
            svm.slt.CreationDate = svm.sl.CreationDate;
            svm.slt.ModifiedBy = svm.sl.ModifiedBy;
            svm.slt.ModifiedDate = (DateTime)svm.sl.ModifiedDate;
            // person is always optional but must be null or key on insert
            if (-1 == svm.slt.PersonID || 0 == svm.slt.PersonID)
                svm.slt.PersonID = null;

            try
            {
                switch (svm.sl.SourceType)
                {
                    case eSourceType.CNLMFarm:
                        svm.slt.TransactionType = SeedLotTransactionType.Harvest;
                        // set the org to CNLM
                        Organization org = await _context.Organization.Where(a => a.OrganizationName == Constants.CNLM_NAME).SingleAsync();
                        svm.slt.OrganizationID = org.OrganizationID;
                        break;
                    case eSourceType.Farm:
                        svm.slt.TransactionType = SeedLotTransactionType.ReceiveFromPartner;
                        break;
                    case eSourceType.Wild:
                        svm.slt.TransactionType = SeedLotTransactionType.WildCollection;
                        break;
                }
                svm.slt.TransactionDate = (DateTime)svm.sl.FinalHarvestDate;

                _context.SeedLotTransaction.Add(svm.slt);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string strMsg = "Failed adding seedlot transaction to the DB. " + ex.Message;
                if (null != ex.InnerException) strMsg += " : " + ex.InnerException.Message;
                ViewData["ErrorMessage"] = strMsg;
                goto errorexit;
            }
            //
            // Finally we must build the provenance records.
            try
            {
                int check = await SaveProvenance(svm);
            }
            catch (Exception ex) {
                string strMsg = "Failed adding seedlot provenance to the DB. " + ex.Message;
                if (null != ex.InnerException) strMsg += " : " + ex.InnerException.Message;
                ViewData["ErrorMessage"] = strMsg;
                goto errorexit;
            }

            ProgramState.State = ProgramState.RunningState.Normal;
            return RedirectToAction("Details", new { id = svm.sl.SeedLotID });


            errorexit:
            {
                if (svm.sl.SourceType == eSourceType.Farm)
                {
                    List<SelectListItem> farmlSiteSelData = await BuildFarmlSiteList((int)svm.sl.SourceSiteID);
                    ViewData["FarmSites"] = farmlSiteSelData;
                }
                //ViewData["SpeciesID"] = new SelectList(_context.Species, "SpeciesID", "SpeciesID", svm.sl.SpeciesID);
                return View(svm);
            }
        }
        public async Task<int> SaveProvenance(SeedLotCreateViewModel svm)
        {
            SeedLot sl = svm.sl;
            int retVal = 0;
            switch (sl.SourceType)
            {
                case eSourceType.Farm:
                    // we will need to collect this information in the create screen
                    // eventually.  
                    int []iSites = new int[6];
                    int []iPercents = new int[6];
                    int i = 0;
                    iSites[i] = svm.siteID1; iPercents[i] = svm.percent1;  i++;
                    iSites[i] = svm.siteID2; iPercents[i] = svm.percent2; i++;
                    iSites[i] = svm.siteID3; iPercents[i] = svm.percent3; i++;
                    iSites[i] = svm.siteID4; iPercents[i] = svm.percent4; i++;
                    iSites[i] = svm.siteID5; iPercents[i] = svm.percent5; i++;
                    iSites[i] = svm.siteID6; iPercents[i] = svm.percent6; i++;

                    int nextSource = iSites[0];
                    i = 0;
                    while(nextSource > 0)
                    {
                        SLProvenance prov = new SLProvenance();
                        prov.collectionSiteID = iSites[i];
                        prov.percentContribution = iPercents[i];
                        prov.generation = 0;
                        prov.seedLotID = svm.sl.SeedLotID;
                        prov.sourceID = svm.sl.SeedLotID;  // not sure why
                        prov.speciesID = svm.sl.SpeciesID;
                        i++;
                        nextSource = iSites[i];
                        await _context.SLProvenance.AddAsync(prov);
                        await _context.SaveChangesAsync();
                    }


                    break;
                case eSourceType.Wild:
                    // this is the original source.  
                    SLProvenance slp = new SLProvenance();
                    slp.seedLotID = sl.SeedLotID;
                    slp.collectionSiteID = (int)sl.SourceSiteID;
                    slp.generation = 0;
                    slp.sourceID = sl.SeedLotID; // end of the line, point to self  - not used
                    slp.speciesID = sl.SpeciesID;
                    slp.percentContribution = (float)100.0; // 100%
                     _context.SLProvenance.Add(slp);
                    await _context.SaveChangesAsync();
                    break;
                case eSourceType.CNLMFarm:
                    //  The moment of truth
                    // get a list of seedlots currently planted in the bed and their percentages of the bed (and speciesID)
                    // Build a  collection of provenance records from each seedlot's provenance with their percentage scaled
                    // down by the percent of the bed the seedlot occupies.
                    // Combine records where the speciesID are the same adding the percentages

                    // get the bed's provenance
                    SLBedProvenance slbp;
                    slbp = await GetBedPlanting((int)sl.BedID);
                    List<SLProvenanceItem> lstLots;  // this list contains the seedlots and their %s 
                                                     //in the bed
                    lstLots = slbp.lstProv;
                    MergeLotsBySpecies(ref lstLots);
                    // now walk the planting and copy each existing provenance record - planted seedlot - 
                    // scaling its pecent contribution to the seed by the percent of the bed it occupies.
                    // insert with our new seedlot ID as our provenance.
                    
                    foreach(SLProvenanceItem itm in lstLots)  // our returned list of plantings
                    {
                        List<SLProvenance> lstProv = await _context.SLProvenance.Where(
                            a => a.seedLotID == itm.seedLotID).ToListAsync();
                        foreach (SLProvenance slp2 in lstProv) {   // the provenance records of the item
                            if (slp2.percentContribution == 0) break;
                            SLProvenance slprov = new SLProvenance();
                            slprov.SLProvenanceID = 0;
                            slprov.seedLotID = svm.sl.SeedLotID;  // our new seedlot ID
                            slprov.generation = slp2.generation;
                            slprov.collectionSiteID = slp2.collectionSiteID;
                            slprov.speciesID = slp2.speciesID;
                            slprov.sourceID = slp2.sourceID;
                            slprov.percentContribution = itm.percentContribution * slp2.percentContribution; // this is  percent of the planted bed with this seedlot
                             _context.SLProvenance.Add(slprov);
                        }
                    }
                    await _context.SaveChangesAsync();
                    break;
            }
            return retVal;
        }

        public async Task<SLBedProvenance> GetBedPlanting(int bedID)
        {
            // Build a list of Seedlots in the bed at the current time and their % contribution by bed feet
            // That is not the seedlots provenance.

            // need total bed length.
            FarmBed fb = await _context.FarmBeds.Where(a => a.FarmBedID == bedID).SingleAsync();
            float fBedLength = fb.Length;
            int intSeedLot = 0;
            SeedLot sl=null;
            List<SLProvenanceItem> lstSLP = new List<SLProvenanceItem>();
            var data2 = await _context.BedTransaction.Where(b => b.FarmBedID == bedID).
                OrderBy(d => d.TransactionDate).ToListAsync();
            foreach (BedTransaction bt in data2)
            {
                // ignore invalid transactions in the db - maybe log later ???
                if (bt.TransactionType == BedTransactionType.Sowed && (null == bt.SeedLotID)
                    || bt.TransactionType == BedTransactionType.Planted && (null == bt.PlugLotID))
                    continue;
                /////////////////////////////
                // we have to handle the different types of transactions to determine what is in the bed
                // how much of what 
                switch (bt.TransactionType)
                {
                    case BedTransactionType.Cleared:
                        {
                            lstSLP.Clear();
                            continue;
                        }
                    case BedTransactionType.Planted:
                        {
                            var plg = _context.PlugLot.Where(p => p.PlugLotID == bt.PlugLotID).Single();
                            intSeedLot = plg.SeedLotID;
                            sl = await _context.SeedLot.Where(a => a.SeedLotID == intSeedLot).SingleAsync();
                            break;
                        }
                    case BedTransactionType.Sowed:
                        {
                            intSeedLot = (int)bt.SeedLotID;
                            sl = await _context.SeedLot.Where(a => a.SeedLotID == intSeedLot).SingleAsync();
                            break;
                        }
                    case BedTransactionType.Removed:
                        {
                            //scale down what we have so far
                            foreach(SLProvenanceItem slpItm in lstSLP)
                            {
                                slpItm.percentContribution = slpItm.percentContribution * (float)bt.SeedQuantity;
                            }
                        }
                        continue;
                }

                SLProvenanceItem slp = new SLProvenanceItem();
                slp.seedLotID = intSeedLot;
                if (null != sl)
                    slp.speciesID = sl.SpeciesID;
                // we are just holding the planted feet temporarily and will scale below when we know
                // the total bed feet planted
                slp.percentContribution = ((float)bt.BedFeet);
                lstSLP.Add(slp);
            }
            // 
            // sum up the revised list to get total feet planted and then divide the stored bed feet by
            // the total planted and update the percentContribution. Return the total planted also
            float fPlantedBedFeet=(float)0.0;
            foreach(SLProvenanceItem slpi in lstSLP) {
                fPlantedBedFeet += slpi.percentContribution;
            }
            foreach (SLProvenanceItem slpi in lstSLP)
            {
                slpi.percentContribution = slpi.percentContribution / fPlantedBedFeet;
            }
            // wrapper class to hold the planted bed length
            SLBedProvenance slbp = new SLBedProvenance();
            slbp.lstProv = lstSLP;
            slbp.bedFeetPlanted = fPlantedBedFeet;
            return slbp;  
        }

        public void MergeLotsBySpecies(ref List<SLProvenanceItem> lstLots)
        {
            foreach(SLProvenanceItem itm in lstLots)
            {
                // combine when species and seedlot match
                int firstItm = lstLots.FindIndex(0,a=> a.speciesID == itm.speciesID && a.seedLotID == itm.seedLotID);
                int secondItm = lstLots.FindLastIndex(0, a => a.speciesID == itm.speciesID && a.seedLotID == itm.seedLotID);
                if (firstItm == secondItm || secondItm == -1) // only one
                    continue;
                while(secondItm != firstItm)  //merge the two into a new one and remove these and try again
                {
                    SLProvenanceItem slpiNew = new SLProvenanceItem();
                    slpiNew.seedLotID = itm.seedLotID;
                    slpiNew.speciesID = itm.speciesID;
                    SLProvenanceItem slpiSecond = lstLots.ElementAt(secondItm);
                    // adding these as they are increasing the percentage of that seedlot
                    slpiNew.percentContribution = itm.percentContribution + slpiSecond.percentContribution;
                    lstLots.Add(slpiNew);
                    lstLots.RemoveAt(secondItm);
                    lstLots.RemoveAt(firstItm);
                    // set up for next loop
                    firstItm = lstLots.FindIndex(0, a => a.speciesID == itm.speciesID && a.seedLotID == itm.seedLotID);
                    secondItm = lstLots.FindLastIndex(0, a => a.speciesID == itm.speciesID && a.seedLotID == itm.seedLotID);

                }

            }
        }

        // GET: SeedLots/GetProvenance
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> GetProvenance(int? slID)
        {
            if (!slID.HasValue)
            {
                return Json(new List<object>());
            }
            List<SLProvenance> lstProvs = await _context.SLProvenance.Where(a => a.seedLotID == slID)
                .Include(b=> b.CollectionSite).ToListAsync();
            return Json(lstProvs);
        }

        // GET: SeedLots/Edit/5
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seedLot = await _context.SeedLot.SingleOrDefaultAsync(m => m.SeedLotID == id);
            if (seedLot == null)
            {
                return NotFound();
            }
            //sites
            List<SelectListItem> collSiteSelData;
            List<SelectListItem> farmlSiteSelData;
            if (eSourceType.Wild == seedLot.SourceType)
                collSiteSelData = await BuildCollSiteList((int)seedLot.SourceSiteID,false);
            else
                collSiteSelData = await BuildCollSiteList(-1);
            if (eSourceType.Farm == seedLot.SourceType)
                farmlSiteSelData = await BuildFarmlSiteList((int)seedLot.SourceSiteID);
            else
                farmlSiteSelData = await BuildFarmlSiteList(-1);
            ViewData["CollectionSites"] = collSiteSelData;
            ViewData["FarmSites"] = farmlSiteSelData;

            List<SelectListItem> gradeListData = BuildGradeList(seedLot.Grade);
            ViewData["Grades"] = gradeListData;
            List<SelectListItem> speciesSelData = await BuildSpeciesSelect(_context.Species);
            ViewData["SpeciesID"] = speciesSelData;
            List<SelectListItem> stateSelData = BuildStateSelect(seedLot.Status);
            //SelectList stateList = new SelectList(Enum.GetNames(typeof(eSeedLotState)));
            ViewData["StateList"] = stateSelData;
            return View(seedLot);
        }

        // POST: SeedLots/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Edit(int id, [Bind("SeedLotID,SeedLotNumber,FinalHarvestDate,"+
            "FirstHarvestDate,Generation,Grade,LotTag,Notes,SourceSiteID,"+
            "SourceType,SpeciesID,Status,timestamp,CreatedBy,CreationDate,ModifiedBy,ModifiedDate")] SeedLot seedLot)
        {
            if (id != seedLot.SeedLotID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    seedLot.ModifiedBy = User.Identity.Name;
                    seedLot.ModifiedDate = DateTime.Now;
                    int siteID = ExtractProperSiteID(this.HttpContext.Request.Form, (eSourceType)seedLot.SourceType);
                    if (-1 != siteID)
                        seedLot.SourceSiteID = siteID;
                    _context.Update(seedLot);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SeedLotExists(seedLot.SeedLotID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = seedLot.SeedLotID });
            }
            ViewData["SpeciesID"] = new SelectList(_context.Species, "SpeciesID", "SpeciesID", seedLot.SpeciesID);
            return View(seedLot);
        }

        //// GET: SeedLots/Allocate/
        //[Authorize(Roles = "Administrator, PowerUser, Employee")]
        //public async Task<IActionResult> Allocate(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var seedLot = await _context.SeedLot.SingleOrDefaultAsync(m => m.SeedLotID == id);
        //    if (seedLot == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(seedLot);
        //}

        // GET: SeedLots/Delete/5
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seedLot = await _context.SeedLot.SingleOrDefaultAsync(m => m.SeedLotID == id);
            if (seedLot == null)
            {
                return NotFound();
            }

            return View(seedLot);
        }

        // POST: SeedLots/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var seedLot = await _context.SeedLot.SingleOrDefaultAsync(m => m.SeedLotID == id);
            _context.SeedLot.Remove(seedLot);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        // GET: Seedlots/Search1
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Search1()
        {
            // dropdowns
            bool bAddBlank = true;
            List<SelectListItem> speciesSelData = await BuildSpeciesSelect(_context.Species, bAddBlank);
            ViewData["SpeciesID"] = speciesSelData;
            ViewData["SourceType"] = BuildSourceTypeSelect();
            ViewData["SortBy"] = BuildSortBySelect();

            // seedlot list
            //var applicationDbContext = _context.SeedLot.Include(s => s.Species).Where(w => (w.SeedLotID > 1000));
            ///List<SeedLot> seedlots = await applicationDbContext.ToListAsync();
            // view model
            SeedLotSearch1ViewModel viewModel = new SeedLotSearch1ViewModel();
            //viewModel.iEnumResults = seedlots;
            viewModel.iEnumResults = null;
            viewModel.iSelectedSpeciesID = 1;
            //viewModel.iShowError = 0;
            return View(viewModel);
        }
        // POST: /SeedLots/Search1
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Search1(SeedLotSearch1ViewModel model)
        {
            bool bAddBlank = true;
            bool bNeedAnd = false;
            //model.iShowError = 0;
            List<SelectListItem> speciesSelData = await BuildSpeciesSelect(_context.Species, bAddBlank);
            ViewData["SpeciesID"] = speciesSelData;
            ViewData["SourceType"] = BuildSourceTypeSelect();
            ViewData["SortBy"] = BuildSortBySelect();

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            List<SeedLot> seedlots;
            // Check for invalid query - empty
            if ((0 == model.searchType && Convert.ToInt32(model.strSeedLotNumber) < 1)
                ||
               (1 == model.searchType && (-1 == model.iSelectedSpeciesID && -1 == model.iSourceType)))
            {
                model.strErrorMsg = "Please enter criteria.";
                model.iShowError = 1;
                seedlots = null;
            }
            else
            {
                var applicationDbContext = _context.SeedLot.Include(s => s.Species);
                // Build search string
                string strSelect = "select * from seedlot where ";
                if (model.searchType == 0)
                {
                    strSelect += "SeedLotNumber = " + model.strSeedLotNumber + "";
                    bNeedAnd = true;
                }
                else
                {
                    if (model.iSelectedSpeciesID > -1)
                    {
                        strSelect += "SpeciesID = " + model.iSelectedSpeciesID.ToString();
                        bNeedAnd = true;
                    }
                    if (model.iSourceType > -1)
                    {
                        if (bNeedAnd) strSelect += " and ";
                        strSelect += "SourceType = " + model.iSourceType.ToString();
                    }
                }
                if (null != model.strSortBy && "" != model.strSortBy)
                {
                    switch (model.strSortBy)
                    {
                        case "SeedLotNumber":
                            seedlots = await applicationDbContext.FromSql(strSelect).OrderBy(o => o.SeedLotNumber).ToListAsync();
                            break;
                        case "LotTag":
                            seedlots = await applicationDbContext.FromSql(strSelect).OrderBy(o => o.LotTag).ToListAsync();
                            break;
                        case "FirstHarvestDate":
                            seedlots = await applicationDbContext.FromSql(strSelect).OrderBy(o => o.FirstHarvestDate).ToListAsync();
                            break;
                        case "Status":
                            seedlots = await applicationDbContext.FromSql(strSelect).OrderBy(o => o.Status).ToListAsync();
                            break;
                        default:
                            seedlots = null;
                            break;
                    }
                }
                else
                {
                    seedlots = await applicationDbContext.FromSql(strSelect).ToListAsync();
                }
            }
            model.iEnumResults = seedlots;
            return View(model);
        }
        [HttpGet]
        //public async Task<IActionResult> Search2()
        //{
        //    SeedLot sl = new SeedLot
        //    {
        //        LotTag = "test"
        //    };
        //    return View("Search2",sl);
        //    //return Json(sl);
        //}

        // Get: /Seedlots/RecalcCurrentQuantities
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> RecalcCurrentQuantities()
        {
            // Walk all the seedlots and recalc and set their current quantity from transactions
            List<SeedLot> lstSeedLot = await _context.SeedLot.ToListAsync();
            SeedLotTransactionsController sltc = new SeedLotTransactionsController(_context);

            foreach(SeedLot sl in lstSeedLot)
            {
                float fltTemp = await sltc.RecalcCurrentQuantity(sl.SeedLotID);
            }
            return View();
        }

        //
        // Helper functions
        //
        private bool SeedLotExists(int id)
        {
            return _context.SeedLot.Any(e => e.SeedLotID == id);
        }
#if false
        public async Task<float> RecalcCurrentQuantity(int seedlotID, string strUntilDate="")
        {
            var seedLot = await _context.SeedLot.FirstOrDefaultAsync(s => s.SeedLotID == seedlotID);
            List<SeedLotTransaction> lstTrans =  await _context.SeedLotTransaction.Where(i => i.SeedLotID == seedlotID).OrderBy(j => j.TransactionDate).ToListAsync();
            float quant = (float)0.0;
            if (strUntilDate.Length < 1)
                strUntilDate = DateTime.Now.ToString();
            DateTime dtUntil = Convert.ToDateTime(strUntilDate);
            foreach (SeedLotTransaction slt in lstTrans)
            {
                if (slt.TransactionDate <= dtUntil)
                    quant += slt.Quantity;
            }
            // Round off as appropriate
            decimal dQuant;
            dQuant = Math.Round((Decimal)quant,Constants.SEEDLOTQUANTITYDIGITS, MidpointRounding.AwayFromZero);
            // update the db
            seedLot.CurrentQuantity = (float)dQuant;  // casting away details here but only in the total column.
            // set the status now that the quantity may have changed:
            if (dQuant > 0)
            {
                seedLot.Status = eSeedLotState.InStock;
            }else
            {
                seedLot.Status = eSeedLotState.OutOfStock;
            }
            await _context.SaveChangesAsync();
            return quant;
        }
#endif
        private List<SelectListItem> BuildStateSelect(eSeedLotState eState)
        {
            List<SelectListItem> statelistdata = new List<SelectListItem>();
            foreach (eSeedLotState e in Enum.GetValues(typeof(eSeedLotState)))
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = e.ToString();
                sli.Value = ((int)e).ToString();
                if (eState == e)
                    sli.Selected = true;
                statelistdata.Add(sli);
            }
            return statelistdata;
        }
        private async Task<List<SelectListItem>> BuildRegionsSelect(DbSet<Level4Region> regions,bool bAddBlank = true)
        {
            List<SelectListItem> regiondata = new List<SelectListItem>();
            List<Level4Region> listRegions = await regions.OrderBy(s => s.Name).ToListAsync();
            if (bAddBlank)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "-1";
                regiondata.Add(sli);
            }
            foreach (Level4Region sp in listRegions)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = sp.Name;
                sli.Value = sp.Level4RegionID.ToString();
                regiondata.Add(sli);
            }
            return regiondata;

        }
        private async Task<List<SelectListItem>> BuildSpeciesSelect(DbSet<Species> species, bool bAddBlank = true, int spID = -2)
        {
            List<SelectListItem> speciesdropdownlistdata = new List<SelectListItem>();
            List<Species> listSpecies = await species.OrderBy(s => s.Genus).ToListAsync();
            if (bAddBlank)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "-1";
                speciesdropdownlistdata.Add(sli);
            }
            foreach (Species sp in listSpecies)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = sp.Genus.ToString();
                if (null != sp.SpeciesName)
                    sli.Text += "_" + sp.SpeciesName.ToString();
                if (null != sp.SubTaxa)
                    sli.Text += "_" + sp.SubTaxa.ToString();

                sli.Value = sp.SpeciesID.ToString();
                if (spID != -2 && spID == sp.SpeciesID)
                    sli.Selected = true;
                speciesdropdownlistdata.Add(sli);
            }
            return speciesdropdownlistdata;
        }

        private async Task<List<SelectListItem>> BuildCollSiteList(int siteID = -1, bool bAddBlank = true )
        {

            List<SelectListItem> colSitedropdownlistdata = new List<SelectListItem>();
            List<CollectionSite> listCollSites = await _context.CollectionSite.ToListAsync();
            if (bAddBlank)
            {
                if (bAddBlank)
                {
                    SelectListItem sli = new SelectListItem();
                    sli.Text = "";
                    sli.Value = "-1";
                    colSitedropdownlistdata.Add(sli);
                }

            }


            foreach (CollectionSite cs in listCollSites)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "-1";
                //sli.Text = cs.SiteCode + "_" + cs.SiteName;
                sli.Text = cs.SiteName;

                sli.Value = cs.CollectionSiteID.ToString();
                if (siteID != -1 && siteID == cs.CollectionSiteID)
                    sli.Selected = true;
                colSitedropdownlistdata.Add(sli);
            }
            return colSitedropdownlistdata;
        }
        private async Task<List<SelectListItem>> BuildFarmlSiteList(int selectedSiteID = -1)
        {
            // always include a blank and support selecting the passed in ID
            List<SelectListItem> farmSitedropdownlistdata = new List<SelectListItem>();

            SelectListItem sli = new SelectListItem();
            sli.Text = "";
            sli.Value = "-1";
            farmSitedropdownlistdata.Add(sli);



            List<FarmSite> listFarms = await _context.FarmSite.ToListAsync();
            foreach (FarmSite fs in listFarms)
            {
                sli = new SelectListItem();
                //sli.Text = fs.FarmCode + "_" + fs.FarmName;
                sli.Text = fs.FarmName;

                sli.Value = fs.FarmSiteID.ToString();
                if (selectedSiteID != -1 && selectedSiteID == fs.FarmSiteID)
                    sli.Selected = true;
                farmSitedropdownlistdata.Add(sli);
            }
            return farmSitedropdownlistdata;
        }
        private List<SelectListItem> BuildGradeList(string strSelected = "A")
        {
            List<SelectListItem> gradeList = new List<SelectListItem>();
            char c = 'A';

            for (int i = 0; i < Constants.MAX_SEED_GRADES; ++i)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = c.ToString();
                sli.Value = c.ToString();
                if (sli.Value == strSelected)
                    sli.Selected = true;
                int cVal = (int)c;
                cVal++;
                c = (char)cVal;
                gradeList.Add(sli);
            }
            return gradeList;
        }
        private List<SelectListItem> BuildGenerationList(int selectedGeneration = 0)
        {
            List<SelectListItem> genList = new List<SelectListItem>();

            for (int i = 0; i < Constants.MAX_SEED_GENERATIONS; ++i)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = i.ToString();
                sli.Value = i.ToString();
                if (i == selectedGeneration)
                    sli.Selected = true;
                genList.Add(sli);
            }
            return genList;
        }
        private async Task<int> GetNextSeedLotNumber()
        {
            //var max = _context.SeedLot.FromSql("select MAX(SeedLotNumber) from SeedLot where SeedLotID > 0");
            var max = -1;
            var conn = _context.Database.GetDbConnection();
            try
            {
                await conn.OpenAsync();
                using (var command = conn.CreateCommand())
                {
                    string query = "select MAX(SeedLotNumber) from SeedLot ";
                    command.CommandText = query;
                    DbDataReader reader = await command.ExecuteReaderAsync();
                    if (reader.HasRows)
                    {
                        while (await reader.ReadAsync())
                        {
                            max = reader.GetInt32(0);
                        }
                        if (-1 == max) max = 1;
                    }
                    reader.Dispose();
                }
            }
            finally
            {
                conn.Close();
            }
            return (int)++max;
        }
        public async Task<string> BuildSeedLotTag(int iSeedLotID,bool bIsPlugLotTag = false,int iPlugLotNumber=0) {
            try
            {
                var sl = _context.SeedLot.Where(s => s.SeedLotID == (int)(iSeedLotID)).Single();
                string strTag = await BuildSeedLotTag(sl, //sl.SpeciesID, sl.SourceType, sl.SourceSiteID,
                        //sl.FirstHarvestDate, sl.Generation, sl.Grade, sl.SeedLotNumber,
                        bIsPlugLotTag,iPlugLotNumber);
                return strTag;
                                }
            catch (Exception e)
            {
                return e.InnerException.Message;
            }

        }
        // BuildSeedLotTag
        // LotTag is calculated from �Species ID�, �Region�, �Collection Date�, �Generation�, Grade, 
        // Site Code and the �Lot ID�. Here is an example: ACHMIL-SJI-17-0-B-ACN-29456.  
        public async Task<string> BuildSeedLotTag(SeedLot sl ,
                bool bIsPlugLotTag = false,int iPlugLotNumber=0)
        {
            string strTag;
            string strSep = "-";
            // look up the species
            Species spc = await _context.Species.FindAsync(sl.SpeciesID);
            if (spc == null)
                strTag = "GGGSSS";
            else
            {
                strTag = spc.Genus.Substring(0, 3).ToUpper();
                strTag += spc.SpeciesName.Substring(0, 3).ToUpper();
            }
            strTag += strSep;
            // Region
            switch (sl.SourceType)
            {
                case eSourceType.Wild:
                    // lookup sitecode
                    CollectionSite cs = await _context.CollectionSite.FindAsync(sl.SourceSiteID);
                    if (null == cs)
                        strTag += "UNK";
                    else
                    {
                        strTag += cs.SiteCode;
                    }
                    break;
                case eSourceType.Farm:
                    strTag += "FFF";
                    break;
                case eSourceType.CNLMFarm:
                    strTag += "CLM";
// HERE                    List < Planting >lstSources =  GetOriginalSources(sl);

                    break;
                default:
                    string strMsg = "shouldn't get here";
                    ViewData["ErrorMessage"] = strMsg;
                    break;
            }
            strTag += strSep;
            // Year of collection
            if (null != sl.FirstHarvestDate)
            {
                DateTime dtTemp = (DateTime)sl.FirstHarvestDate;
                strTag += dtTemp.Year.ToString().Substring(2, 2);
            }
            else
            {
                strTag += "??";
            }
            strTag += strSep;
            // generation
            strTag += sl.Generation.ToString();
            strTag += strSep;
            // grade
            if (!bIsPlugLotTag)
                strTag += sl.Grade;
            else
                strTag += "P" + iPlugLotNumber.ToString();
            strTag += strSep;
            // SeedLot Number
            strTag += sl.SeedLotNumber.ToString();
            return strTag;
        }

        public  List<Planting> GetOriginalSources(SeedLot sl)
        {
            // get the current bed planting as of now
            BedTransactionsController btc = new BedTransactionsController(_context);
            JsonResult jsonRes =  (JsonResult)btc.GetCurrentPlanting((short)sl.BedID);
            
            List<Planting> lstPlantings = JsonConvert.DeserializeObject<List<Planting>>(jsonRes.ToString());
            foreach(Planting p in lstPlantings)
            {
                if(p.srcType == PlantingType.Plug) {
                    
                }
                if (p.srcType == PlantingType.Sow) {
                }
            }
            return lstPlantings;

        }
        private int ExtractProperSiteID(IFormCollection form, eSourceType sourceType)
        {
            var Value = form["SourceSiteID"];
            string strFarmID = Value[0];
            string strCollectionID = Value[1];
            int siteID;
            bool bSucceeded;
            if (eSourceType.Farm == sourceType)
            {
                bSucceeded = Int32.TryParse(strFarmID, out siteID);
            }
            else
            {
                bSucceeded = Int32.TryParse(strCollectionID, out siteID);
            }
            if (!bSucceeded)
                siteID = -1;
            return siteID;
        }
        private List<SelectListItem> BuildSourceTypeSelect()
        {
            List<SelectListItem> selList = new List<SelectListItem>();
            SelectListItem sel = new SelectListItem();
            sel.Text = "";
            sel.Value = "-1";
            sel.Selected = true;
            selList.Add(sel);

            SelectListItem sel1 = new SelectListItem();
            sel1.Value = ((int)eSourceType.Farm).ToString();
            sel1.Text = "Farm";
            selList.Add(sel1);

            SelectListItem sel2 = new SelectListItem();
            sel2.Value = ((int)(eSourceType.Wild)).ToString();
            sel2.Text = "Collected";
            selList.Add(sel2);

            return selList;
        }
        private List<SelectListItem> BuildSortBySelect()
        {
            string[] strColumns = { "", "SeedLotNumber", "LotTag", 
            "FirstHarvestDate","Status"};
            List<SelectListItem> selList = new List<SelectListItem>();
            for (int i = 0; i < strColumns.Length; ++i)
            {
                SelectListItem sel = new SelectListItem();
                sel.Text = strColumns[i];
                sel.Value = strColumns[i];
                if (0 == i)
                    sel.Selected = true;
                selList.Add(sel);
            }
            return selList;
        }
        private async Task<Dictionary<int,string>> BuildOrgDictionary()
        {
            Dictionary<int, string> dictOrg = new Dictionary<int, string>();
            List<Organization> listOrg = await _context.Organization.ToListAsync();
            foreach (Organization org in listOrg)
            {
                dictOrg.Add(org.OrganizationID, org.OrganizationName);
            }
            return dictOrg;
        }

        public async Task<float> GetQuantity(int seedLotID,string  strSince=null, string  strUntil=null)
        {
            float fTotal = -999;
            if (null == strSince)
                strSince = SeedTrack.Constants.STR_EARLIEST_SEARCH;
            if (null == strUntil)
                strUntil = SeedTrack.Constants.STR_LATEST_SEARCH;
            DateTime dtSince = Convert.ToDateTime(strSince);
            DateTime dtUntil = Convert.ToDateTime(strUntil);
            var seedLot = await _context.SeedLot.Include(t => t.SeedLotTransactions).SingleOrDefaultAsync(m => m.SeedLotID == seedLotID);
            if (seedLot == null)
            {
                 return fTotal;
            }
            fTotal = 0;
            foreach (SeedLotTransaction trans in seedLot.SeedLotTransactions)
            {
                if (trans.TransactionDate < dtSince || trans.TransactionDate > dtUntil)
                    continue;
                fTotal += trans.Quantity;
            }

            return fTotal;
        }

    }  // end SeedLotsController class
}  //end namespace
