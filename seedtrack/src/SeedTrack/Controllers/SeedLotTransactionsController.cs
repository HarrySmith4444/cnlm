using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotTransactionModels;
using SeedTrack.Models.SeedLotModels;
using Microsoft.AspNetCore.Authorization;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class SeedLotTransactionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SeedLotTransactionsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: SeedLotTransactions
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.SeedLotTransaction.Include(s => s.Organization).Include(s => s.Person);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: SeedLotTransactions/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seedLotTransaction = await _context.SeedLotTransaction.Include(o => o.Organization).Include(p => p.Person).SingleOrDefaultAsync(m => m.SeedLotTransactionID == id);
            if (seedLotTransaction == null) { 
            
                return NotFound();
            }
            SeedLotTransactionViewModel2 sltView = new SeedLotTransactionViewModel2();
            sltView.slt = seedLotTransaction;
            var seedLot = await _context.SeedLot.Include(s => s.Species).FirstOrDefaultAsync(s => s.SeedLotID == seedLotTransaction.SeedLotID);
            if (seedLot == null) return NotFound();
            sltView.sl = seedLot;
            return View(sltView);
        }
#if false
        // GET: SeedLotTransactions/Create
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Create()
        {
            ViewData["TransType"] = BuildTransTypeSelect(SeedLotTransactionType.WildCollection);
            Dictionary<int,string> pIDToOrgDict = await BuildPersonIDOrgNameDict();
            ViewData["PersToOrgDict"] = pIDToOrgDict;
            List<SelectListItem> selPeople =  await BuildPersonSelect(true);
            ViewData["People"] = selPeople;
            SeedLotTransactionViewModel slvm = new SeedLotTransactionViewModel(pIDToOrgDict,null);
            return View(slvm);
        }
#endif
        // GET: SeedLotTransactions/Create
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Create2(int? id)
        {
            var sl = await _context.SeedLot.Include(s => s.Species).SingleOrDefaultAsync(m => m.SeedLotID == id);
            if(null == sl)
                return NotFound();
            ViewData["TransType"] = BuildTransTypeSelect(SeedLotTransactionType.WildCollection);
            Dictionary<int, string> pIDToOrgDict = await BuildPersonIDOrgNameDict();
            ViewData["PersToOrgDict"] = pIDToOrgDict;
            List<SelectListItem> selPeople = await BuildPersonSelect(_context,true);
            ViewData["People"] = selPeople;
            SeedLotTransaction slt = new SeedLotTransaction();
            slt.SeedLotID = (int)id;
            slt.TransactionDate = DateTime.Now;
            return View(new Tuple<SeedLotTransaction, SeedLot>(slt, sl));
        }

        // POST: SeedLotTransactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]

        public async Task<IActionResult> Create(SeedLotTransaction Item1, SeedLot Item2)
        //public async Task<IActionResult> Create([Bind("SeedLotTransactionID,Notes,OldTransactionID,OrganizationID,PersonID,Quantity,SeedLotID,TransactionType")] SeedLotTransaction seedLotTransaction)
        {
            if (ModelState.IsValid)
            {
                Item1.CreatedBy = User.Identity.Name;
                Item1.CreationDate = DateTime.Now;
                Item1.ModifiedBy = User.Identity.Name;
                Item1.ModifiedDate = DateTime.Now;
                if(null != Item1.PersonID)
                {
                    // update the org ID
                    Person personTransacting = await _context.Person.FirstOrDefaultAsync(p => p.PersonID == Item1.PersonID);
                    Item1.OrganizationID = personTransacting.OrganizationID;
                }
                // handle sign of quantity.  Always input by user as positive 
                switch (Item1.TransactionType)
                {
                    case SeedLotTransactionType.Disbursement:
                    case SeedLotTransactionType.Discarded:
                        Item1.Quantity = -1 * Item1.Quantity;
                        break;
                }

                try
                //                using (_context)
                {
                    _context.Add(Item1);  // the transaction
                    // float fltNow = await slc.RecalcCurrentQuantity(Item1.SeedLotID); // from transactions
                    SeedLot sl = await _context.SeedLot.SingleAsync(a => a.SeedLotID == Item1.SeedLotID);
                    UpdateSeedLotQuantity(_context,sl , Item1.Quantity, Item1.TransactionType);
                    _context.Update(sl);
                    await _context.SaveChangesAsync();
                }
                catch(Exception ex)
                {
                    string strMsg = ex.Message;
                }
                return RedirectToAction("Details", "SeedLots", new { id = Item1.SeedLotID });
                //return RedirectToAction("Index");
            }
            ViewData["OrganizationID"] = new SelectList(_context.Organization, "OrganizationID", "OrganizationID", Item1.OrganizationID);
            ViewData["PersonID"] = new SelectList(_context.Person, "PersonID", "PersonID", Item1.PersonID);
            //return View(seedLotTransaction);
            return View(new Tuple<SeedLotTransaction, SeedLot>(Item1, Item2));
        }
        // GET: SeedLotTransactions/Allocate
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Allocate(int? id)
        {
            SeedLot sl;
            if (null != id) { 
            sl = await _context.SeedLot.Include(s => s.Species).SingleOrDefaultAsync(m => m.SeedLotID == id);
            if (null == sl)
                return NotFound();
            }else
            {
                sl = new SeedLot();
            }
            Dictionary<int, string> pIDToOrgDict = await BuildPersonIDOrgNameDict();
            ViewData["PersToOrgDict"] = pIDToOrgDict;
            List<SelectListItem> selPeople = await BuildPersonSelect(_context,true);
            ViewData["People"] = selPeople;
            SeedLotTransaction slt = new SeedLotTransaction();
            slt.SeedLotID = (int)id;
            slt.TransactionDate = DateTime.Now;
            slt.TransactionType = SeedLotTransactionType.Disbursement; // at least by default
            return View(new Tuple<SeedLotTransaction, SeedLot>(slt, sl));
        }

        // GET: SeedLotTransactions/Edit/5
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seedLotTransaction = await _context.SeedLotTransaction.SingleOrDefaultAsync(m => m.SeedLotTransactionID == id);
            if (seedLotTransaction == null)
            {
                return NotFound();
            }
            ViewData["OrganizationID"] = new SelectList(_context.Organization, "OrganizationID", "OrganizationID", seedLotTransaction.OrganizationID);
            ViewData["PersonID"] = new SelectList(_context.Person, "PersonID", "PersonID", seedLotTransaction.PersonID);
            return View(seedLotTransaction);
        }

        // POST: SeedLotTransactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Edit(int id, [Bind("SeedLotTransactionID,CreatedBy,CreationDate,ModifiedBy,ModifiedDate,Notes,OldTransactionID,OrganizationID,PersonID,Quantity,SeedLotID,TransactionType")] SeedLotTransaction seedLotTransaction)
        {
            if (id != seedLotTransaction.SeedLotTransactionID)
            {
                return NotFound();
            }
            seedLotTransaction.ModifiedBy = User.Identity.Name;
            seedLotTransaction.ModifiedDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                try
                {
                    // update current quantity in seedlot
                    string strNow = DateTime.Now.ToString();
                    _context.Update(seedLotTransaction);
                    await _context.SaveChangesAsync();
                    float fltCurrentQuantity = await RecalcCurrentQuantity(seedLotTransaction.SeedLotID);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SeedLotTransactionExists(seedLotTransaction.SeedLotTransactionID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", "SeedLots", new { id = seedLotTransaction.SeedLotID });
                //return RedirectToAction("Index");
            }
            ViewData["OrganizationID"] = new SelectList(_context.Organization, "OrganizationID", "OrganizationID", seedLotTransaction.OrganizationID);
            ViewData["PersonID"] = new SelectList(_context.Person, "PersonID", "PersonID", seedLotTransaction.PersonID);
            return View(seedLotTransaction);
        }

        // GET: SeedLotTransactions/Delete/5
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var seedLotTransaction = await _context.SeedLotTransaction.SingleOrDefaultAsync(m => m.SeedLotTransactionID == id);
            if (seedLotTransaction == null)
            {
                return NotFound();
            }

            return View(seedLotTransaction);
        }

        // POST: SeedLotTransactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var seedLotTransaction = await _context.SeedLotTransaction.SingleOrDefaultAsync(m => m.SeedLotTransactionID == id);
            int seedlotID = seedLotTransaction.SeedLotID;
            _context.SeedLotTransaction.Remove(seedLotTransaction);
            await _context.SaveChangesAsync();
            await RecalcCurrentQuantity(seedlotID);
            return RedirectToAction("Details", "SeedLots", new { id = seedlotID });
            //return RedirectToAction("Index");
        }

        private bool SeedLotTransactionExists(int id)
        {
            return _context.SeedLotTransaction.Any(e => e.SeedLotTransactionID == id);
        }
        // Handle setting the stored quantities uniformly
        public static void UpdateSeedLotQuantity(ApplicationDbContext context, SeedLot seedLot , float quantity, SeedLotTransactionType transactionType) 
        {
            if (null == seedLot)
                return;

            seedLot.CurrentQuantity += quantity;
            if(transactionType == SeedLotTransactionType.Harvest 
                || transactionType == SeedLotTransactionType.ReceiveFromPartner
                || transactionType == SeedLotTransactionType.WildCollection)
            {
                seedLot.InitialQuantity += quantity;
            }
            if (seedLot.CurrentQuantity > 0)
            {
                seedLot.Status = eSeedLotState.InStock; }
            else { 
                seedLot.Status = eSeedLotState.OutOfStock;
            }
            context.Update(seedLot);
            context.SaveChangesAsync();
            return;
        }
        public async Task<int> RecalcAll(bool bDoSave = true, string strUntilDate = "")
        {
            string strMessage = "";
            List<int> lstSeedLots = await _context.SeedLot.Select(a => a.SeedLotID).ToListAsync();
            foreach(int id in lstSeedLots)
            {
                try
                {
                    await RecalcCurrentQuantity(id, bDoSave, strUntilDate);
                }
                catch (Exception ex) {
                    strMessage = ex.Message;
                    return 0;
                }
            }
            return 1;
        }

        // This  routine  upates the quantity and 
        // sets the status to reflect the current state.
        // Generally not called with a date as we want to see now.
        // A variation that didn't save anything could be called with a date for 
        // questions of how much you had at a certain date
        // Should only be called to recalculate a suspicious quantity in the seedlot. 

        public async Task<float> RecalcCurrentQuantity(int seedlotID, Boolean bDoSave = true,string strUntilDate = "")
        {
            var seedLot = await _context.SeedLot.FirstOrDefaultAsync(s => s.SeedLotID == seedlotID);
            List<SeedLotTransaction> lstTrans = await _context.SeedLotTransaction.Where(i => i.SeedLotID == seedlotID).OrderBy(j => j.TransactionDate).ToListAsync();
            float quant = (float)0.0;
            float initQuant = (float)0.0;
            if (strUntilDate.Length < 1)
                strUntilDate = DateTime.Now.ToString();
            DateTime dtUntil = Convert.ToDateTime(strUntilDate);
            foreach (SeedLotTransaction slt in lstTrans)
            {
                if (slt.TransactionDate <= dtUntil)
                    quant += slt.Quantity;
                if (slt.TransactionType == SeedLotTransactionType.Harvest
                    || slt.TransactionType == SeedLotTransactionType.ReceiveFromPartner
                    || slt.TransactionType == SeedLotTransactionType.WildCollection)
                    initQuant += slt.Quantity;
            }
            // Round off as appropriate
            quant = (float)Math.Round((Decimal)quant, Constants.SEEDLOTQUANTITYDIGITS, MidpointRounding.AwayFromZero);
            initQuant = (float)Math.Round((Decimal)initQuant, Constants.SEEDLOTQUANTITYDIGITS, MidpointRounding.AwayFromZero);
            if (bDoSave)
            {
                // update the db
                seedLot.CurrentQuantity = quant;
                seedLot.InitialQuantity = initQuant;
                seedLot.Status = (quant > (float)0.00) ? (eSeedLotState.InStock) : (eSeedLotState.OutOfStock);
                _context.SeedLot.Update(seedLot);
                await _context.SaveChangesAsync();
            }
            return quant;
        }

        public static async Task<int> UpdateCurrentQuantity(ApplicationDbContext cntxt, int seedlotID, float currentDelta, float initialDelta)
        {
            try
            {
                SeedLot sl = await cntxt.SeedLot.Where(a => a.SeedLotID == seedlotID).SingleAsync();
                sl.CurrentQuantity += currentDelta;
                sl.InitialQuantity += initialDelta;
                cntxt.Update(sl);
                await cntxt.SaveChangesAsync();
            }
            catch (Exception) { return -1; }
            return 0;
        }
        private List<SelectListItem> BuildTransTypeSelect(SeedLotTransactionType eTransType)
        {
            List<SelectListItem> statelistdata = new List<SelectListItem>();
            foreach (SeedLotTransactionType e in Enum.GetValues(typeof(SeedLotTransactionType)))
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = e.ToString();
                sli.Value = ((int)e).ToString();
                if (eTransType == e)
                    sli.Selected = true;
                statelistdata.Add(sli);
            }
            return statelistdata;
        }
        private async Task<Dictionary<int, string>> BuildPersonIDOrgNameDict()
        {
            List<SelectListItem> personlistdata = new List<SelectListItem>();
            List<Person> listPerson = await _context.Person.Include(o => o.Organization).ToListAsync();
            Dictionary<int, string> dictPToOrg = new Dictionary<int, string>();
            dictPToOrg.Add(0, "Unknown");
            foreach (Person p in listPerson)
            {
                if (null == p.Organization)
                {
                    dictPToOrg.Add(p.PersonID, "No Organization");
                }
                else
                {
                    dictPToOrg.Add(p.PersonID, p.Organization.OrganizationName);
                }
            }
            return dictPToOrg;
        }
        public static async Task<List<SelectListItem>> BuildPersonSelect( ApplicationDbContext dbx, bool bAddBlank = false, int iSelected=0)
        {
            List<SelectListItem> personlistdata = new List<SelectListItem>();
            List<Person> listPerson = await dbx.Person.OrderBy(o => o.LastName).ToListAsync();
            if (bAddBlank)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = "";
                sli.Value = "0";
                personlistdata.Add(sli);
            }
            foreach (Person p in listPerson)
            {
                SelectListItem sli = new SelectListItem();
                sli.Text = p.LastName + ", " + p.FirstName;
                sli.Value = p.PersonID.ToString();
                if (iSelected != 0 && iSelected == p.PersonID)
                    sli.Selected = true;
                personlistdata.Add(sli);
            }
            return personlistdata;
        }

    }  // end class
}  // end namespace
