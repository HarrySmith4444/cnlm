using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.PlugLotModels;

namespace SeedTrack.Controllers
{
    public class PlugSoilsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlugSoilsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: PlugSoils
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlugSoil.ToListAsync());
        }

        // GET: PlugSoils/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugSoil = await _context.PlugSoil.SingleOrDefaultAsync(m => m.PlugSoilID == id);
            if (plugSoil == null)
            {
                return NotFound();
            }

            return View(plugSoil);
        }

        // GET: PlugSoils/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlugSoils/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlugSoilID,PlugSoilDescription,PlugSoilName")] PlugSoil plugSoil)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plugSoil);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(plugSoil);
        }

        // GET: PlugSoils/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugSoil = await _context.PlugSoil.SingleOrDefaultAsync(m => m.PlugSoilID == id);
            if (plugSoil == null)
            {
                return NotFound();
            }
            return View(plugSoil);
        }

        // POST: PlugSoils/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlugSoilID,PlugSoilDescription,PlugSoilName")] PlugSoil plugSoil)
        {
            if (id != plugSoil.PlugSoilID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plugSoil);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlugSoilExists(plugSoil.PlugSoilID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(plugSoil);
        }

        // GET: PlugSoils/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugSoil = await _context.PlugSoil.SingleOrDefaultAsync(m => m.PlugSoilID == id);
            if (plugSoil == null)
            {
                return NotFound();
            }

            return View(plugSoil);
        }

        // POST: PlugSoils/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plugSoil = await _context.PlugSoil.SingleOrDefaultAsync(m => m.PlugSoilID == id);
            _context.PlugSoil.Remove(plugSoil);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlugSoilExists(int id)
        {
            return _context.PlugSoil.Any(e => e.PlugSoilID == id);
        }
    }
}
