using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.PlugLotModels;

namespace SeedTrack.Controllers
{
    public class PlugMaintenanceActionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlugMaintenanceActionsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: PlugMaintenanceActions
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlugMaintenanceAction.ToListAsync());
        }

        // GET: PlugMaintenanceActions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugMaintenanceAction = await _context.PlugMaintenanceAction.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugMaintenanceAction == null)
            {
                return NotFound();
            }

            return View(plugMaintenanceAction);
        }

        // GET: PlugMaintenanceActions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlugMaintenanceActions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlugLotID,PlugMaintenanceActionDate,PlugMaintenanceActionNotes")] PlugMaintenanceAction plugMaintenanceAction)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plugMaintenanceAction);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(plugMaintenanceAction);
        }

        // GET: PlugMaintenanceActions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugMaintenanceAction = await _context.PlugMaintenanceAction.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugMaintenanceAction == null)
            {
                return NotFound();
            }
            return View(plugMaintenanceAction);
        }

        // POST: PlugMaintenanceActions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlugLotID,PlugMaintenanceActionDate,PlugMaintenanceActionNotes")] PlugMaintenanceAction plugMaintenanceAction)
        {
            if (id != plugMaintenanceAction.PlugLotID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plugMaintenanceAction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlugMaintenanceActionExists(plugMaintenanceAction.PlugLotID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(plugMaintenanceAction);
        }

        // GET: PlugMaintenanceActions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugMaintenanceAction = await _context.PlugMaintenanceAction.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugMaintenanceAction == null)
            {
                return NotFound();
            }

            return View(plugMaintenanceAction);
        }

        // POST: PlugMaintenanceActions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plugMaintenanceAction = await _context.PlugMaintenanceAction.SingleOrDefaultAsync(m => m.PlugLotID == id);
            _context.PlugMaintenanceAction.Remove(plugMaintenanceAction);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlugMaintenanceActionExists(int id)
        {
            return _context.PlugMaintenanceAction.Any(e => e.PlugLotID == id);
        }
    }
}
