using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.PlugLotModels;

namespace SeedTrack.Controllers
{
    public class PlugLotLocationTransactionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlugLotLocationTransactionsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: PlugLotLocationTransactions
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlugLotLocationTransaction.ToListAsync());
        }

        // GET: PlugLotLocationTransactions/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLotLocationTransaction = await _context.PlugLotLocationTransaction.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLotLocationTransaction == null)
            {
                return NotFound();
            }

            return View(plugLotLocationTransaction);
        }

        // GET: PlugLotLocationTransactions/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlugLotLocationTransactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlugLotID,PlugLotLocationID,PlugLotMovementDate")] PlugLotLocationTransaction plugLotLocationTransaction)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plugLotLocationTransaction);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(plugLotLocationTransaction);
        }

        // GET: PlugLotLocationTransactions/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLotLocationTransaction = await _context.PlugLotLocationTransaction.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLotLocationTransaction == null)
            {
                return NotFound();
            }
            return View(plugLotLocationTransaction);
        }

        // POST: PlugLotLocationTransactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlugLotID,PlugLotLocationID,PlugLotMovementDate")] PlugLotLocationTransaction plugLotLocationTransaction)
        {
            if (id != plugLotLocationTransaction.PlugLotID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plugLotLocationTransaction);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlugLotLocationTransactionExists(plugLotLocationTransaction.PlugLotID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(plugLotLocationTransaction);
        }

        // GET: PlugLotLocationTransactions/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLotLocationTransaction = await _context.PlugLotLocationTransaction.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLotLocationTransaction == null)
            {
                return NotFound();
            }

            return View(plugLotLocationTransaction);
        }

        // POST: PlugLotLocationTransactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plugLotLocationTransaction = await _context.PlugLotLocationTransaction.SingleOrDefaultAsync(m => m.PlugLotID == id);
            _context.PlugLotLocationTransaction.Remove(plugLotLocationTransaction);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlugLotLocationTransactionExists(int id)
        {
            return _context.PlugLotLocationTransaction.Any(e => e.PlugLotID == id);
        }
    }
}
