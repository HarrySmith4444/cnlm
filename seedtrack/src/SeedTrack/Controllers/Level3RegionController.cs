using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotModels;
using Microsoft.AspNetCore.Authorization;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class Level3RegionController : Controller
    {
        private readonly ApplicationDbContext _context;

        public Level3RegionController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: Level3Region
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Level3Region.ToListAsync());
        }

        // GET: Level3Region/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var level3Region = await _context.Level3Region.SingleOrDefaultAsync(m => m.Level3RegionID == id);
            if (level3Region == null)
            {
                return NotFound();
            }

            return View(level3Region);
        }

        // GET: Level3Region/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Level3Region/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Create([Bind("Level3RegionID,Name,Code")] Level3Region level3Region)
        {
            if (ModelState.IsValid)
            {
                _context.Add(level3Region);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(level3Region);
        }

        // GET: Level3Region/Edit/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var level3Region = await _context.Level3Region.SingleOrDefaultAsync(m => m.Level3RegionID == id);
            if (level3Region == null)
            {
                return NotFound();
            }
            return View(level3Region);
        }

        // POST: Level3Region/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int id, [Bind("Level3RegionID,Name,Code")] Level3Region level3Region)
        {
            if (id != level3Region.Level3RegionID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(level3Region);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!Level3RegionExists(level3Region.Level3RegionID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(level3Region);
        }

        // GET: Level3Region/Delete/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var level3Region = await _context.Level3Region.SingleOrDefaultAsync(m => m.Level3RegionID == id);
            if (level3Region == null)
            {
                return NotFound();
            }

            return View(level3Region);
        }

        // POST: Level3Region/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var level3Region = await _context.Level3Region.SingleOrDefaultAsync(m => m.Level3RegionID == id);
            _context.Level3Region.Remove(level3Region);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool Level3RegionExists(int id)
        {
            return _context.Level3Region.Any(e => e.Level3RegionID == id);
        }
    }
}
