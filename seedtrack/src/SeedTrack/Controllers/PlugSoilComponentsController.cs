using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.PlugLotModels;

namespace SeedTrack.Controllers
{
    public class PlugSoilComponentsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlugSoilComponentsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: PlugSoilComponents
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlugSoilComponent.ToListAsync());
        }

        // GET: PlugSoilComponents/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugSoilComponent = await _context.PlugSoilComponent.SingleOrDefaultAsync(m => m.PlugSoilComponentID == id);
            if (plugSoilComponent == null)
            {
                return NotFound();
            }

            return View(plugSoilComponent);
        }

        // GET: PlugSoilComponents/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlugSoilComponents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlugSoilComponentID,PlugSoilComponentDescription,PlugSoilComponentName")] PlugSoilComponent plugSoilComponent)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plugSoilComponent);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(plugSoilComponent);
        }

        // GET: PlugSoilComponents/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugSoilComponent = await _context.PlugSoilComponent.SingleOrDefaultAsync(m => m.PlugSoilComponentID == id);
            if (plugSoilComponent == null)
            {
                return NotFound();
            }
            return View(plugSoilComponent);
        }

        // POST: PlugSoilComponents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlugSoilComponentID,PlugSoilComponentDescription,PlugSoilComponentName")] PlugSoilComponent plugSoilComponent)
        {
            if (id != plugSoilComponent.PlugSoilComponentID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plugSoilComponent);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlugSoilComponentExists(plugSoilComponent.PlugSoilComponentID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(plugSoilComponent);
        }

        // GET: PlugSoilComponents/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugSoilComponent = await _context.PlugSoilComponent.SingleOrDefaultAsync(m => m.PlugSoilComponentID == id);
            if (plugSoilComponent == null)
            {
                return NotFound();
            }

            return View(plugSoilComponent);
        }

        // POST: PlugSoilComponents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plugSoilComponent = await _context.PlugSoilComponent.SingleOrDefaultAsync(m => m.PlugSoilComponentID == id);
            _context.PlugSoilComponent.Remove(plugSoilComponent);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlugSoilComponentExists(int id)
        {
            return _context.PlugSoilComponent.Any(e => e.PlugSoilComponentID == id);
        }
    }
}
