﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using SeedTrack.Models;
using SeedTrack.Models.ManageViewModels;
using SeedTrack.Services;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly DbContextOptions<ApplicationDbContext> _dbcOptions;


        public ManageController(
        UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager,
        IEmailSender emailSender,
        ISmsSender smsSender,
        ILoggerFactory loggerFactory,
        RoleManager<IdentityRole> roleManager,
        DbContextOptions<ApplicationDbContext> dbcOptions)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _logger = loggerFactory.CreateLogger<ManageController>();
            _roleManager = roleManager;
            _dbcOptions = dbcOptions;
        }

        //
        // GET: /Manage/Index
        [HttpGet]
        public async Task<IActionResult> Index(ManageMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.SetTwoFactorSuccess ? "Your two-factor authentication provider has been set."
                : message == ManageMessageId.Error ? "An error has occurred."
                : message == ManageMessageId.AddPhoneSuccess ? "Your phone number was added."
                : message == ManageMessageId.RemovePhoneSuccess ? "Your phone number was removed."
                : "";

            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var model = new IndexViewModel
            {
                HasPassword = await _userManager.HasPasswordAsync(user),
                PhoneNumber = await _userManager.GetPhoneNumberAsync(user),
                TwoFactor = await _userManager.GetTwoFactorEnabledAsync(user),
                Logins = await _userManager.GetLoginsAsync(user),
                BrowserRemembered = await _signInManager.IsTwoFactorClientRememberedAsync(user)
            };
            return View(model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> RemoveLogin(RemoveLoginViewModel account)
        {
            ManageMessageId? message = ManageMessageId.Error;
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.RemoveLoginAsync(user, account.LoginProvider, account.ProviderKey);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    message = ManageMessageId.RemoveLoginSuccess;
                }
            }
            return RedirectToAction(nameof(ManageLogins), new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // Generate the token and send it
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, model.PhoneNumber);
            await _smsSender.SendSmsAsync(model.PhoneNumber, "Your security code is: " + code);
            return RedirectToAction(nameof(VerifyPhoneNumber), new { PhoneNumber = model.PhoneNumber });
        }

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> EnableTwoFactorAuthentication()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                await _userManager.SetTwoFactorEnabledAsync(user, true);
                await _signInManager.SignInAsync(user, isPersistent: false);
                _logger.LogInformation(1, "User enabled two-factor authentication.");
            }
            return RedirectToAction(nameof(Index), "Manage");
        }

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DisableTwoFactorAuthentication()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                await _userManager.SetTwoFactorEnabledAsync(user, false);
                await _signInManager.SignInAsync(user, isPersistent: false);
                _logger.LogInformation(2, "User disabled two-factor authentication.");
            }
            return RedirectToAction(nameof(Index), "Manage");
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, phoneNumber);
            // Send an SMS to verify the phone number
            return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.ChangePhoneNumberAsync(user, model.PhoneNumber, model.Code);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.AddPhoneSuccess });
                }
            }
            // If we got this far, something failed, redisplay the form
            ModelState.AddModelError(string.Empty, "Failed to verify phone number");
            return View(model);
        }

        //
        // POST: /Manage/RemovePhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> RemovePhoneNumber()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.SetPhoneNumberAsync(user, null);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.RemovePhoneSuccess });
                }
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }

        //
        // GET: /Manage/ChangePassword
        [HttpGet]
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public IActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    _logger.LogInformation(3, "User changed their password successfully.");
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.ChangePasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }

        //
        // GET: /Manage/SetPassword
        [HttpGet]
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public IActionResult SetPassword()
        {
            return View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                var result = await _userManager.AddPasswordAsync(user, model.NewPassword);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction(nameof(Index), new { Message = ManageMessageId.SetPasswordSuccess });
                }
                AddErrors(result);
                return View(model);
            }
            return RedirectToAction(nameof(Index), new { Message = ManageMessageId.Error });
        }
        //GET: /Manage/ManageUserRoles
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> ManageUserRoles(ManageMessageId? message = null)
        {
            var context = new ApplicationDbContext(_dbcOptions);
            var allUsers = context.Users.OrderBy(x => x.Email);
            var allRoles = context.Roles;
            var allUserRoles = new SortedList< NamedIdentityUserRole, IdentityUser>(new TypeComparer());
            var userStore = new UserStore<IdentityUser>(context);
//            var userManager = new UserManager<IdentityUser>(userStore, null, null, null, null, null, null, null, null);
//            var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context), null, null, null, null, null);
            foreach (IdentityUser user in allUsers)
            {
                // the call below is necessary to have Roles populated in each users object in allUsers
                await context.Users.Include(x => x.Roles).OrderBy(y=> y.Email).FirstOrDefaultAsync(n => n.UserName == user.UserName);
                // load each role assigned to a user and get it's name, not just it's ID
                foreach(IdentityUserRole<String> userrole in user.Roles)
                {
                    IdentityRole ir = await context.Roles.FindAsync(userrole.RoleId);
                    NamedIdentityUserRole niur = new NamedIdentityUserRole(ir.Name, userrole.RoleId, userrole.UserId,user.Email);
                    int index=0;
                    if (index == allUserRoles.IndexOfValue(user))
                    {
                        if (-1 == index)
                        {
                            // user already exists in userrole table, must remove it first
                            allUserRoles.RemoveAt(index);
                        }
                    }
                    allUserRoles.Add(niur,user);
                }
            }
            
            string selectedRoleName = "PowerUser";
            string selectedUserName = "HarrySmith4444@gmail.com";

            return View(new ManageUserRolesViewModel
            {
                AllRoles = allRoles.ToList<IdentityRole>(),
                AllUsers = allUsers.ToList<ApplicationUser>(),
                AllUserRoles = allUserRoles,
                SelectedRoleName = selectedRoleName,
                SelectedUserName = selectedUserName
                
            });

        }
        //
        // POST: /Manage//DefineUserRole
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> DefineUserRole(string alluserssel,string allrolessel)
        {
            var context = new ApplicationDbContext(_dbcOptions);
            var userStore = new UserStore<IdentityUser>(context);
            var allUsers = context.Users.Include(x=> x.Roles);
            // Get the IdentityUser that matches the selected username
            string userNameSelected = alluserssel;
            IdentityUser userIDSelected = null;
            foreach (IdentityUser iU in allUsers)
            {
                if (iU.UserName == userNameSelected)
                {
                    userIDSelected = iU;
                    break;
                }
            }
            var userManager = new UserManager<IdentityUser>(userStore, null, null, null, null, null, null, null, null);
            // get current role so we can remove it
            foreach (IdentityUserRole<string> ir in userIDSelected.Roles)
            {
                IdentityRole  roleID  = await context.Roles.Where(x => (x.Id.Equals(ir.RoleId))).FirstOrDefaultAsync();
                await userManager.RemoveFromRoleAsync(userIDSelected, roleID.Name);
                break;

            }
            IdentityUser iUser = userIDSelected;
            try
            {
                await userManager.AddToRoleAsync(iUser, allrolessel);
            }
            catch
            {

            }

            return RedirectToAction(nameof(ManageUserRoles), null);
        }

        //GET: /Manage/ManageLogins
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> ManageLogins(ManageMessageId? message = null)
        {
            ViewData["StatusMessage"] =
                message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.AddLoginSuccess ? "The external login was added."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var userLogins = await _userManager.GetLoginsAsync(user);
            var otherLogins = _signInManager.GetExternalAuthenticationSchemes().Where(auth => userLogins.All(ul => auth.AuthenticationScheme != ul.LoginProvider)).ToList();
            ViewData["ShowRemoveButton"] = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public IActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            var redirectUrl = Url.Action("LinkLoginCallback", "Manage");
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl, _userManager.GetUserId(User));
            return Challenge(properties, provider);
        }

        //
        // GET: /Manage/LinkLoginCallback
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> LinkLoginCallback()
        {
            var user = await GetCurrentUserAsync();
            if (user == null)
            {
                return View("Error");
            }
            var info = await _signInManager.GetExternalLoginInfoAsync(await _userManager.GetUserIdAsync(user));
            if (info == null)
            {
                return RedirectToAction(nameof(ManageLogins), new { Message = ManageMessageId.Error });
            }
            var result = await _userManager.AddLoginAsync(user, info);
            var message = result.Succeeded ? ManageMessageId.AddLoginSuccess : ManageMessageId.Error;
            return RedirectToAction(nameof(ManageLogins), new { Message = message });
        }

        //GET: /Manage/DeleteUser
        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public IActionResult DeleteUser(ManageMessageId? message = null)
        {
            var context = new ApplicationDbContext(_dbcOptions);
            var allUsers = context.Users.OrderBy(x => x.Email);
            var allRoles = context.Roles;
            var allUserRoles = new SortedList<NamedIdentityUserRole, IdentityUser>(new TypeComparer());
            var userStore = new UserStore<IdentityUser>(context);
            List<SelectListItem> lUsers = new List<SelectListItem>();
            foreach (var user in allUsers)
            {
                SelectListItem itm = new SelectListItem();
                itm.Text = user.UserName;
                itm.Value = user.Id;
                lUsers.Add(itm);
            }
            ViewData["UserList"] = lUsers;
            return View();
        }
        // POST: /Manage/Delete/

        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteUserConfirmed(string PersonID)
        {
            if (ModelState.IsValid)
            {
                if (PersonID == null)
                {
                    return View("Error");
                }

                var user = await _userManager.FindByIdAsync(PersonID);
                var logins = user.Logins;
                var rolesForUser = await _userManager.GetRolesAsync(user);
                var context = new ApplicationDbContext(_dbcOptions);

                using (var transaction = context.Database.BeginTransaction())
                {
                    foreach (var login in logins.ToList())
                    {
                        await _userManager.RemoveLoginAsync(user, login.LoginProvider, login.ProviderKey);
                    }

                    if (rolesForUser.Count() > 0)
                    {
                        foreach (var item in rolesForUser.ToList())
                        {
                            // item should be the name of the role
                            var result = await _userManager.RemoveFromRoleAsync(user, item);
                        }
                    }

                    await _userManager.DeleteAsync(user);
                    transaction.Commit();
                }

                return RedirectToAction("Index","Home");
            }
            else
            {
                return View();
            }
        }
        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            AddLoginSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        #endregion
    }
    public class TypeComparer : Comparer<NamedIdentityUserRole>
    {
        public override int Compare(NamedIdentityUserRole x, NamedIdentityUserRole y)
        {
            if (ReferenceEquals(x, y)) return 0;
            if(null != x && null != y)
            {
                if (x.UserId != y.UserId) 
                    return x.Email.CompareTo(y.Email);
                if(x.RoleId != y.RoleId)
                    return 1;
                else
                    return 0;
            }
            return 1;
        }
    }
}
