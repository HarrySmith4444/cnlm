using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.PlugLotModels;

namespace SeedTrack.Controllers
{
    public class PlugLotObservationsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PlugLotObservationsController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: PlugLotObservations
        public async Task<IActionResult> Index()
        {
            return View(await _context.PlugLotObservations.ToListAsync());
        }

        // GET: PlugLotObservations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLotObservations = await _context.PlugLotObservations.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLotObservations == null)
            {
                return NotFound();
            }

            return View(plugLotObservations);
        }

        // GET: PlugLotObservations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PlugLotObservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlugLotID,PlugLotObservation,PlugLotObservationDate")] PlugLotObservations plugLotObservations)
        {
            if (ModelState.IsValid)
            {
                _context.Add(plugLotObservations);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(plugLotObservations);
        }

        // GET: PlugLotObservations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLotObservations = await _context.PlugLotObservations.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLotObservations == null)
            {
                return NotFound();
            }
            return View(plugLotObservations);
        }

        // POST: PlugLotObservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlugLotID,PlugLotObservation,PlugLotObservationDate")] PlugLotObservations plugLotObservations)
        {
            if (id != plugLotObservations.PlugLotID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(plugLotObservations);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PlugLotObservationsExists(plugLotObservations.PlugLotID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(plugLotObservations);
        }

        // GET: PlugLotObservations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var plugLotObservations = await _context.PlugLotObservations.SingleOrDefaultAsync(m => m.PlugLotID == id);
            if (plugLotObservations == null)
            {
                return NotFound();
            }

            return View(plugLotObservations);
        }

        // POST: PlugLotObservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var plugLotObservations = await _context.PlugLotObservations.SingleOrDefaultAsync(m => m.PlugLotID == id);
            _context.PlugLotObservations.Remove(plugLotObservations);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool PlugLotObservationsExists(int id)
        {
            return _context.PlugLotObservations.Any(e => e.PlugLotID == id);
        }
    }
}
