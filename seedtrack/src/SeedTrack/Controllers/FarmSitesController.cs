using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SeedTrack.Data;
using SeedTrack.Models.SeedLotModels;
using Microsoft.AspNetCore.Authorization;

namespace SeedTrack.Controllers
{
    [Authorize]
    public class FarmSitesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public FarmSitesController(ApplicationDbContext context)
        {
            _context = context;    
        }

        // GET: FarmSites
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.FarmSite.ToListAsync());
        }

        // GET: FarmSites/Details/5
        [Authorize(Roles = "Administrator, PowerUser, Employee, Guest")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var farmSite = await _context.FarmSite.SingleOrDefaultAsync(m => m.FarmSiteID == id);
            if (farmSite == null)
            {
                return NotFound();
            }

            return View(farmSite);
        }

        // GET: FarmSites/Create
        [Authorize(Roles = "Administrator, PowerUser")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: FarmSites/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Create([Bind("FarmSiteID,FarmCode,FarmName")] FarmSite farmSite)
        {
            if (ModelState.IsValid)
            {
                _context.Add(farmSite);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(farmSite);
        }

        // GET: FarmSites/Edit/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var farmSite = await _context.FarmSite.SingleOrDefaultAsync(m => m.FarmSiteID == id);
            if (farmSite == null)
            {
                return NotFound();
            }
            return View(farmSite);
        }

        // POST: FarmSites/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Edit(int id, [Bind("FarmSiteID,FarmCode,FarmName")] FarmSite farmSite)
        {
            if (id != farmSite.FarmSiteID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(farmSite);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FarmSiteExists(farmSite.FarmSiteID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(farmSite);
        }

        // GET: FarmSites/Delete/5
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var farmSite = await _context.FarmSite.SingleOrDefaultAsync(m => m.FarmSiteID == id);
            if (farmSite == null)
            {
                return NotFound();
            }

            return View(farmSite);
        }

        // POST: FarmSites/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, PowerUser")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var farmSite = await _context.FarmSite.SingleOrDefaultAsync(m => m.FarmSiteID == id);
            _context.FarmSite.Remove(farmSite);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool FarmSiteExists(int id)
        {
            return _context.FarmSite.Any(e => e.FarmSiteID == id);
        }
    }
}
