﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SetInitialSeedLotState
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int iCurrentSeedLotID = 0;
            try
            {
                // connect to database
                string connString;
                connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                if (null == connString)
                    connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                SqlConnection connection = new SqlConnection(connString);
                SqlConnection connection2 = new SqlConnection(connString);
                connection.Open();
                connection2.Open();
                // Get a list of all the Seedlot IDs so we can go through each one and update it
                List<int> lstSeedLots = new List<int>();
                string strQuery = "select SeedLotID from SeedLot;";
                SqlCommand command = new SqlCommand(strQuery, connection);
                SqlCommand command2 = new SqlCommand();
                command2.Connection = connection2;
                // Using statement below is so that the reader goes out of scope when we are done
                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int index = Convert.ToInt32(reader[0].ToString());
                            lstSeedLots.Add(index);
                        }
                        Console.WriteLine("Processing {0} seedlots.", lstSeedLots.Count);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return;
                    }
                }
                // walk the list of Seedlots and look up each one's transactions.
                // total these and if the total is positive update the status to "In Stock" - 2
                // else "Out of Stock" -  3
                string strBaseQuery = "select Quantity from SeedLotTransaction where SeedLotID = ";
                foreach (int seedlotID in lstSeedLots)
                {
                    iCurrentSeedLotID = seedlotID;
                    strQuery = strBaseQuery + seedlotID.ToString() + ";";
                    command.CommandText = strQuery;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            float fltQuant = 0.0F;
                            while (reader.Read())
                            {
                                fltQuant += Convert.ToSingle(reader[0].ToString());

                            }
                            Console.WriteLine("Seedlot {0} total quanity is {1}", seedlotID, fltQuant);
                            string strUpdate = "Update Seedlot SET Status = ";
                            if (fltQuant > 0.0F)
                            {
                                strUpdate += ((int)eSeedLotState.InStock).ToString() + " where SeedLotID = " + seedlotID.ToString();
                            }
                            else
                            {
                                strUpdate += ((int)eSeedLotState.OutOfStock).ToString() + " where SeedLotID = " + seedlotID.ToString();
                            }
                            command2.CommandText = strUpdate;
                            command2.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            return;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message + " on seedlot {0}", iCurrentSeedLotID);
                Environment.Exit(-1);

            }

        }

        // string constants not really necessary but good practice for things that could change and
        // to prevent mistyping in comparisons
        public enum eSeedLotState { Raw = 0, ReadyForAllocation = 1, InStock = 2, OutOfStock = 3 }

    }
}
