﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImportSeedLots
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.IO;
    using System.Data;
    using System.Data.SqlClient;
    using System.Collections;
    //
    // Read the input CSV file for SeedLots,  construct
    // an insert statement and put the records in the database.

    namespace ImportSeedLots
    {
        public class Program
        {
            public static void Main(string[] args)
            {
                bool bSpeciesCheck = false;
                bool bCommaCheck = false;
                StreamWriter sw = null;
                if (args.Length < 1)
                {
                    System.Console.WriteLine("Usage: ImportSeedLots inputCSVfile");
                    Environment.Exit(0);
                }
                if(args.Length>1)
                {
                    if ("CheckSpecies" == args[1]) { 
                        bSpeciesCheck = true;
                        FileStream fs = new FileStream("SpeciesIssues.txt", FileMode.Create, FileAccess.Write);
                        sw = new StreamWriter(fs);
                    }else if("CountCommas" == args[1]){
                        bCommaCheck = true;
                        FileStream fs = new FileStream("CommaCheck.txt", FileMode.Create, FileAccess.Write);
                        sw = new StreamWriter(fs);
                    }
                    else
                    {
                        System.Console.WriteLine("Unrecognized 2nd argument {}",args[1]);
                        Environment.Exit(0);
                    }
                }
                string strInFile = args[0];
                string[] fields = null;
                int rowID = 0;
                try
                {
                    // connect to database
                    string connString;
                    connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                    if (null == connString)
                        connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                    SqlConnection connection = new SqlConnection(connString);
                    // Need dictionary tables for RegionCode, Species[ScientificName=Genus+" "+Species], FarmSite, CollectionSite
                    // from the database.
                    Dictionary<String, int> dictlevel4 = new Dictionary<String, int>();
                    string strQuery = "select Name,Level4RegionID from Level4Region;";
                    connection.Open();
                    bool bSuccess = BuildDictionary(dictlevel4, strQuery, connection);
                    Dictionary<string, Tuple<int, string>> dictFarm = new Dictionary<string, Tuple<int, string>>();
                    strQuery = "select FarmName,FarmSiteID,FarmCode from FarmSite;";
                    bSuccess = BuildSiteDictionary(dictFarm, strQuery, connection);
                    Dictionary<string, Tuple<int, string>> dictCollSite = new Dictionary<string, Tuple<int, string>>();
                    strQuery = "select SiteName,CollectionSiteID,SiteCode from CollectionSite;";
                    bSuccess = BuildSiteDictionary(dictCollSite, strQuery, connection);
                    Dictionary<string, int> dictSpecies = new Dictionary<string, int>();
                    bSuccess = BuildSpeciesDictionary(dictSpecies, connection);
                    Dictionary<int, string> dictCodon = new Dictionary<int, string>();
                    strQuery = "select SpeciesID,Codon from Species;";
                    bSuccess = BuildCodonDictionary(dictCodon, strQuery, connection);


                    // Now read the iput file - the csv
                    // open file
                    FileStream fs = new FileStream(strInFile, FileMode.Open, FileAccess.Read);
                    StreamReader sr = new StreamReader(fs, System.Text.Encoding.ASCII, false);
                    // skip the first line as it is column headers
                    sr.ReadLine();
                    // now walk the file buiding insert strings and sending them to the db.
                    // In this case we will switch on the SourceType value to get the right ID for SourceSiteID

                    string strLine = " ";
                    int rowsAdded = 0;
                    //
                    // for each line
                    while (null != strLine)
                    {
                        rowID++;
                        strLine = sr.ReadLine();
                        if (null == strLine) break;
                        // remove the quotation marks from the fields
                        strLine = strLine.Replace('"', ' ');
                        // split into an array of the fields
                        fields = strLine.Split(',');
                        // remove leading and trailing spaces
                        // also escape any single quotes in the text with another single quote
                        for (int i = 0; i < fields.Length; ++i)
                        {
                            fields[i] = fields[i].Trim(' ');
                            // only in fields we will insert do we need to double up the apostrophe's
                            // not in fields we use for comparison
                            if(7 == i)  // notes
                            {
                                fields[i] = fields[i].Replace("'", "''");
                            }
                        }
                        if (bCommaCheck)
                        {
                            if(10 != fields.Length )
                            {
                                sw.WriteLine("wrong number of commas on line {0}", rowID);
                            }
                        }
                        if(bSpeciesCheck)
                        {
                            try
                            {
                                int iSpecies = dictSpecies[fields[2]];
                            }
                            catch(Exception ex)
                            {
                                System.Console.WriteLine("ScientificName - GenusSpecies mismatch " +
                                    "on line {0} Name was {1}", rowID, fields[2]);
                                sw.WriteLine("ScientificName - GenusSpecies mismatch " +
                                    "on line {0} Name was {1}", rowID, fields[2]);
                            }
                            
                        }
                        if (bSpeciesCheck || bCommaCheck) continue;
                        // Input file has "LotNumber","OldLotNumber","ScientificName","RegionCode",
                        // CollectionDate,DateReceived,SeedGrade,Note,CollectionSites,SourceType
                        //
                        // mapping is:
                        // Table Col,               File Col
                        //================          =================
                        // SeedLotID                Ignore
                        // CreatedBy                'Imported'
                        // CreationDate             Set to time of import
                        // FinalHarvestDate         Ignore
                        // FirstHarvestDate         CollectionDate (4)
                        // Generation               If sourcetype (9) wild then 0 else -1
                        // Grade                    SeedGrade (6)
                        // HarvestedQuantity        dropped column
                        // LotTag                   See routine BuildSeedLotTag
                        // ModifiedBy               'Imported'
                        // ModifiedDate             Set to time of import  
                        // Notes                    Notes (7)
                        // SourceOrgID              dropped column
                        // SoureSiteID              From dictFarm or dictCollSite look up CollectionSites(8)
                        //                            dict depends on SourceType (9)
                        // SourceType               Constant from SourceType (9)
                        // SpeciesID                // From dictSpecies lookup ScientificName (2)
                        // Status                   set do InStock then update after reading in transactions                                              
                        // timestamp                ignore - autogenerated?
                        // ADNumber                 LotNumber (0)
                        // FMNumber                 OldLotNumber (1)
                        // SeedLotNumber            OldLotNumber if non null else LotNumber 

                        // build the insert statement
                        string strInsert = "INSERT INTO SeedLot (CreatedBy,CreationDate," +
                            "FinalHarvestDate,FirstHarvestDate,Generation,Grade,LotTag," +
                            "ModifiedBy,ModifiedDate,Notes,SourceSiteID,SourceType,SpeciesID," +
                            "Status,ADNumber,FMNumber,SeedLotNumber) VALUES( ";
                        strInsert += "'Imported', ";  // created by
                        DateTime dt = DateTime.Now;   // creationdate
                        strInsert += "'" + dt.ToString() + "', ";
                        strInsert += "'', ";  // final harvest -empty string
                        string strFirstHarvestDate = fields[4];
                        strInsert += "'" + strFirstHarvestDate + "', "; //  First harvest date
                        int sourceID = -1;
                        int generation = -1;
                        int iSourceType = -1;
                        string strSiteCode = null;
                        Tuple<int, string> tup;
                        // Insert generation and gather other site sepcific data for later insertion
                        // and lotTag creation
                        string strSiteType = fields[9];
                        if (null == strSiteType || 0 == strSiteType.Length)
                            strSiteType = Constants.Wild;
                        switch (strSiteType)  // generation
                        {
                            case Constants.Wild:
                                generation = 0;
                                strInsert += "0, ";
                                tup = dictCollSite[fields[8]];
                                sourceID = tup.Item1;
                                strSiteCode = tup.Item2;
                                iSourceType = (int)eSourceType.CollectionSite;
                                break;
                            case Constants.Farm:
                                generation = -1;
                                strInsert += "-1, ";
                                tup = dictFarm[fields[8]];
                                sourceID = tup.Item1;
                                strSiteCode = tup.Item2;
                                iSourceType = (int)eSourceType.FarmSite;
                                break;
                            default:
                                System.Console.WriteLine("Unregognized site type {0} in row {1}", fields[9], rowID);
                                Environment.Exit(-1);
                                break;
                        }
                        string strGrade = fields[6];
                        strInsert += "'" + strGrade + "', '";  // grade
                        // Lot Tag
                        // Gather some pieces we will also need to insert, to build the LotTag
                        string strIndex = fields[2];
                        //System.Console.WriteLine("looking for |{0}|", strIndex);
                        int speciesID = dictSpecies[fields[2]];
                        string strCodon = dictCodon[speciesID];
                        string strRegion = fields[3];
                        int seedLotNumber = ChooseSeedLotNumber(fields[0], fields[1]);
                        strInsert += BuildSeedLotTag(strCodon, strRegion, strFirstHarvestDate,
                                        generation, strGrade, strSiteCode, seedLotNumber);
                        strInsert += "', ";
                        strInsert += "'Imported', ";                        // modified by
                        strInsert += "'" + dt.ToString() + "', ";
                        strInsert += "'" + fields[7] + "', ";               // notes
                        strInsert += "'" + sourceID.ToString() + "', ";     // siteID

                        strInsert += iSourceType + ", ";           // source Type
                        strInsert += speciesID.ToString() + ", ";           // speciesID
                        strInsert += ((int)eSeedLotState.InStock).ToString() + ", "; // state
                        strInsert += fields[0].ToString() + ", ";           // ADNumber
                        string strFMNumber = fields[1].ToString(); ;
                        if ("" == strFMNumber)
                            strFMNumber = "-1";
                        strInsert += strFMNumber + ", ";           // FMNumber
                        strInsert += seedLotNumber.ToString() + ");";        // SeedLot Number
                        Console.WriteLine(strInsert);
                        SqlCommand command = new SqlCommand(strInsert, connection);
                        //command.CommandText = strInsert;
                        int rows = command.ExecuteNonQuery();
                        rowsAdded += rows;
                    }
                    Console.WriteLine("Added {0} rows.", rowsAdded);
                    if (bSpeciesCheck || bCommaCheck)
                        sw.Flush();
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message + " on row{0}",rowID );
                         Environment.Exit(-1);
                }
            }

            private static bool BuildSpeciesDictionary(Dictionary<string, int> dictSpecies, SqlConnection connection)
            {
                string strQuery = "select Genus,SpeciesName,SubTaxa,SpeciesID from Species;";
                SqlCommand command = new SqlCommand(strQuery, connection);

                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int index = Convert.ToInt32(reader[3].ToString());
                            string strKey = reader[0].ToString() + " " + reader[1].ToString();
                            if (null != reader[2] && reader[2].ToString().Length > 0)
                                strKey += " " + reader[2].ToString();
                            dictSpecies.Add(strKey, index);
                            Console.WriteLine(String.Format("{0}, {1}, {2}. {3}",
                                reader[0], reader[1], reader[2], reader[3]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
                return true;
            }
            private static bool BuildCodonDictionary(Dictionary<int, string> dict,
                string strQuery, SqlConnection connection)
            {
                SqlCommand command = new SqlCommand(strQuery, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int speciesID = Convert.ToInt32(reader[0].ToString());
                            dict.Add(speciesID, reader[1].ToString());
                            Console.WriteLine(String.Format("{0}, {1}",
                                reader[0], reader[1]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
                return true;
            }

            private static bool BuildSiteDictionary(Dictionary<string, Tuple<int, string>> dict, string strQuery, SqlConnection connection)
            {
                SqlCommand command = new SqlCommand(strQuery, connection);
                //connection.Open();
                // Using statement below is so that the reader goes out of scope when we are done
                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int index = Convert.ToInt32(reader[1].ToString());
                            Tuple<int, string> tup = new Tuple<int, string>(index, reader[2].ToString());
                            dict.Add(reader[0].ToString(), tup);
                            Console.WriteLine(String.Format("{0}, {1}, {2}",
                                reader[0], reader[1], reader[2]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
                return true;
            }
            private static bool BuildDictionary(Dictionary<string, int> dict, string strQuery, SqlConnection connection)
            {
                SqlCommand command = new SqlCommand(strQuery, connection);
                //connection.Open();
                // Using statement below is so that the reader goes out of scope when we are done
                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    try
                    {
                        while (reader.Read())
                        {
                            int index = Convert.ToInt32(reader[1].ToString());
                            dict.Add(reader[0].ToString(), index);
                            Console.WriteLine(String.Format("{0}, {1}",
                                reader[0], reader[1]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
                return true;
            }
            // LotTag is calculated from “Species ID”, “Region”, “Collection Date”, “Generation”, Grade, 
            // Site Code and the “Lot ID”. Here is an example: ACHMIL-SJI-17-0-B-ACN-29456.  
            private static string BuildSeedLotTag(string strCodon, string strRegion,
                   string strFirstHarvestDate, int Generation, string Grade, string strSiteCode, int SeedLotNumber)
            {
                string strTag = "";
                string strSep = "-";
                // Codon
                strTag += strCodon;
                strTag += strSep;
                // Region
                strTag += strRegion;
                strTag += strSep;

                // Year of collection
                if (null != strFirstHarvestDate)
                {
                    DateTime dtTemp = Convert.ToDateTime(strFirstHarvestDate);
                    strTag += dtTemp.Year.ToString().Substring(2, 2);
                }
                else
                {
                    strTag += "??";
                }
                strTag += strSep;
                // generation
                strTag += Generation.ToString();
                strTag += strSep;
                // grade
                strTag += Grade;
                strTag += strSep;
                // site code
                strTag += strSiteCode;
                strTag += strSep;
                // SeedLot Number
                strTag += SeedLotNumber.ToString();
                return strTag;
            }
            private static int ChooseSeedLotNumber(string strAccessNum, string strFileMakerNum)
            {
                int lotNumber = -1;
                int FMNum;
                if (int.TryParse(strFileMakerNum, out FMNum))
                {
                    if (FMNum > 0)
                    {
                        return lotNumber = FMNum;
                    }
                }
                bool bWorked = int.TryParse(strAccessNum, out lotNumber);
                return lotNumber;
            }

        }

        // string constants not really necessary but good practice for things that could change and
        // to prevent mistyping in comparisons
        public enum eSeedLotState { Raw = 0, ReadyForAllocation = 1, InStock = 2, OutOfStock = 3 }
        public static class Constants
        {
            public const string Farm = "Farm";
            public const string Wild = "Wild";
        }
        public enum eSourceType { FarmSite = 0, CollectionSite = 1 }

    }
}



