﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
//
// Read the input CSV file for LevelIVRegions, look up the corresponding Level3Region ID, construct
// an insert statement and put the Level 4 regions in the database.

namespace ImportLevel4
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if(args.Length < 1)
            {
                System.Console.WriteLine("Usage: ImportLevel4 inputCSVfile");
                Environment.Exit(0);
            }
            string strInFile = args[0];
            string[] fields = null;
            try
            {
                // connect to database
                string connString;
                connString = System.Environment.GetEnvironmentVariable("NOITCENNOCKCARTDEES");
                if(null==connString)
                    connString = "Server=(localdb)\\mssqllocaldb; Database = aspnet-SeedTrack-7031278e-e5ab-4918-90cf-fe82915bcd0c; Trusted_Connection = True";
                SqlConnection connection = new SqlConnection(connString);
                // We will build a dictionary of the level3 names and codes here by reading the 
                // Level 3 table from the database.
                string strQuery = "select Name,Level3RegionID from Level3Region;";
                SqlCommand command = new SqlCommand(strQuery, connection);
                connection.Open();
                Dictionary<String,int> dictLevel3 = new Dictionary<String, int>();
                // Using statement below is so that the reader goes out of scope when we are done
                // with it and then we can reuse the connection for our insert later on.
                using (SqlDataReader reader = command.ExecuteReader()) {
                    try
                    {
                        while (reader.Read())
                        {
                            int index = Convert.ToInt32(reader[1].ToString());
                            dictLevel3.Add(reader[0].ToString(), index);
                            Console.WriteLine(String.Format("{0}, {1}",
                                reader[0], reader[1]));
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                // Now read the iput file - the csv
                // open file
                StreamReader sr = new StreamReader(new FileStream(strInFile, FileMode.Open, FileAccess.Read));
                // skip the first line as it is column headers
                sr.ReadLine();
                // now walk the file buiding insert strings and sending them to the db.
                string strLine = " ";
                int rowID=0;
                int rowsAdded = 0;
                //
                // for each line
                while (null != strLine)
                {
                    rowID++;
                    strLine = sr.ReadLine();
                    if (null == strLine) break;
                    // remove the quotation marks from the fields
                    strLine = strLine.Replace('"', ' ');
                    // split into an array of the fields
                    fields = strLine.Split(',');
                    // remove leading and trailing spaces
                    for (int i= 0; i< fields.Length;++i)
                    {
                        fields[i] = fields[i].Trim(' ');
                    }
                    // Input file has "RegionCode","LevelIVRegionName","LevelIVRegion.","LevelIIIRegionName"
                    // Table has Level4RegionID,Level3RegionID,Name,RegionCode,CombinedRegionCode
                    //
                    // mapping is:
                    // Table Col,               File Col
                    //================          =================
                    // Level4REgion ID          ignore
                    // Lvel3RegionID            Must Look up LevelIIIRegionName(3) from LevelIII Region table
                    // Name                     LevelIVREgionName (1)
                    // RegionCode               RegionCode (0)
                    // CombinedRegionCode       LevelIVRegion (2)
                    // build the insert statement
                    string strInsert= "INSERT INTO Level4Region (Level3RegionID,Name,"+
                        "RegionCode,CombinedRegionCode) VALUES(";
                    //strInsert += rowID.ToString();
                    int reg3ID = dictLevel3[fields[3]];  //Level4RegionID
                    strInsert += reg3ID.ToString(); // Level3RegionID
                    strInsert += ", " + "\'" + fields[1] + "\'";       // 'Name'
                    strInsert += ", " + "\'" + fields[0] + "\'";        // 'RegionCode'
                    strInsert += ", " + "\'" + fields[2] + "\'";        // 'CombinedRegionCode'
                    strInsert += ");";                                 // );
                    // show the command we will then execute
                    Console.WriteLine(strInsert);
                    command.CommandText = strInsert;
                    int rows = command.ExecuteNonQuery();
                    rowsAdded += rows;
                }
                Console.WriteLine("Added {0} rows.", rowsAdded);


            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }

        }

    }
}
